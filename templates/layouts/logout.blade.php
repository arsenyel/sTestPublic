<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="public/img/favicon.ico"/>
        <title>vOne | @yield('title')</title>
        <link rel="stylesheet" href="public/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="public/font-awesome/css/font-awesome.min.css">
        @yield('style')
    </head>
    <body id="imagen-login">
        <div class="container" style="padding-top: 50px;">
            @yield('content')
        </div>
    @if (ENVIROMENT === 'development')
        <script src="public/commons.js?v={{ time() }}"></script>
    @else
        <script src="public/commons.js"></script>
    @endif
    @yield('scripts')
    </body>
</html>