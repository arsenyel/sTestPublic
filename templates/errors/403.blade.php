@extends('layouts.error')

@section('title', '403')

@section("content")
	<h2>403 - Prohibido</h2>
    <p>La solicitud se ha rechazado.</p>
    <a href="#" onclick="javasccript:window.history.back();">Regresar</a>
@endsection