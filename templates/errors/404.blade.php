@extends('layouts.error')

@section('title', '404')

@section("content")
	<h2>404 - No encontrado</h2>
    <p>El servidor no encuentra la página solicitada.</p>
    <a href="#" onclick="javascript:window.history.back();">Regresar</a> 
@endsection
