@extends('layouts.error')

@section('title', '401')

@section("content")
	<h2>401 - No autorizado</h2>
    <p>Necesita estar autenticado para acceder.</p>
    <a href="/">Regresar</a>
@endsection