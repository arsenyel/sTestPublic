@extends('layouts.master')

@if($action == 'Ver')
    @section('title', 'Ver Reserva')
@elseif($action == 'Editar')

    @section('title', 'Editar Reserva')
@elseif($action == 'Agregar')
    @section('title', 'Agregar Reserva')
@endif

@section('facilities')
    <li class="active">
@endsection

@section("content")
    @if(isset($reservation))
        @if($action === 'Ver')
            <div id="reservation" 
                data-reservation="{{ json_encode($reservation) }}"                 
                data-facilities="{{ json_encode($facilities) }}"                 
                data-people="{{ json_encode($people) }}"                 
                data-action="{{ $action }}"  
                data-disabled="true"></div>
        @else
            <div id="reservation" 
                data-reservation="{{ json_encode($reservation) }}"                 
                data-facilities="{{ json_encode($facilities) }}"                 
                data-people="{{ json_encode($people) }}"
                data-action="{{ $action }}"
                data-disabled="false"></div>
        @endif
    @else
            <div id="reservation"                 
                data-facilities="{{ json_encode($facilities) }}"                 
                data-people="{{ json_encode($people) }}"
                data-action="{{ $action }}" 
                data-disabled="false"></div>
    @endif
@endsection

@section("scripts")
    @if(ENVIROMENT === 'development')
<script src="/public/facility/view.js?v={{ time() }}"></script>
    @else
<script src="/public/facility/view.js"></script>
    @endif
@endsection