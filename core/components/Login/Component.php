<?php
    namespace Core\Components;

    use Core\Mongo;
    use Models\SessionModel;
    use Models\EventsLoginModel;
    use Core\Validators\DataType;
    use Core\Helper\Strings;
    use Core\Helper\Dates;
    use Core\Helper\Request;
    use Core\Helper\Converters;
    use Core\Components\Session;
    use Components\Mqtt;
    
    use MongoDB\BSON\ObjectID;
    
    class Login 
    {
        function __construct($model, $session)
        {
            global $config;

            $this->model = $model;

            $this->server = '';

            $this->session = $session;

            $this->_config = $config;
        }

        public function auth($options = array())
        {
            if (isset($options['user']))
            {
                $this->user = strtolower($options['user']);
            }
            else
            {
                return array(
                    'code' => 400,
                    'message' => 'Usuario es requerido'
                );
            }
            
            if (isset($options['pass']))
            {
                $this->pass = $options['pass'];
            }
            else
            {
                return array(
                    'code' => 400,
                    'message' => 'Password es requerido'
                );
            }

            if (isset($options['server']))
            {
                $this->server = $options['server'];
            }

            $result = $this->_login();

            if ($result !== TRUE)
            {
                return $result;
            }

            $result = $this->_killLastSession();

            if ($result !== TRUE)
            {
                return $result;
            }

            $uri = $_SERVER['REQUEST_URI'];

            $uri = explode('/', $uri);

            $uri = $uri[count($uri)-1];

            $this->session->set(array(
                'user' => $this->user,
                'firstActivity' => Dates::fullDate(),
                'lastActivity' => Dates::fullDate(),
                'guard' => (string)$this->id,
                'role' => (string)$this->role,
                'uri' => $uri,
                'ip' => Request::RealClientIP(),
                'browser' => Request::browserName(),
            ));
        
            $message = Strings::formatString('sistema 10 @product-@ipserver-@user @time @firstname @lastname [@document] se ha logueado con la IP: @ip', array(
                '@product' => $this->_config['PRODUCT_ID'],
                '@ipserver' => $this->server,
                '@user' => $this->session->user,
                '@time' => (string)Converters::stringToDate(),
                '@firstname' => $this->firstname,
                '@lastname' => $this->lastname,
                '@document' => $this->documentNumber,
                '@ip' => Request::RealClientIP()
            ));
            
            $result = $this->session->init();

            if ($result !== TRUE)
            {
                return $result;
            }
            else
            {
                $eventosLoginModel = new EventsLoginModel();

                $eventosLoginModel->Usuario = $this->session->user;

                $eventosLoginModel->Navegador = Request::browserName();
                
                $eventosLoginModel->Hora = Converters::stringToDate();
                
                $eventosLoginModel->HoraString = Dates::fullDate();

                $eventosLoginModel->Rol = new ObjectID($this->session->role);

                $eventosLoginModel->IP = Request::RealClientIP();

                $eventosLoginModel->Actividad = 'Inicio de sesion de ' . $this->user;

                $result = $eventosLoginModel->insert();

                if (gettype($result) !== 'object')
                {
                    return $result;
                }
                else
                {
                    Mqtt::publish(array('message' => $message, 'topic' => 'system'));

                    $this->session->set(array(
                        'lastActivity' => Dates::fullDate(),
                        'deleteDate' => Dates::fullDate()
                    ));
                    
                    $this->session->update();

                    return TRUE;
                }
            }
        }

        public function logout($options = array())
        {
            if (isset($options['server']))
            {
                $this->server = $options['server'];
            }

            if ($this->session !== '')
            {
                $this->user = strtolower($this->session->user);

                $user = $this->model->findOne(array(
                    'Usuario' => $this->user
                ));

                if ($user !== FALSE)
                {
                    if ($this->_assignAttribute($user) === TRUE)
                    {
                        $message = Strings::formatString('sistema 10 @product-@ipserver-@user @time @firstname @lastname [@document] se ha logueado con la IP: @ip', array(
                            '@product' => $this->_config['PRODUCT_ID'],
                            '@ipserver' => $this->server,
                            '@user' => $this->user,
                            '@time' => (string)Converters::stringToDate(),
                            '@firstname' => $this->firstname,
                            '@lastname' => $this->lastname,
                            '@document' => $this->documentNumber,
                            '@ip' => Request::RealClientIP()
                        ));

                        Mqtt::publish(array('message' => $message, 'topic' => 'system'));

                        $eventosLoginModel = new EventsLoginModel();

                        $eventosLoginModel->Usuario = $this->user;

                        $eventosLoginModel->Navegador = Request::browserName();

                        $eventosLoginModel->Hora = Converters::stringToDate();
                        
                        $eventosLoginModel->HoraString = Dates::fullDate();

                        if (isset($this->session->Rol) && DataType::validOid($this->session->role))
                        {
                            $eventosLoginModel->Rol = new ObjectID($this->session->role);
                        }

                        $eventosLoginModel->IP = Request::RealClientIP();

                        $eventosLoginModel->Actividad = "Usuario {$this->user} cerror sesion";

                        $result = $eventosLoginModel->insert();

                        return $this->session->destroy();
                    }
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }

        private function _login()
        {
            $user = $this->model->findOne(array(
                'Usuario' => $this->user,
                'Estado' => TRUE
            ));

            if ($user !== FALSE)
            {
                if (!password_verify($this->pass, $user->Password))
                {
                    return array(
                        'code' => 401,
                        'message' => 'la contraseña no es correcta'
                    );
                }

                $this->objectUser = $user;

                return $this->_assignAttribute($user);
            }
            else
            {
                return array(
                    'code' => 401,
                    'message' => 'Usuario no encontrado'
                );
            }
        }

        private function _assignAttribute($user = '')
        {
            if ($user !== '')
            {
                $this->firstname = $user->Nombre;

                $this->lastname = $user->Apellido;

                $this->documentNumber = $user->NumeroDocumento;

                $this->role = $user->Rol;

                $this->id = $user->_id;

                return TRUE;
            }

            return FALSE;
        }

        private function _killLastSession()
        {
            $sessionModel = new SessionModel();

            $session = $sessionModel->findOne(array(
                'Usuario' => $this->user
            ));

            if ($session !== FALSE)
            {
                $user = $this->model->findOne(array(
                    'Usuario' => $this->user
                ));

                if ($user !== FALSE)
                {
                    $message = Strings::formatString('sistema 10 @product-@ipserver-@user @time @firstname @lastname [@document] ha sido deslogueado/a por la IP: @ip', array(
                        '@product' => $this->_config['PRODUCT_ID'],
                        '@ipserver' => $this->server,
                        '@user' => $this->user,
                        '@time' => (string)Converters::stringToDate(),
                        '@firstname' => $user->Nombre,
                        '@lastname' => $user->Apellido,
                        '@document' => $user->NumeroDocumento,
                        '@ip' => Request::RealClientIP()
                    ));
                    
                    Mqtt::publish(array('message' => $message, 'topic' => 'system'));

                    $eventosLoginModel = new EventsLoginModel();

                    $eventosLoginModel->Usuario = $this->user;

                    $eventosLoginModel->Navegador = $session->Navegador;

                    $eventosLoginModel->Hora = Converters::stringToDate();
                    
                    $eventosLoginModel->HoraString = Dates::fullDate();

                    if (isset($session->Rol) && DataType::validOid($session->Rol))
                    {
                        $eventosLoginModel->Rol = new ObjectID($session->Rol);
                    }

                    $eventosLoginModel->IP = $session->IP;

                    $eventosLoginModel->Actividad = 'Inicio desde otro navegador';

                    $result = $eventosLoginModel->insert();

                    if (gettype($result) !== 'object')
                    {
                        return array(
                            'code' => 500,
                            'message' =>  'Error interno, consulte su administrador'
                        );
                    }
                    else
                    {
                        $result = $session->remove();

                        if ($result !== TRUE) 
                        {
                            return array(
                                'code' => 500,
                                'message' =>  'Error interno, consulte su administrador'
                            );
                        }
                        else
                        {
                            return array(
                                'code' => 409,
                                'message' => 'El usuario se encontraba conectado'
                            );
                        }
                    }
                }
            }

            return TRUE;
        }
    }
