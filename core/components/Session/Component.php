<?php
    namespace Core\Components;

    use Models\SessionModel;
    use Models\RoleModel;
    use Core\Helper\Dates;
    use Core\Helper\Request;
    use Core\Helper\Strings;
    use Core\Helper\Parser;
    use Core\Helper\Converters;
    use MongoDB\BSON\ObjectID;

    class Session
    {
        function __construct()
        {
            if (!isset($_SESSION))
            {
                session_start();
            }
            
            foreach ($_SESSION as $key => $value)
            {
                $this->$key = $value;
            }

            $this->model = new SessionModel();
        }
        
        public function set($data = array())
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;
                
                $this->_write($key, $value);
            }
        }

        public function init()
        {
            return $this->_save();
        }

        public function update()
        {
            return $this->_update();
        }

        public function destroy()
        {
            if ($this->_delete())
            {
                $_SESSION = array();

                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();

                    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
                }

                return session_destroy();
            }
            
            return FALSE;
        }

        private function _write($name, $value)
        {
            $_SESSION[$name] = $value;
        }

        public function hasPermission($module, $permission = NULL)
        {
            $role = $this->getRole();

            if ($role !== FALSE)
            {
                if (isset($role->Permisos))
                {
                    $result = Strings::getValueThroughKeyFromObject($module, $role->Permisos);

                    if ($result !== FALSE)
                    {
                        if (gettype($result) === 'array')
                        {
                            if ($permission !== NULL)
                            {
                                return $this->_isInTheList($result, $permission);
                            }
                        }

                        return TRUE;
                    }
                }
            }
            
            return FALSE;
        }

        private function _isInTheList($list, $item)
        {
            foreach ($list as $key)
            {
                if ($key === $item)
                {
                    return TRUE;
                }
            }

            return FALSE;
        }

        private function _isTheSameName($name, $value)
        {
            if ($name === $value)
            {
                return TRUE;
            }

            return FALSE;
        }

        public function hasRole($name)
        {
            $role = $this->getRole();

            if ($role !== FALSE)
            {
                return $this->_isTheSameName($role->Nombre, $name);
            }
        
            return FALSE;
        }

        public function isLoggedIn()
        {
            if ($this->hasUser() && $this->hasPosition() && $this->_get() === true)
            {
                $this->_writeLastActivity();

                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }

        public function hasUser()
        {
            return isset($this->user);
        }

        public function hasPosition()
        {
            return isset($this->position);
        }

        public function getRole()
        {
            if ($this->_get() !== FALSE)
            {
                return $this->rolObject;
            }
            else
            {
                return FALSE;
            }
        }

        private function _writeLastActivity()
        {
            if (isset($this->user))
            {
                $uri = $_SERVER['REQUEST_URI'];

                $uri = explode('/', $uri);

                $uri = $uri[count($uri)-1];

                $value = array(
                    'lastActivity' => Dates::fullDate(),
                    'uri' => $uri
                );

                $this->set($value);

                $this->_setFields();

                $result = $this->model->update();

                if ($result !== FALSE)
                {
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }

        private function _get()
        {
            $session = $this->model->findOne(array(
                'Usuario' => $this->user
            ));

            if ($session !== FALSE)
            {
                $model = new RoleModel();

                if (isset($session->Rol) && $session->Rol !== '')
                {
                    $role = $model->findOne(array(
                        '_id' => $session->Rol
                    ));

                    if ($role !== FALSE)
                    {
                        $this->rolObject = $role;
                    }
                }

                $this->model = $session;

                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }

        private function _save()
        {
            $this->_setFields();

            $result = $this->model->insert();

            if (gettype($result) === 'object')
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }

        private function _update()
        {
            $this->_setFields();

            $result = $this->model->update();

            if ($result !== FALSE)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }

        private function _delete()
        {
            $this->_get();

            return $this->model->remove();
        }

        private function _setFields()
        {
            $this->model->Usuario = $this->user;

            $this->model->Navegador = $this->browser;

            $this->model->PrimerActividad = $this->firstActivity;

            $this->model->UltimaActividad = $this->lastActivity;
            
            $this->model->UltimaActividadMongo = Converters::stringToDate($this->lastActivity);

            $this->model->Rol = new ObjectID($this->role);

            $this->model->IP = $this->ip;

            $this->model->URI = $this->uri;
        }
        
        public function hasEnroller()
        {
            return isset($this->enroller);
        }
        
        public function hasCamera($camera)
        {
            if(isset($this->rolObject->Camaras) && is_array($this->rolObject->Camaras))
            {
                foreach($this->rolObject->Camaras as $rolCamera)
                {
                    if((string)$rolCamera->_id == (string)$camera)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
