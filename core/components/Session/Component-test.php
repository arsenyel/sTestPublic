<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Components\Session\Session;

    if (!isset($_SESSION))
    {
        $_SESSION = array();
    }

    class ComponentTest extends TestCase
    {
        public function testInstanceCorrectly()
        {
            $component = new Session();

            $this->assertNotEmpty($component);
        }

        /*public function testSetData()
        {
            $model = new \ModeloStub();

            $parser = new Models\Helper\Parser($model->getMap());

            $data = $parser->setData($model);

            $this->assertArrayHasKey("_id", $data);
        }

        public function testAddChild()
        {
            $model = new \ModeloStub();

            $model->Conectores[] = array(
                'Numero' => '2',
                'Nombre' => 'Nuevo',
                'Pulso' => 5
            );

            $parser = new Models\Helper\Parser($model->getMap());

            $data = $parser->setData($model);

            $this->assertCount(2, $data['Conectores']);
        }

        public function testAddChildWithoutChangeParent()
        {
            $model = new \ModeloStub();

            $model->Conectores[] = array(
                'Numero' => '2',
                'Nombre' => 'Nuevo',
                'Pulso' => 5
            );

            $parser = new Models\Helper\Parser($model->getMap());

            $data = $parser->setData($model);

            $this->assertEquals('1', $data['Estado']);
        }*/
    }
