<?php
    namespace Core;

    use \stdClass;
    use Core\Mongo;
    use Core\Helper\Parser;
    use Core\Validators\DataType;
    use MongoDB\BSON\ObjectID;
    /**
    * 
    */
    class Model
    {
        private $connection;

        public $args = array();

        function __construct($otherConf = array())
        {
            $database = '';

            if (isset($otherConf['DATABASE']))
            {
                $database = $otherConf['DATABASE'];
            }
            
            $user = '';
            
            if (isset($otherConf['DATABASE_USER']))
            {
                $user = $otherConf['DATABASE_USER'];
            }
            
            $password = '';
            
            if (isset($otherConf['DATABASE_PASSWORD']))
            {
                $password = $otherConf['DATABASE_PASSWORD'];
            }
            
            $host = '';
            
            if (isset($otherConf['DATABASE_HOST']))
            {
                $host = $otherConf['DATABASE_HOST'];
            }
            
            $this->connection = new Mongo($database, $user,  $password, $host);

            global $app;

            if (isset($app, $app->lang))
            {
                $this->lang = $app->lang;
            }
            
            $this->_construct();
        }
        
        private function _construct()
        {
            
        }

        public function getCollection()
        {
            return $this->collection;
        }

        public function getConnection()
        {
            return $this->connection;
        }

        public function validate($ignore = array(), $data = array())
        {
            return TRUE;
        }

        /**
         * @param string $name
         * @param mixed $value;
         * @return mixed
         * From view to db
         */
        public function specifyValue($name, $value)
        {
            return $value;
        }

        /**
         * @param string $name
         * @param mixed $value
         * @return mixed
         * From db to view
         */
        public function howToShowValue($name, $value)
        {
            if($value instanceof ObjectID)
            {
                $value = (string)$value;
            }

            return $value;
        }

        /**
         * @param string $collection
         * @param mixed $object
         * @return mixed
         */
        public function setRelation($collection, $object)
        {
            return $object;
        }

        /**
         * @param array $ignore optional
         * @return array
         */
        protected function _setData($ignore = array(), $mongo = FALSE)
        {
            if ($mongo === FALSE)
            {
                return Parser::setData($this, $this->map, $this, $ignore);
            } 
            else
            {
                return Parser::setMongoData(Parser::setData($this, $this->map, $this, $ignore));
            }
        }

        /**
         * @param array $ignore
         * @param array $data
         * @return mixed
         */
        protected function _validate($ignore = array(), $data = array())
        {
            foreach ($this->map as $name => $type)
            {
                if (!in_array($name, $ignore))
                {
                    if (gettype($type) === 'array')
                    {
                        if (isset($data[$name]))
                        {
                            $result = DataType::runValidation($type, $data[$name]);
      
                            if ($result !== TRUE)
                            {
                                return $result;
                            }
                        }
                    }
                    else
                    {
                        $temp = NULL;

                        if (isset($data[$name]))
                        {
                            $temp = $data[$name];
                        }

                        if (!DataType::check($type, $temp))
                        {
                            return "$name inválido.";
                        }
                    }
                    
                    if (isset($this->args[$name], $this->args[$name]['name']) && $this->args[$name]['name'] === 'name')
                    {
                        $verify = $this->getConnection()->find($this->collection, array(
                            $name => $data[$name], 
                        ), array(
                            'projection' => array(
                                '_id' => 1
                            )
                        ));
                        
                        $verify = $verify->toArray();
                        
                        if (isset($verify[0]) && (!isset($this->_id) || (string)$verify[0]->_id != (string)$this->_id))
                        {
                            $field = strtolower($name);

                            return "Ya existe un/a $field '{$this->$name}'.";
                        }
                    }
                }
            }

            return $this->validate($ignore, $data);
        }

        /**
        * Si la coleccion no tiene campo Estado debe pasarse como primer
        * parametro un false, de lo contrario no traerá lista de nada.
        * @param boolean $onlyActive
        * @param string $emptyMessage optional
        * @return array
        */
        public function getList($onlyActive = TRUE, $emptyMessage = NULL)
        {
            $name = '';

            $count = 1;

            foreach ($this->map as $key => $value)
            {
                if ($count == 2)
                {
                    $name = $key;
                    break;
                }

                $count++;
            }

            $query = array(
                'Estado' => TRUE,
                'projection' => array(
                    'Estado' => 1
                )
            );

            if ($onlyActive === FALSE)
            {
                unset($query['Estado']);
            }

            $query['projection'][$name] = 1;
            
            $query['sort'] = array($name => 1);

            $items = $this->find($query);

            $result = array();

            if($emptyMessage !== NULL)
            {
                $result[] = array(
                    'value' => '', 
                    'label' => $emptyMessage
                );
            }

            foreach ($items as $key => $item)
            {
                $temp = new stdClass();

                $temp->value = (string)$item->_id;

                $temp->label = $item->$name;

                $temp->status = $item->Estado;

                $result[] = $temp;
            }

            return $result;
        }
        
        public function findOne($options = array())
        {
            $options['limit'] = 1;
            
            $items = $this->find($options);
            
            if (count($items) > 0)
            {
                foreach ($items as $key => $item)
                {
                    foreach ($this->map as $name => $value)
                    {
                        if (isset($item->$name))
                        {
                            $this->$name = $item->$name;
                        }
                    }
                }

                return $this;
            }
            else
            {
                return FALSE;
            }
        }

        /**
         * @param array $options
         * @return array
         */
        public function find($options = array())
        {
            global $idioma;

            $result = $this->setOptions($options);

            $result = $this->connection->find($this->collection, $result['filter'], $result['options']);
            
            return $result->toArray();
        }

        /**
         * @param array $options
         * @return array
         */
        public function findWithNames($options = array())
        {
            $items = $this->find($options);

            $result = array();

            foreach ($items as $key => $item)
            {
                $result[] = Parser::render($item, $this->args, $this, FALSE);
            }

            return $result;
        }

        /**
         * @param array $options
         * @return array
         */
        public function findWithRelations($options = array())
        {
            $items = $this->find($options);

            $result = array();

            foreach ($items as $key => $item)
            {
                $result[] = Parser::render($item, $this->args, $this, TRUE);
            }

            return $result;
        }

        /**
         * @param array $options
         * @return array
         */
        public function count($options = array())
        {
            global $idioma;

            $result = $this->setOptions($options);
            
            $result['options']['projection'] = array(
                '_id' => 1
            );


            $items = $this->connection->find($this->collection, $result['filter'], $result['options']);
            
            $count = 0;
            
            foreach ($items as $key => $value)
            {
                $count++;
            }

            return $count;
        }

        /**
         * @param array $options
         * @return array
         */
        private function setOptions($options = array())
        {
            $result = array(
                'filter' => array(),
                'options' => array() 
            );

            foreach ($options as $key => $value)
            {
                if ($key === 'sort')
                {
                    $result['options']['sort'] = array();

                    foreach ($value as $jey => $type)
                    {
                        $result['options']['sort'][$jey] = $type;
                    }
                }
                elseif ($key === 'skip')
                {
                    $result['options']['skip'] = (int)$value;
                }
                elseif ($key === 'limit')
                {
                    $result['options']['limit'] = (int)$value;
                }
                elseif ($key === 'projection')
                {
                    $result['options']['projection'] = $value;
                }
                else
                {
                    if ($key === '_id' && !is_array($value) && DataType::validOid($value))
                    {
                        $result['filter'][$key] = new ObjectID((string)$value);
                    }
                    else
                    {
                        $result['filter'][$key] = $value;
                    }
                }
            }

            return $result;
        }

        /**
         *
         */
        private function convertObjects()
        {
            if (DataType::validOid($this->_id))
            {
                $this->_id = new ObjectID($this->_id);
            }
        }

        /**
         * @return mixed
         */
        public function insert()
        {
            $data = $this->_setData(array(
                '_id'
            ));

            $result = $this->_validate(array(
                '_id'
            ), $data);

            if ($result === TRUE)
            {
                $result = $this->connection->insert($data, $this->collection);
                
                $errors = $result['result']->getWriteErrors();

                $inserted = $result['result']->getInsertedCount();

                if (empty($errors) && $inserted > 0)
                {
                    $result = $result['_id'];
                }
                else
                {
                    if (empty($errors))
                    {
                        $result = 'No Ingresado';
                    }
                    else
                    {
                        if ($errors[0]->getCode() === '11000')
                        {
                            $result = 'Tipo-Nro Documento duplicado';
                        }
                        else
                        {
                            $result = $errors[0]->getMessage();
                        }
                    }
                }
            }
            
            return $result;
        }

        /**
         * @return mixed
         */
        public function update($mongo = FALSE)
        {
            $set = $this->_setData(array(
                '_id'
            ), $mongo);

            $result = $this->_validate(array(), $set);

            if ($result === TRUE)
            {
                $where = array(
                    '_id' => $this->_id
                );
                
                $set = array(
                    '$set' => $set
                );
                
                $extra = array(
                    'multi' => FALSE,
                    'upsert' => FALSE
                );
                
                $result = $this->partialUpdate($where, $set, $extra);
            }

            return $result;
        }

        /**
         * @param array $where
         * @return mixed
         */
        public function remove($where = array())
        {
            if(!is_array($where) || empty($where))
            {
                $where = array(
                    '_id' => $this->_id
                );
            }

            $result = $this->connection->Borrar($where, $this->collection);

            $errors = $result->getWriteErrors();

            $removed = $result->getDeletedCount();

            if (count($errors) <= 0 && $removed > 0)
            {
                $result = TRUE;
            }
            elseif (count($errors) <= 0 && $removed <= 0)
            {
                $result = 'No se han realizado cambios';
            }
            elseif (count($errors) > 0)
            {
                $result = $errors[0]->getMessage();
            }

            return $result;
        }

        /**
         * @return array
         */
        public function defaultProjection()
        {
            $projection = array();
            
            foreach($this->args as $key=>$value)
            {
                $projection[$key] = 1;
            }

            return $projection;
        }

        /**
         * @return array
         */
        public function getMap()
        {
            if (isset($this->map))
            {
                return $this->map;
            }

            return array();
        }

        /**
         * @param array $where
         * @param array $set
         * @param array $extra
         * @return mixed
         */
        public function partialUpdate($where, $set, $extra)
        {
            $result = $this->connection->update($where, $set, $extra, $this->collection);
            
            if(is_string($result))
            {
                echo ("\n$result\n");
                
                return $result;
            }
            
            $errors = $result->getWriteErrors();

            $coincidences = $result->getMatchedCount();

            $upserts = $result->getUpsertedCount();

            $modified = $result->getModifiedCount();

            if (count($errors) > 0)
            {
                if ($errors[0]->getCode() === '11000')
                {
                    $result = 'Objeto duplicado';
                }
                else
                {
                    $result = $errors[0]->getMessage();
                }
            }
            else
            {
                if ($coincidences > 0)
                {
                    if ($upserts > 0 || $modified > 0)
                    {
                        $result = TRUE;
                    }
                    else
                    {
                        $result = 'No se han realizado cambios';
                    }
                }
                else
                {
                    $result = 'No se ha encontrado el objeto';
                }
            }

            return $result;
        }
    }
