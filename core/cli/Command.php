<?php
namespace Core\Cli;

use Core\Mongo;
/**
 * 
 */
class Command
{
    public $options = array();

    public $mongo;

    function __construct()
    {
        global $config;

        $this->config = $config;
        
        $this->mongo = new Mongo($config['DATABASE'], $config['DATABASE_USER'],  $config['DATABASE_PASSWORD'], $config['DATABASE_HOST']);
    }

    public function run($options)
    {
        return true;
    }
}
