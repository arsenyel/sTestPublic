<?php
$clis = scandir(BASEPATH . '/core/cli/');

foreach ($clis as $key => $cli)
{
    if ($cli !== '.' && $cli !== '..')
    {
        require_once BASEPATH . '/core/cli/' . $cli;
    }
}

$helpers = scandir(BASEPATH . '/core/helper/');

foreach ($helpers as $key => $helper)
{
    if ($helper !== '.' && $helper !== '..' && strpos($helper, 'test') === FALSE && strpos($helper, 'stub') === FALSE)
    {
        require_once BASEPATH . '/core/helper/' . $helper;
    }
}

$validators = scandir(BASEPATH . '/core/validators/');

foreach ($validators as $key => $validation)
{
    if ($validation !== '.' && $validation !== '..' && strpos($validation, 'test') === FALSE && strpos($validation, 'stub') === FALSE)
    {
        require_once BASEPATH . '/core/validators/' . $validation;
    }
}

$components = scandir(BASEPATH . '/core/components/');

require_once BASEPATH . '/core/components/Component.php';

foreach ($components as $key => $component)
{
    if ($component !== '.' && $component !== '..' && strpos($component, 'test') === FALSE && strpos($component, 'stub') === FALSE && $component !== 'Component.php')
    {
        require_once BASEPATH . '/core/components/' . $component . '/Component.php';
    }
}

$utils = scandir(BASEPATH . '/core/utils/');

foreach ($utils as $key => $util)
{
    if ($util !== '.' && $util !== '..' && strpos($util, 'test') === FALSE && strpos($util, 'stub') === FALSE)
    {
        require_once BASEPATH . '/core/utils/' . $util . '/index.php';
    }
}
