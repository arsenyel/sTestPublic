<?php
/**
 * Haciendo uso de las rutas de https://github.com/klein/klein.php
 */
namespace Core;

class Route
{
    public static function get($url, $callback)
    {
        global $app;

        $app->setRoute('GET', $url, function ($request, $response) use ($callback, $app)
        {
            return $callback($request, $response, $app->getBlade(), $app->session);
        });
    }

    public static function post($url, $callback)
    {
        global $app;
        
        $app->setRoute('POST', $url, function ($request, $response) use ($callback, $app)
        {
            return $callback($request, $response, $app->getBlade(), $app->session);
        });
    }

    public static function put($url, $callback)
    {
        global $app;
        
        $app->setRoute('PUT', $url, function ($request, $response) use ($callback, $app)
        {
            return $callback($request, $response, $app->getBlade(), $app->session);
        });
    }

    public static function delete($url, $callback)
    {
        global $app;
        
        $app->setRoute('DELETE', $url, function ($request, $response) use ($callback, $app)
        {
            return $callback($request, $response, $app->getBlade(), $app->session);
        });
    }

    public static function options($callback)
    {
        global $app;
        
        $app->setRoute('OPTIONS', $url, function ($request, $response) use ($callback, $app)
        {
            return $callback($request, $response, $app->getBlade(), $app->session);
        });
    }

    public static function api($options)
    {
        global $app;

        $app->setResource($options);
    }
}
