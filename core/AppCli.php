<?php
/**
 * Haciendo uso del motor de plantillas de https://github.com/jenssegers/blade
 */
namespace Core;
use Models\VSystemModel;

function loadFilesFromDir($directory = '')
{
    $files = scandir($directory);

    foreach ($files as $key => $file)
    {
        if ($file !== '.' && $file !== '..')
        {
            $path = "$directory/$file";

            if (is_dir($path))
            {
                loadFilesFromDir($path);
            }
            else
            {
                if (file_exists($path))
                {
                    require_once($path);
                }
            }
        }
    }
}

class AppCli
{
    protected $commands = array();

    public function parseArgs($argv)
    {
        $this->options = array();

        foreach ($argv as $key => $value)
        {
            if ($key > 0)
            {
                if (strpos($value, '-') !== FALSE)
                {
                    $this->options[$value] = $key;
                }
            }
        }

        foreach ($this->options as $key => &$value)
        {
            if (isset($argv[$value + 1]))
            {
                if (!strpos($argv[$value + 1], '-'))
                {
                    $value = $argv[$value + 1];
                }
            }
            else
            {
                $value = true;
            }
        }
    }

    public function setCommand($name, $object)
    {
        $name = strtolower($name);

        $this->commands[$name] = $object;
    }

    public function systemConf($model)
    {
        $this->conf = $model->findOne(array());
        
        $this->regionalization = $model->getRegionalization();
        
        $id = isset($this->conf, $this->conf->Regionalizacion, $this->conf->Regionalizacion->Pais) ? $this->conf->Regionalizacion->Pais : '123456789012345678901234';
        
        $country = VSystemModel::getCountries(array(
            '_id' => $id
        ), array(
            'projection' => array(
                'Pais' => 1
            )
        ));
        
        if(isset($country[0]->name))
        {
            $this->regionalization->country = $country[0]->name;
            
            switch ($this->regionalization->country)
            {
                case 'Argentina':
                    date_default_timezone_set('America/Argentina/Buenos_Aires');
                    break;
                case 'Chile':
                    date_default_timezone_set('America/Santiago');
                    break;
            }
        }
    }

    public function run()
    {
        if (isset($this->options['-c']))
        {
            if ($this->options['-c'] !== '' && $this->options['-c'] !== NULL)
            {
                foreach ($this->commands as $name => $object)
                {
                    if ($this->options['-c'] === $name)
                    {
                        $this->commands[$name]->run($this->options);
                    }
                }
            }
            else
            {
                throw new \Exception("EL parametro -c no puede estar vacio.", 1);
            }
        }
        else
        {
            throw new \Exception("El parametro -c es requerido.", 1);		
        }
    }

    public static function loadFilesFromDir($directory = '')
    {
        $files = scandir($directory);

        foreach ($files as $key => $file)
        {
            if ($file !== '.' && $file !== '..')
            {
                $path = "$directory/$file";

                if (is_dir($path))
                {
                    self::loadFilesFromDir($path);
                }
                else
                {
                    if (file_exists($path))
                    {
                        require_once($path);
                    }
                }
            }
        }
    }
}
