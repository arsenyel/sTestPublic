<?php
require_once BASEPATH . '/vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = new Dotenv(BASEPATH);

$dotenv->load();

require_once BASEPATH . '/core/AppCli.php';

use Core\AppCli;

///Loading connection stuff
require_once BASEPATH . '/config/database.php';

require_once BASEPATH . '/core/Mongo.php';

require_once BASEPATH . '/core/SqlServer.php';

require_once BASEPATH . '/core/Model.php';

require_once BASEPATH . '/core/autoload.php';

AppCli::loadFilesFromDir(BASEPATH . '/models');
///end connection stuff

AppCli::loadFilesFromDir(BASEPATH . '/components');

$app = new AppCli();

$app->systemConf(new Models\SystemModel());

$commands =  scandir(BASEPATH . '/commands/');

foreach ($commands as $key => $command)
{
    $num_temp = strpos($command, '.php');

    if ($command !== '.' && $command !== '..' && $num_temp !== FALSE)
    {
        require_once BASEPATH . '/commands/' . $command;

        $class = "Core\Cli\Commands\\" . substr($command, 0, $num_temp);

        $name = substr($command, 0, strpos($command, 'Command'));

        $temp = new $class();

        $app->setCommand($name, $temp);
    }
}
