<?php
/**
 * 
 */
class ModeloStub
{
    protected $map = array(
        '_id' => 'ObjectId',
        'Nombre' => 'string',	        
        'Ip' => '.string',
        'Serial' => '.string',
        'Puerto' => 'int',
        'Estado' => 'boolean',
        'Usuario' => 'string',
        'Contraseña' => 'string',
        'Conectores' => array(
            array(
                'Numero' => 'int',
                'Nombre' => 'string',
                'Pulso' => 'string'
            )
        )
    );

    public $args = array(
        '_id' => array(
            'name' => 'id'
        ),
        'Nombre' => array(
            'name' => 'name'
        ),	        
        'Ip' => array(
            'name' => 'ip'
        ),
        'Serial' => array(
            'name' => 'serial'
        ),
        'Puerto' => array(
            'name' => 'port'
        ),
        'Estado' => array(
            'name' => 'status'
        ),
        'Usuario' => array(
            'name' => 'user'
        ),
        'Contraseña' => array(
            'name' => 'password'
        ),
        'Conectores' => array(
            'name' => 'connectors',
            'children' => array(
                array(
                    'Numero' => array(
                        'name' => 'number'
                    ),
                    'Nombre' => array(
                        'name' => 'name'
                    ),
                    'Pulso' => array(
                        'name' => 'pulse'
                    )
                )
            )
        )
    );

    public function getMap()
    {
        return $this->map;
    }

    public function specifyValue($name, $value)
    {
        return $value;
    }
}

function setValues($model)
{
    $conectorStub = new stdClass();

    $conectorStub->Numero = "1";

    $conectorStub->Nombre = "Contactor uno";

    $conectorStub->Pulso = 5;

    $model->_id = new MongoDB\BSON\ObjectID("58947b753744f562c81fc5d1");

    $model->Nombre = "Grupo 2";

    $model->Ip = "192.168.0.155";

    $model->Puerto = "80";

    $model->Usuario = "admin";

    $model->Contraseña = "8146dpr";

    $model->Estado = "1";

    $model->Conectores = array(
        $conectorStub
    );

    return $model;
}