<?php
namespace Core\Helper;

class Strings
{
    /**
    * EliminarEspacios
    */
    public static function removeSpaces($string)
    {
        $string = str_replace(' ', '', $string);
        
        return $string;
    }

    public static function checkPlain($text)
    {
        return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
    }

    public static function placeholder($text)
    {
        return '<em class="placeholder">' . self::checkPlain($text) . '</em>';
    }

    public static function formatString($string, $args = array())
    {
        // Transform arguments before inserting them.
        foreach ($args as $key => $value)
        {
            switch ($key[0])
            {
                case '@':
                    // Escaped only.
                    $args[$key] = self::checkPlain($value);
                    break;
                case '%':
                default:
                    // Escaped and placeholder.
                    $args[$key] = self::placeholder($value);
                    break;
                case '!':
                    // Pass-through.
            }
        }
        return strtr($string, $args);
    }

    /**
    * EliminarEspaciosFC
    */
    public static function removeInitEndSpaces($string)
    {
        //Elimina los espacios al final y al comienzo
        // "    Hello World  " -> "Hello World"
        return trim($string, ' ');
    }

    /**
    * EliminarNoNumeros
    */
    public static function removeNoNumber($string)
    {
        return preg_replace('/[^0-9]+/', '', $string);
    }

    /**
    * EliminarNoLatLon
    */
    public static function cleanLatLon($string)
    {
        $string = preg_replace('/[^0-9.-]+/', '', $string);

        return (double)$string;
    }

    /**
    * EliminarNoAlphaNumericosYGuion
    */
    public static function removeNoAlphaNumHyphen($string)
    {
        return preg_replace('/[^0-9A-Za-z-]+/', '', $string);
    }

    /**
    * EliminarNoAlphaNumericosYGuionBajo
    */
    public static function removeNoAlphaNumUnderscore($string)
    {
        return preg_replace('/[^0-9A-Za-z_]+/', '', $string);
    }

    /**
    * EliminarNoAlphaNumericosYEspacios
    */
    public static function removeNoAlphaNumSpace($string)
    {
        return preg_replace('/[^0-9A-Za-z\s]+/', '', $string);
    }

    /**
    * EliminarNoAlphaNumericos
    */
    public static function removeNoAlphaNum($string)
    {
        return preg_replace('/[^A-Za-z0-9]+/', '', $string);
    }

    /**
    * EliminarNoNumericosYGuion
    */
    public static function removeNoNumHyphen($string)
    {
        return preg_replace('/[^0-9-]+/', '', $string);
    }

    /**
    * EliminarNoAlphaYEspacios
    */
    public static function removeNoAlphaSpace($string)
    {
        return preg_replace('/[^A-Za-z\sñÑáéíóúÁÉÍÓÚ]+/', '', $string);
    }

    /**
    * EliminarEspaciosD
    */    
    public static function removeDoubleSpace($string)
    {
        //Elimina los espacios dobles
        // "Hello   World" -> "Hello World"
        return preg_replace('/\s+/', ' ', $string);
    }

    /**
    * EliminarEspaciosFCD
    */
    public static function removeSpaceFCD($string)
    {
        //Elimina los espacios al final, al comienzo y los dobles
        //"   Hello     World    " -> "Hello World"
        $string = preg_replace('/\s+/', ' ', $string);
        
        return trim($string, ' ');
    }

    /**
    * CapitalizarTodo
    */
    public static function capitalizeAll($string)
    {
        //Capitaliza todo
        //"Hello123" -> "HELLO123"
        //NOTA: Usar siempre con las patentes
        return strtoupper($string);
    }

    /**
    * CapitalizarPrimeras
    */
    public static function capitalizeWordStart($string)
    {
        //Capitaliza despues de cada " "
        //"rodrigo cabral" -> "Rodrigo Cabral"
        return ucwords(strtolower($string), " .");
    }

    /**
    * Capitaliza solo primera letra de un texto
    * "desarrollo de aplicaciones" -> "Desarrollo de aplicaciones"
    */
    public static function capitalizeOnlyFirstLetterText($string)
    {
        return ucfirst(strtolower($string));
    }

    /**
    * PrepararPatente
    */
    public static function preparePlate($patente)
    {
        //deja la patente lista para ser guardada en la DB
        $patente = self::removeSpaceFCD($patente);

        
        $patente = self::removeNoAlphaNum($patente);
        
        return self::capitalizeAll($patente);
    }

    /**
    * PrepararNombre
    */
    public static function prepareName($name)
    {
        //deja los names listos para ser guardados en la DB
        //NOTA: con name me refiero a cualquier palabra que sea usada como referencia de name (una marca, modelo, aseguradora, etc)
        $name = self::removeSpaceFCD($name);
        
        return self::capitalizeWordStart($name);
    }
    
    /**
    * PrepararUsuario
    */
    public static function prepareUser($usuario)
    {
        $usuario = self::removeNoAlphaNumSpace($usuario);
        return strtolower($usuario);
    }

    public static function getValueThroughKeyFromArray($key, $array)
    {
        $keys = explode('.', $key);

        if (count($keys) > 1)
        {
            $temp = $keys[0];

            if (isset($array[$temp]))
            {
                unset($keys[0]);

                $newKeys = implode('.', $keys);

                if (gettype($array[$temp]) === 'array')
                {
                    return self::getValueThroughKeyFromArray($newKeys, $array[$temp]);
                }

                if (gettype($array[$temp]) === 'object')
                {
                    return self::getValueThroughKeyFromObject($newKeys, $array[$temp]);
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            if (isset($array[$key]))
            {
                return $array[$key];
            }
            else
            {
                return FALSE;
            }
        }
    }

    public static function getValueThroughKeyFromObject($key, $object)
    {
        $keys = explode('.', $key);

        if (count($keys) > 1)
        {
            $temp = $keys[0];

            if (isset($object->$temp))
            {
                unset($keys[0]);

                $newKeys = implode('.', $keys);

                if (gettype($object->$temp) === 'array')
                {
                    return self::getValueThroughKeyFromArray($newKeys, $object->$temp);
                }

                if (gettype($object->$temp) === 'object')
                {
                    return self::getValueThroughKeyFromObject($newKeys, $object->$temp);
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            if (isset($object->$key))
            {
                return $object->$key;
            }
            else
            {
                return FALSE;
            }
        }
    }
    
    public static function prepareHexColor($value)
    {
        return preg_replace('/[^A-F0-9\#]+/', '', strtoupper($value));
    }
}
