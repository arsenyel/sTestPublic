<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Helper\Strings;

    function arrays_are_similar($a, $b)
    {
        if (strcmp(json_encode($a), json_encode($b)) !== 0)
        {
            return FALSE;
        }

        return TRUE;
    }

    class StringsTest extends TestCase
    {
        public function testRemoveSpaces()
        {
            $text = 'This has to end up stuck';

            $this->assertEquals(Strings::removeSpaces($text), 'Thishastoendupstuck');
        }

        public function testFormatString()
        {
            $text = 'This is just a test for vWeb';

            $this->assertEquals(Strings::formatString('This is just a test for @system', array(
                '@system' => 'vWeb'
            )), $text);
        }

        public function testGetValueThroughKeyArray()
        {
            $array = array(
                'config' => array(
                    'system' => array(
                        'employee' => array(
                            'C',
                            'A',
                            'B',
                            'M'
                        )
                    )
                )
            );

            $this->assertTrue(arrays_are_similar(Strings::getValueThroughKeyFromArray('config', $array), $array['config']));

            $this->assertTrue(arrays_are_similar(Strings::getValueThroughKeyFromArray('config.system', $array), $array['config']['system']));

            $this->assertTrue(arrays_are_similar(Strings::getValueThroughKeyFromArray('config.system.employee', $array), $array['config']['system']['employee']));
        }

        public function testGetValueThroughKeyObject()
        {
            $object = new stdClass();

            $object->config = new stdClass();

            $object->config->system = new stdClass();

            $object->config->system->employee = array(
                'C',
                'A',
                'B',
                'M'
            );

            $this->assertEquals(get_object_vars(Strings::getValueThroughKeyFromObject('config', $object)), get_object_vars($object->config));

            $this->assertEquals(get_object_vars(Strings::getValueThroughKeyFromObject('config.system', $object)), get_object_vars($object->config->system));

            $this->assertTrue(arrays_are_similar(Strings::getValueThroughKeyFromObject('config.system.employee', $object), $object->config->system->employee));            
        }
        
        public function testPrepareHexColor()
        {
            $this->assertEquals(Strings::prepareHexColor('#fab0ce'), '#FAB0CE');
            
            $this->assertEquals(Strings::prepareHexColor('  *#fFfg  '), '#FFF');
            
            $this->assertEquals(Strings::prepareHexColor('  * */-+..qwrtyuiopñlkjhgszxvnm,.  '), '');
        }
    }
