<?php
    namespace Core\Helper;
    
    use \Datetime;
    use \DateInterval;
    use MongoDB\BSON\UTCDateTime;
    use Core\Helper\Converters;

    class Dates
    {
        public static function setTimeZone()
        {
            global $app;

            if (isset($app))
            {
                switch ($app->conf->Regionalizacion->Pais)
                {
                    case 'Argentina':
                        date_default_timezone_set('America/Argentina/Buenos_Aires');
                        break;
                    case 'Chile':
                        date_default_timezone_set('America/Santiago');
                        break;
                }
            }
            else
            {
                throw new Exception("No se tiene acceso a la configuracion.", 1);   
            }
        }

        public static function getTimeZome()
        {
            global $app;

            $timeZone = array(
                'Argentina' => (-3),
                'Chile' => (-4)
            );

            return $timeZone['Argentina'];
        }

        public static function fullDate()
        {
            $fecha = date('d/m/Y H:i:s');
            
            return $fecha;
        }

        public static function fullStringToDate($string = '')
        {
            if ($string !== '')
            {
                $string = str_replace('/', '-', $string);
                
                return strtotime($string) * 1000;
            }
            else
            {
                return FALSE;
            }
        }
        
        public static function mongoDateToEpoch($mongoDate)
        {
            $mongoDate = $mongoDate->toDateTime();

            $mongoDate = $mongoDate->getTimestamp();
            
            return (int)$mongoDate;
        }

        /**
        *   Recibe dos fechas en strings.
        */
        public static function compareDatesMoreThanAWeek($start, $end)
        {
            $start = str_replace('/', '-', $start);

            $end = str_replace('/', '-', $end);
            
            $date1 = date_create($start);
            
            $date2 = date_create($end);
            
            $diff = date_diff($date1, $date2);

            $diffT = $diff->format("%R%a");
            
            if (strlen($diffT) > 2 || (int)$diffT > 7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
        * StringMayorQueHoy
        * Recibe fecha en formato string
        */
        public static function dateGreaterThanToday($stringDate)
        {
            self::setTimeZone();

            $date = str_replace('/', '-', $stringDate);

            $date = date('d-m-Y H:i.s', strtotime($date));

            $today = new DateTime();

            $date = date_create($date);

            $diff = date_diff($date, $today);

            $diff = $diff->format('%R%a');
            
            if ($diff[0] == '-')
            {   
                return false;
            }
            else
            {
                return true; 
            }
        }

        /**
        *   Recibe dos fechas en strings.
        */
        public static function compareDatesMoreThanAHour($start, $end)
        {  
            $start = str_replace('/', '-', $start);

            $end = str_replace('/', '-', $end);
            
            $date1 = date_create($start);
            
            $date2 = date_create($end);
            
            $diff = date_diff($date1, $date2);

            $diffT = $diff->format("%h%a");

            if ((int)$diffT >= 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        
        /*
        ValidarDosHorasYMinutos
        */
        
        public static function compareTwoHoursWithMinutes($start, $end)
        {
            $start = explode(':', $start);
            
            $end = explode(':', $end);
            
            $hourStart   = (int)$start[0];
            
            $hourEnd   = (int)$end[0];
            
            $minuteStart = (int)$start[1];
            
            $minuteEnd = (int)$end[1];
            
            if($hourStart>$hourEnd)
            {
                return false;
            }
            if($hourStart==$hourEnd && $minuteStart>=$minuteEnd)
            {
                return false;
            }
            return true;
        }

        /**
        * CompararFechasMasDeUnaSemana
        */
        /*function compareDatesMoreOneWeek($inicio, $fin)
        {  
            $inicio = date_create_from_format('d/m/Y H:i:s', $inicio);
            ///$inicio = date_format($inicio, 'd/m/Y H:i:s');
            $inicio = str_replace('/', '-', $inicio);
            
            $inicio = new DateTime ($inicio);
            
            $fin = date_create_from_format('d/m/Y H:i:s', $fin);
            $fin = date_format($fin, 'd/m/Y H:i:s');
            $fin = str_replace('/', '-', $fin);
            
            $fin = new DateTime ($fin);

            $diferencia = $inicio->diff($fin);
            $diferencia = $diferencia->format('%R%a');
            if(strlen($diferencia) > 2 || $diferencia[1] > 7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }*/
    }
