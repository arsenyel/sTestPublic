<?php
namespace Core\Helper;

class Arrays
{
    public static function isAssociative(array $array)
    {
        if (array() === $array)
        {
            return false;
        }
        return array_keys($array) !== range(0, count($array) - 1);
    }
    
    public static function convertToObject($array)
    {
        self::convertToObjectHelper($array);
        
        return $array;
    }
    
    public static function convertToObjectHelper(&$array)
    {
        foreach($array as $key => $value)
        {
            if(is_array($value))
            {
                self::convertToObjectHelper($array[$key]);
            }
        }
        if(self::isAssociative($array))
        {
            $array = (object)$array;
        }
    }
}
