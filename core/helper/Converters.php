<?php
namespace Core\Helper;

use \Exception;
use \DateInterval;
use MongoDB\BSON\UTCDateTime;
use MongoDB\BSON\ObjectID;

class Converters
{
    public static function stringToObjectId($id = '')
    {
        if ($id !== '')
        {
            if (strlen($id) == 24)
            {
                return new ObjectID($id);
            }
            else
            {
                throw new Exception("La cadena no se puede convertir en ObjectID", 1);
            }
        }
        else
        {
            throw new Exception("El parametro no puede estar vacio", 1);
        }   
    }

    /**
     * @param string $string String con el formato de fecha siguiente "d/m/Y H:i:s"
     * @param string $type String con el tipo de fecha que se necesita
     *     @option 'Fecha Completa'
     *     @option 'Fecha Simple'
     *     @option 'Fecha Corta'
     * @param string $agregar
     * @param string $agregarTipo
     * @return UTCDateTime;
     */
    public static function stringToDate($string = '', $type = 'Fecha Completa', $agregar = '', $agregarTipo = '')
    {
        $temp = $string;

        if ($string == '')
        {
            $string = date('d/m/Y H:i:s');
        }

        if ($type == 'Fecha Completa' || $type == '' || $type === 'long')
        {
            $string = date_create_from_format('d/m/Y H:i:s', $string);

            $string = date_format($string, 'd/m/Y H:i:s');
            
            $string = str_replace('/', '-', $string);
        }

        if ($type == 'Fecha Simple' || $type === 'simple')
        {
            $string = date_create_from_format('d/m/Y', $string);
            
            if ($agregar != '')
            {
                $intervalo = 'PT24H';

                $string->add(new DateInterval($intervalo));
            }

            $string = date_format($string, 'd/m/Y');
            
            $string = str_replace('/', '-', $string);
        }

        if ($type == 'Fecha Corta')
        {
            $string = date_create_from_format('Y-m-d', $string);
            
            $string = date_format($string, 'Y-m-d');
        }
        
        $string = strtotime($string) * 1000;
        
        if ($agregar != '')
        {
            switch (strtolower(str_replace(' ', '', $agregarTipo)))
            {
                case 'segundo':
                case 'segundos':
                case 'second':
                case 'seconds':
                    $string = $string + ($agregar * 1000);
                    break;
                case 'minuto':
                case 'minutos':
                case 'minute':
                case 'minutes':
                    $string = $string + ($agregar * 1000 * 60);
                    break;
                case 'hora':
                case 'horas':
                case 'hours':
                case 'hour':
                    $string = $string + ($agregar * 1000 * 60 * 60);
                    break;
                case '':
                case 'dia':
                case 'dias':
                case 'day':
                case 'days':
                    $string = $string + ($agregar * 1000 * 60 * 60 * 24);
                    break;
                case 'semana':
                case 'semanas':
                case 'week':
                case 'weeks':
                    $string = $string + ($agregar * 1000 * 60 * 60 * 24 * 7);
                    break;
                case 'mes':
                case 'meses':
                case 'month':
                case 'months':
                    $string = $string + ($agregar * 1000 * 60 * 60 * 24 * 30);
                    break;
                case 'año':
                case 'años':
                case 'year':
                case 'years':
                    $string = $string + ($agregar * 1000 * 60 * 60 * 24 * 365);
                    break;
                default:
                    /*Milisegundos*/
                    $string = $string + $agregar;
                    break;
            }
        }
        
        return new UTCDateTime($string);            
    }

    public static function mongoDateToString($date, $largo = true)
    {
        Dates::setTimeZone();

        $date = $date->toDateTime();

        if (Dates::getTimeZome() > 0)
        {
            $intervalo = 'PT' . Dates::getTimeZome() . 'H';

            $date->add(new DateInterval($intervalo));
        }

        if (Dates::getTimeZome() < 0)
        {
            $intervalo = 'PT' . (Dates::getTimeZome() * (-1)) . 'H';

            $date->sub(new DateInterval($intervalo));
        }

        if ($largo === true)
        {
            $date = $date->format('d/m/Y H:i:s');
        }
        else
        {
            $date = $date->format('d/m/Y');
        }

        return $date;
    }
    
    public static function epochDateToStringDate($date, $miliseconds = true)
    {
        /*DateEpochToStringDate*/
        $date = (int)$date;
        
        if ($miliseconds)
        {
            $date/=1000;
        }
        
        return date('d/m/Y H:i:s', $date);
    }
    
    public static function fullDateToFriendlyDate($date)
    {
        $date = explode(' ', $date);

        $fecha = $date[0];
        
        $horas = $date[1];
        
        return "$fecha a las $horas hs";
    }

    public static function decimalDegreesToGoogleMaps($latitude, $longitude)
    {
        $result = array(
            'latitude' => array(),
            'longitude' => array(),
            'letterLatitude' => 'N',
            'letterLongitude' => 'E'
        );

        $latitudeD = (double)$latitude;

        $longitudeD = (double)$longitude;

        if ($latitude[0] == '-')
        {
            $result['letterLatitude'] = 'S';

            $latitudeD *= (-1);
        }
        
        if ($longitude[0] == '-')
        {
            $result['letterLongitude'] = 'W';
    
            $longitudeD *= (-1);  
        }

        $result['latitude']['grades'] = (int)$latitudeD;

        $result['latitude']['minutes'] = (int)(($latitudeD - $result['latitude']['grades']) * 60);

        $result['latitude']['seconds'] = (int)(((($latitudeD - $result['latitude']['grades']) * 60) - $result['latitude']['minutes']) * 60);

        $result['latitude']['hundredths'] = (int)(((((($latitudeD - $result['latitude']['grades']) * 60) - $result['latitude']['minutes']) * 60) - $result['latitude']['seconds']) * 10);
        
        $result['longitude']['grades'] = (int)$longitudeD;

        $result['longitude']['minutes'] = (int)(($longitudeD - $result['longitude']['grades']) * 60);

        $result['longitude']['seconds'] = (int)(((($longitudeD - $result['longitude']['grades']) * 60) - $result['longitude']['minutes']) * 60);

        $result['longitude']['hundredths'] = (int)(((((($longitudeD - $result['longitude']['grades']) * 60) - $result['longitude']['minutes']) * 60) - $result['longitude']['seconds']) * 10);
        
        return $result;
    }
    
    public static function sensorUnitConverter($originalUnit, $finalUnit, $value)
    {
        if($originalUnit === $finalUnit)
        {
            return $value;
        }
        
        switch($originalUnit)
        {
            case 'ºC':
                $value = $finalUnit === 'ºF' ? ($value*1.8)+32 : $finalUnit === 'K' ? $value+273.15 : $value;
            break;
            case 'cm':
                $value = $finalUnit === 'm' ? $value/100 : $finalUnit === 'dm' ? $value/10 : $value;
            break;
            case 'U':
                $value = ((int)$value === 1);
            break;
            case '%':
                /*No hay conversiones*/
            break;
            case 'L/min':
                $value = $finalUnit === 'L/s' ? $value/60 : $finalUnit === 'M³/s' ? ($value*1000)/60 : $value;
            break;
            case 'mmHg':
                $value = $finalUnit === 'atm' ? $value/760  : $finalUnit === 'hpa' ? $value*1.333224  : $finalUnit === 'psi' ? $value*0.0193367747 : $value;
            break;
            case 'rpm':
                $value = ($finalUnit === 'rps' || $finalUnit === 'hertz') ? $value/60 : $value;
            break;
        }
        
        return $value;
    }
    
    public function mongoDateCompare($dateSince, $dateUntil)
    {
        $dateSince = (float)(string)$dateSince;
        
        $dateUntil = (float)(string)$dateUntil;
        
        return $dateUntil>$dateSince;
    }
}
