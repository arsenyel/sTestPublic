<?php
namespace Core\Helper;

class Hardware
{
    public static function isConnected($url, $port)
    {
        $url = explode('://', $url);
        $url = end($url);
        $connected = @fsockopen($url, $port, $errno, $errstr, 10); 
        //website, port  (try 80 or 443)
        if ($connected)
        {
            $is_conn = true; //action when connected
            fclose($connected);
        }
        else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }
    
    public static function getSerialPortAvailable()
    {
        if (PHP_OS === 'Linux')
        {
            $output = shell_exec('dmesg | grep tty');

            $output = str_replace("\n", ' ', $output);

            $output = explode(' ', $output);

            $list = array();

            $temp_list = array();

            foreach ($output as $key => $item)
            {
                if ($item !== '')
                {
                    if (strpos($item, 'tty') !== FALSE)
                    {
                        $temp = str_replace('[', '', $item);
                        
                        $temp = str_replace(']', '', $temp);
                        
                        $temp = str_replace(':', '', $temp);
                        
                        if (!isset($temp_list[$temp]))
                        {
                            $list[] = $temp;
                        }
                        
                        $temp_list[$temp] = TRUE;
                    }
                }
            }

            return $list;
        }
    }
}
