<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Helper\Parser;
    use Core\Validators\DataType;

    require_once('Parser-stub.php');

    class ParserTest extends TestCase
    {
        public function testInstanceCorrectly()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $parser = new Parser($model->getMap());

            $this->assertNotEmpty($parser);
        }

        public function testSetData()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $data = Parser::setData($model, $model->getMap());

            $this->assertArrayHasKey("_id", $data);
        }

        public function testAddChild()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $nuevo = new stdClass();

            $nuevo->Numero = '2';

            $nuevo->Nombre = 'Contactor dos';

            $nuevo->Pulso = 5;

            $model->Conectores[] = $nuevo;

            $data = Parser::setData($model, $model->getMap());

            $this->assertCount(2, $data['Conectores']);
        }

        public function testAddChildWithoutChangeParent()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $nuevo = new stdClass();

            $nuevo->Numero = '2';

            $nuevo->Nombre = 'Contactor dos';

            $nuevo->Pulso = 5;

            $model->Conectores[] = $nuevo;

            $data = Parser::setData($model, $model->getMap());

            $this->assertEquals('1', $data['Estado']);
        }

        public function testAddAttributeNonRequired()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $data = Parser::setData($model, $model->getMap());

            $this->assertArrayHasKey('Ip', $data);
        }

        public function testAddAttributeNonRequiredAfterUpdate()
        {
            $model = new \ModeloStub();

            $model = setValues($model);

            $model->Serial = 'ttyS0';

            unset($model->Ip);

            $data = Parser::setData($model, $model->getMap());

            $this->assertArrayNotHasKey('Ip', $data);

            $this->assertArrayHasKey('Serial', $data);
        }

        public function testChargeDataFromRequest()
        {
            $request = array(
                'id' => '58947b753744f562c81fc5d1',
                'name' => "Grupo 2",	        
                'ip' => "192.168.0.155",
                'port' => '80',
                'status' => TRUE,
                'user' => "admin",
                'password' => "8146dpr",
                'connectors' => array(
                    array(
                        'number' => 1,
                        'name' => "Contactor dos",
                        'pulse' => 5
                    )
                )
            );

            $model = new \ModeloStub();

            $model = Parser::fastCharge($model, $request, $model->args, $model->getMap());

            $this->assertObjectHasAttribute('Nombre', $model);
        }

        public function testChargeDataFromRequestWithSeveralsChildrens()
        {
            $request = array(
                'id' => '58947b753744f562c81fc5d1',
                'name' => "Grupo 2",	        
                'ip' => "192.168.0.155",
                'port' => '80',
                'status' => TRUE,
                'user' => "admin",
                'password' => "8146dpr",
                'connectors' => array(
                    array(
                        'number' => 1,
                        'name' => "Contactor uno",
                        'pulse' => 5
                    ),
                    array(
                        'number' => 2,
                        'name' => 'Contactor dos',
                        'pulse' => 6
                    )
                )
            );

            $model = new \ModeloStub();

            $model = Parser::fastCharge($model, $request, $model->args, $model->getMap());

            $this->assertCount(2, $model->Conectores);
        }
        
        public function testNumberToLetter()
        {
            $this->assertTrue(Parser::numberToLetter(1) == 'A');
            $this->assertTrue(Parser::numberToLetter(10) == 'J');
            $this->assertTrue(Parser::numberToLetter(-10) == '');
            $this->assertTrue(Parser::numberToLetter(27) == 'AA');
            $this->assertTrue(Parser::numberToLetter(28) == 'AB');
        }
        
        public function testSetMongoData()
        {
            $request = array(
                'pop' => array(
                    'anf' => '0'
                ),
                'smtp' => array(
                    'host' => '1',
                    'puerto' => '2'
                ),
                'mqtt' => array(),
                'asterisk' => 'false'
            );
            
            $result = Parser::setMongoData($request);
            
            $this->assertArrayHasKey('pop.anf', $result);
            $this->assertArrayHasKey('smtp.puerto', $result);
            $this->assertArrayHasKey('mqtt', $result);
            $this->assertArrayHasKey('asterisk', $result);
        }
    }
