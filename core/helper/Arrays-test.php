<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Helper\Arrays;
    
    class ArraysTest extends TestCase
    {
        public function testIsAssociative()
        {
            $test0 = array();
            
            $test1 = array(
                'a' => 'b'
            );
            
            $test2 = array('a');
            
            $test3 = array(
                array('a' => 'b')
            );
            
            $test4 = array(
                'a' => array(
                    'b' => 'c'
                ),
                'd' => ''
            );
            
            $this->assertFalse(Arrays::isAssociative($test0));
            
            $this->assertTrue(Arrays::isAssociative($test1));
            
            $this->assertFalse(Arrays::isAssociative($test2));
            
            $this->assertFalse(Arrays::isAssociative($test3));
            
            $this->assertTrue(Arrays::isAssociative($test4));
        }
        
        public function testConvertToObject()
        {
            $in = array(
                'a' => 'b',
                'c' => array('d'),
                'e' => array(
                    'f' => 'g'
                ),
                'h' => array(
                    'i' => array('j')
                )
            );
            
            $out = new \stdClass();
            
            $out->a = 'b';
            $out->c = array('d');
            $out->e = new \stdClass();
            $out->e->f = 'g';
            $out->h = new \stdClass();
            $out->h->i = array('j');
            
            $this->assertEquals(Arrays::convertToObject($in), $out);
        }
    }
