<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Helper\Converters;
    
    class ConvertersTest extends TestCase
    {
        public function testStringToDate()
        {
            /*created*/
            $date1 = Converters::stringToDate('29/05/1986 12:05:50');
            
            /*updated*/
            $date2 = Converters::stringToDate('30/05/1986 12:05:50');
            
            /*due*/
            $date3 = Converters::stringToDate('31/05/1986 12:05:50');
            
            $this->assertTrue(Converters::mongoDateCompare($date1, $date2));
            
            $this->assertTrue(Converters::mongoDateCompare($date2, $date3));
            
            $this->assertTrue(Converters::mongoDateCompare($date1, $date3));
            
            $this->assertFalse(Converters::mongoDateCompare($date1, $date1));
            
            $this->assertFalse(Converters::mongoDateCompare($date2, $date1));
        }
    }
