<?php
namespace Core\Helper;

use \stdClass;
use \Exception;
use Core\Mongo;
use Core\Helper\Converters;
use Core\Validators\DataType;
use MongoDB\BSON\ObjectID;

class Parser
{
    /**
    * consultar porque no se usa model en setdata para ARRAYS
    */
    public static function setData($object, $map, $model = NULL, $ignore = array())
    {
        $data = array();

        $keys = array_keys($map);

        if (isset($keys[0]))
        {
            if (is_numeric($keys[0]))
            {
                foreach ($object as $key => $item)
                {
                    $data[$key] = self::setData($item, $map[0], $model);
                }
            }
            else
            {
                foreach ($map as $name => $type)
                {
                    if (isset($object->$name))
                    {
                        if (gettype($type) === 'array')
                        {
                            $data[$name] = self::setData($object->$name, $type, $model, $ignore);
                        }
                        else
                        {
                            if ($model !== NULL)
                            {
                                $data[$name] = $model->specifyValue($name, $object->$name);
                            }
                            else
                            {
                                $data[$name] = $object->$name;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (count($map) > 0)
            {
                foreach ($map as $name => $type)
                {
                    if (isset($object->$name))
                    {
                        if (gettype($type) === 'array')
                        {
                            $data[$name] = self::setData($object->$name, $type, $model, $ignore);
                        }
                        else
                        {
                            if ($model !== NULL)
                            {
                                $data[$name] = $model->specifyValue($name, $object->$name);
                            }
                            else
                            {
                                $data[$name] = $object->$name;
                            }
                        }
                    }
                }
            }
            else
            {
                return $object;
            }
        }
        
        return $data;
    }

    public static function fastRender($object, $labels, $model = NULL)
    {
        return self::render($object, $labels, $model, FALSE);
    }

    public static function slowRender($object, $labels, $model = NULL)
    {
        return self::render($object, $labels, $model, TRUE);
    }

    public static function render($object, $labels, $model = NULL, $slow = FALSE)
    {
        $newObject = new stdClass();
        
        foreach ($labels as $name => $label)
        {
            try
            {
                $newName = $label['name'];
            }
            catch (Exception $e)
            {
                print_r('Atributo name invalido', $label);
            }

            if (isset($label['children']))
            {
                if (isset($object->$name))
                {
                    $keys = array_keys($label['children']);
                    
                    if (count($keys) > 0)
                    {
                        if (is_numeric($keys[0]))
                        {
                            foreach ($object->$name as $key => $item)
                            {
                                $newObject->$newName[$key] = self::render($item, $label['children'][0], $model, $slow);
                            }
                        }
                        else
                        {
                            $newObject->$newName = self::render($object->$name, $label['children'], $model, $slow);
                        }
                    }
                }
            }
            else
            {
                if (isset($label['relation']))
                {
                    if (isset($object->$name))
                    {
                        if ($slow)
                        {
                            if ($object->$name !== NULL && $object->$name !== '')
                            {
                                $newObject->$newName = self::_loadRelation($label['relation'], $object->$name, $model);
                            }
                        }
                        else
                        {
                            if ($object->$name !== NULL && $object->$name !== '')
                            {
                                if ($model !== NULL)
                                {
                                    $newObject->$newName = $model->howToShowValue($name, $object->$name);
                                }
                                else
                                {
                                    $newObject->$newName = $object->$name;
                                }
                            }  
                        }
                    }
                }
                elseif (isset($object->$name))
                {
                    if ($object->$name !== NULL && $object->$name !== '')
                    {
                        if ($model !== NULL)
                        {
                            $newObject->$newName = $model->howToShowValue($name, $object->$name);
                        }
                        else
                        {
                            $newObject->$newName = $object->$name;
                        }
                    }
                }
            }
        }

        return $newObject;
    }

    private static function _loadRelation($args, $value, $model = NULL)
    {
        $db = new Mongo('vSystem');

        $options = array(
            'limit' => 1
        );

        foreach ($args['fields'] as $key => $field)
        {
            $options['projection'][$field] = 1;
        }

        if (isset($args['db']))
        {
            $relation = $db->find($args['collection'], array(
                '_id' => $value
            ), $options);
            
            $relation = $relation->toArray();

            if (isset($relation[0]))
            {
                if ($model !== NULL)
                {
                    $relation = $model->setRelation($args['collection'], $relation[0]);
                }
                else
                {
                    $relation = $relation[0];
                }
            }
            else
            {
                $relation = array(
                    'id' => (string)$value
                );
            }
        }
        else
        {
            $class = "Models\\" . $args['model'];

            $temp = new $class();

            $options['_id'] = $value;

            $relation = $temp->findOne($options);

            if ($relation !== FALSE)
            {
                $relation = self::slowRender($relation, $temp->args, $temp);
            }
            else
            {
                $relation = array(
                    'id' => (string)$value
                );
            }
        }
        
        return $relation;
    }
    
    public function fastListCharge($request)
    {
        $list = array();
        
        foreach($request as $listElement)
        {
            if(isset($listElement['name'], $listElement['id']) && Strings::removeSpaces($listElement['name']) !== '' && DataType::validOid($listElement['id']))
            {
                array_push($list, array(
                    'Nombre' => Strings::prepareName($listElement['name']),
                    '_id' => new ObjectID($listElement['id'])
                ));
            }
        }
        
        return $list;
    }

    /**
     * @param object $object Is the object of the model
     * @param object $request Is the object of the request
     * @param array $args Is the args from the model
     * @param array $map Is the map of attributes of the model
     * @return object $object with some changes
     */
    public static function fastCharge($object, $request, $args, $map)
    {
        $request = is_object($request) ? $request : (object) $request;

        foreach ($args as $modelName => $options)
        {
            $name = $options["name"];

            if (isset($request->$name))
            {
                if (isset($options['children']))
                {
                    $keys = array_keys($options['children']);

                    if (is_numeric($keys[0]))
                    {
                        $object->$modelName = array();

                        foreach ($request->$name as $key => $item)
                        {
                            $object->$modelName[$key] = self::fastCharge(isset($object->$modelName[$key]) ? $object->$modelName[$key] : new stdClass(), $item, $options['children'][0], $map[$modelName][0]);
                        }
                    }
                    else
                    {
                        $object->$modelName = self::fastCharge(isset($object->$modelName) ? $object->$modelName : new stdClass(), $request->$name, $options['children'], $map[$modelName]);
                    }
                }
                else
                {
                    if ($map[$modelName] === 'ObjectId' || $map[$modelName] === '.ObjectId')
                    {
                        try
                        {
                            $object->$modelName = new ObjectID($request->$name);
                        } catch (Exception $e)
                        {
                            $var = $e->getMessage();
                        }
                    }
                    elseif ($map[$modelName] === 'string' || $map[$modelName] === '.string')
                    {
                        $object->$modelName = $request->$name;
                    }
                    elseif ($map[$modelName] === 'boolean' || $map[$modelName] === '.boolean')
                    {
                        if (gettype($request->$name) === 'string')
                        {
                            $object->$modelName = ($request->$name === 'true');
                        }
                        else
                        {
                            $object->$modelName = $request->$name;
                        }
                    }
                    elseif ($map[$modelName] === 'int' || $map[$modelName] === '.int')
                    {
                        $object->$modelName = (int)$request->$name;
                    }
                    elseif ($map[$modelName] === 'ISODate' || $map[$modelName] === '.ISODate')
                    {
                        $object->$modelName = Converters::stringToDate(date("d/m/Y H:i:s", strtotime(str_replace('/', '-', $request->$name))));
                    }
                    else
                    {
                        $object->$modelName = $request->$name;
                    }
                }
            }
        }
        
        return $object;
    }

    public static function getVarsFromUrl($string = '', $callback = null)
    {
        $items = array();

        foreach (explode('&', $string) as $key => $item)
        {
            $newItems = explode('=', $item);

            if ($callback !== null)
            {
                $items = $callback($items, $newItems[0], $newItems[1]);
            }
            else 
            {
                $items[$newItems[0]] = $newItems[1];
            }
        }

        return $items;
    }
    
    public static function numberToLetter($number)
    {
        $number = (int)$number-1;
        
        /*Se quitaron las lestras q pueden causar error de lectura - AHORA solo se quito la Ñ*/
        $letters = array('A','B','C','D','E','F','G','H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X','Y','Z');
        
        if ($number >= count($letters))
        {
            $pos = $number%count($letters);

            return self::numberToLetter((int)($number/count($letters))).$letters[$pos];
        }
        elseif (isset($letters[$number]))
        {
            return $letters[$number];
        }

        return '';
    }
    
    public static function clearEmptyArray($array)
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $array[$key] = self::clearEmptyArray($array[$key]);
            }
    
            if (empty($array[$key]))
            {
                unset($array[$key]);
            }
        }
    
        return $array;
    }
    
    public static function permissionsCharge(&$model, $request)
    {
        if (is_array($request) || is_object($request))
        {
            foreach ($request as $key=>$value)
            {
                if (!isset($model[$key]))
                {
                    $model[$key] = array();
                }
                
                if (isset($value['A']) || isset($value['B']) || isset($value['M']) || isset($value['C']) || isset($value['TND']) || isset($value['P']))
                {
                    foreach ($value as $perm=>$status)
                    {
                        if (isset($status['checked']) && $status['checked'] === 'true')
                        {
                            array_push($model[$key], $perm);
                        }
                    }
                }
                else
                {
                    self::permissionsCharge($model[$key], $value);
                }
            }
        }
    }
    
    public static function permissionsRender(&$tree)
    {
        foreach ($tree as $key => &$value)
        {
            if (is_array($value))
            {
                $aux = array();

                foreach ($value as $type)
                {
                    $aux[$type] = true;
                }

                $value = (object)$aux;
            }
            else
            {
                self::permissionsRender($value);
            }
        }
        
    }
    
    function setMongoDataHelper($data, $string, &$answer)
    {
        foreach($data as $key => $fact)
        {
            $nString = $string === '' ? $key : $string.'.'.$key;
            
            if(is_array($fact) && !empty($fact))
            {
                self::setMongoDataHelper($fact, $nString, $answer);
            }
            else
            {
                $answer[$nString] = $fact;
            }
        }
    }
    
    public static function setMongoData($data)
    {
        $answer = array();
        
        self::setMongoDataHelper($data, '', $answer);
        
        return $answer;
    }
    
    public static function hostToIp($host)
    {
        
        $host = explode('//', $host);
        
        $host = end($host);
        
        return gethostbyname($host);
    }
}
