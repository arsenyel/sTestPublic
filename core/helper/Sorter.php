<?php
namespace Core\Helper;
use Core\Mongo;
use Core;
use Core\Validators\DataType;
use MongoDB\BSON\ObjectID;

class Sorter
{
    private static function auxSortAssociationMatrixByColumnName(&$array, $subkey, $sort_ascending)
    {
        if (count($array))
            $temp_array[key($array)] = array_shift($array);
    
        foreach($array as $key => $val){
            $offset = 0;
            $found = false;
            foreach($temp_array as $tmp_key => $tmp_val)
            {
                if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
                {
                    $temp_array = array_merge(    
                        (array)array_slice($temp_array,0,$offset),
                        array($key => $val),
                        array_slice($temp_array,$offset)
                    );
                    $found = true;
                }
                $offset++;
            }
            if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
        }
    
        if ($sort_ascending) $array = array_reverse($temp_array);
    
        else $array = $temp_array;
    }
    
    public static function sortAssociationMatrixByColumnName($array, $subkey="label", $sort_ascending=true)
    {
        if(!empty($array))
        {
            self::auxSortAssociationMatrixByColumnName($array, $subkey, $sort_ascending);
            return $array;
        }
        else
        {
            return array();
        }
    }
}
