<?php
namespace Core\Helper;

class Request
{
    public static function getHost($host)
    {
        $url = explode('://', $url);
        $url = end($url);
        return gethostbyname($url);
    }
    
    public static function realClientIP()
    {
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0)
            {
                $addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);

                return trim($addr[0]);
            }
            else
            {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
        else
        {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    public static function browserName()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        if (strpos($userAgent, 'Opera') || strpos($userAgent, 'OPR/'))
        {
            return 'Opera';
        }
        elseif (strpos($userAgent, 'Edge'))
        {
            return 'Edge';
        }
        elseif (strpos($userAgent, 'Chrome'))
        {
            return 'Chrome';
        }
        elseif (strpos($userAgent, 'Safari'))
        {
            return 'Safari';
        }
        elseif (strpos($userAgent, 'Firefox'))
        {
            return 'Firefox';
        }
        elseif (strpos($userAgent, 'MSIE') || strpos($userAgent, 'Trident/7'))
        {
            return 'Internet Explorer';
        }
        else
        {
            return 'Otro';
        }
    }
}
