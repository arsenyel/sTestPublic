<?php
    namespace Core;
    
    use MongoDB\Driver\Manager;
    use MongoDB\Driver\Command;
    use MongoDB\Driver\Exception\Exception as MongoException;
    use MongoDB\Driver\Exception\BulkWriteException;
    use MongoDB\BSON\ObjectID;
    use MongoDB\BSON\UTCDateTime;
    use MongoDB\Driver\Query;
    use MongoDB\BSON\Javascript;
    use MongoDB\Driver\BulkWrite;
    
    use Core\Helper\Converters;
    
    class Mongo
    {
        protected $connector = null;

        protected $connected = false;
        
        protected $database = '';

        protected $user;

        protected $password;

        protected $host;
         
        function __construct($database = '', $user = '', $password = '', $host = '')
        {
            global $config;
            
            $this->database = $database !== '' ? $database : $config['DATABASE'];

            $this->user = $user !== '' ? $user : $config['DATABASE_USER'];
            
            $this->password = $password !== '' ? $password : $config['DATABASE_PASSWORD'];
            
            $this->host = $host !== '' ? $host : $config['DATABASE_HOST'];
            
            $this->connect();
        }
        
        private function connect()
        {
            if ($this->host !== '' && $this->host !== NULL)
            {
                try 
                {
                    if ($_ENV['ENVIROMENT'] === 'development')
                    {
                        $this->connector = new Manager('mongodb://'. $this->host, array(
                            'ssl' => FALSE
                        ));
                    }
                    else
                    {
                        $this->connector = new Manager('mongodb://'. $this->host);
                    }

                    $this->connected = true;

                }
                catch (MongoException $e)
                {
                    if($e->getCode() == 13053)
                    {
                        printf("Error: %s\n", 'Can\'t connect to database');
                        
                        exit();
                    }
                    else
                    {
                        printf("Error: %s\n", $e->getMessage());
                    }

                    $this->connected = false;

                    exit();
                }
            }
        }

        public function stringToJavascript($script, $scope = '')
        {
            if ($scope == '')
            {
                $script = new Javascript($script);
            }
            else
            {
                $script = new Javascript($script, $scope);
            }
            
            return $script;
        }
        
        public function executeCommand($command, $collectionDB, $safe = true)
        {
            try
            {
                $command = new Command($command);

                $datos = $this->connector->executeCommand($collectionDB, $command);

                return $datos;
            }
            catch (MongoException $e)
            {
                echo $e->getMessage(), "\n";
                
                if($safe)
                {
                    exit;
                }
            }
        }
        
        public function getCollStatsSize($collection, $db='')
        {
            $command = array(
                'collStats' => $collection
            );

            $answer = $this->executeCommand($command, $this->database);

            $answer = $answer->toArray();
            
            return (int)$answer[0]->count;
        }
        
        public function find($collection, $filter, $options = array())
        {
            if (!$this->connected)
            {
                $this->connect();
            }
            
            try
            {
                if (!empty($options))
                {
                    $query = new Query($filter, $options);
                }
                else
                {
                    $query = new Query($filter);
                }

                $datos = $this->connector->executeQuery($this->database . '.' . $collection, $query);             
       
                return $datos;
            }
            catch (Exception $e)
            {
                printf("Error: %s\n", $e->getMessage());

                exit;
            }
        }
        
        public function remove($where, $collection, $limit = 0)
        {
            if (!$this->connected)
            {
                $this->connect();
            }
            
            $bulk = new BulkWrite(['ordered' => true]);

            $db = '' . $this->database . '.' . $collection;
            
            if ((int)$limit === 1)
            {
                $bulk->delete($where, array(
                    'limit' => 1
                ));
            }
            else
            {
                $bulk->delete($where);
            }

            try 
            {
                $result = $this->connector->executeBulkWrite($db, $bulk);
            }
            catch (BulkWriteException $e)
            {
                $result = $e->getWriteResult();
            }
            catch (MongoException $e)
            {
                $result = $e->getMessage();
            }

            return $result;
        }
        
        public function update($where, $set, $extras, $collection)
        {
            if (!$this->connected) 
            {
                $this->connect();
            }
            
            $bulk = new BulkWrite(['ordered' => true]);

            $db = '' . $this->database . '.' . $collection;

            $bulk->update($where, $set, $extras);

            try 
            {
                $result = $this->connector->executeBulkWrite($db, $bulk);
            }
            catch (BulkWriteException $e)
            {
                $result = $e->getWriteResult();
            }
            catch (MongoException $e)
            {
                $result = $e->getMessage();
            }

            return $result;
        }
        
        public function insert($data, $collection)
        {
            $result = array();

            if (!$this->connected)
            {
                $this->connect();
            }
            
            $bulk = new BulkWrite(['ordered' => true]);

            $db = '' . $this->database . '.' . $collection;

            $bulk->insert($data);
            
            $inserted = false;
            
            try 
            {
                $result['result'] = $this->connector->executeBulkWrite($db, $bulk);

                $inserted = $this->find($collection, $data, array(
                    'projection' => array(
                        '_id' => 1
                    ),
                    'limit' => 1,
                    'sort' => array(
                        '_id' => -1
                    )
                ));
                
                $inserted = $inserted->toArray();

                $result['_id'] = $inserted[0]->_id;
            }
            catch (BulkWriteException $e)
            {
                $result['result'] = $e->getWriteResult();
            }
            catch (MongoException $e)
            {
                $result['result'] = $e->getMessage();
            }
            
            return $result;
        }
    }
