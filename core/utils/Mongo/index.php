<?php
namespace Core\Utils\Mongo;

use Core\Mongo;

class Maintenance
{
    public static function updateRAM()
    {
        global $config;

        $mongo = new Mongo($config['DATABASE'], $config['DATABASE_USER'], $config['DATABASE_PASSWORD'], $config['DATABASE_HOST']);

        $ramBytes = 50000000;

        $comando = array(
            'setParameter' => 1,
            'internalQueryExecMaxBlockingSortBytes' => $ramBytes
        );

        $db = 'admin';

        $mongo->executeCommand($comando, $db);

        return true;
    }
}
