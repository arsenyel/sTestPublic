<?php
require_once BASEPATH . '/vendor/autoload.php';

use Dotenv\Dotenv;

ini_set("session.cookie_lifetime",28800);

ini_set("session.gc_maxlifetime",28800);

if (!ini_get("auto_detect_line_endings")) {
    ini_set("auto_detect_line_endings", '1');
}
    
$dotenv = new Dotenv(BASEPATH);

$dotenv->load();

require_once 'App.php';

use Core\App;

///Loading connection stuff
require_once BASEPATH . '/config/database.php';

require_once BASEPATH . '/core/Mongo.php';

require_once BASEPATH . '/core/SqlServer.php';

require_once BASEPATH . '/core/Model.php';

require_once BASEPATH . '/core/autoload.php';

App::loadFilesFromDir(BASEPATH . '/helpers');

App::loadFilesFromDir(BASEPATH . '/models');

///end connection stuff
$app = new App();

App::loadFilesFromDir(BASEPATH . '/components');

require_once BASEPATH . '/core/Route.php';

App::loadFilesFromDir(BASEPATH . '/routes');
