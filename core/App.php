<?php
/**
 * Haciendo uso del motor de plantillas de https://github.com/jenssegers/blade
 */
namespace Core;

use Jenssegers\Blade\Blade;
use Klein\Klein;
use Core\Components\Session;
use MongoDB\BSON\ObjectID;

class View
{
    function __construct($globals = array())
    {
        $this->blade = new Blade('templates', 'cache');

        $this->globals = $globals;
    }

    public function render($name, $vars = array())
    {
        echo $this->blade->make($name, array_merge($this->globals, $vars));
    }
}

class Response
{
    private $body;

    function __construct()
    {
        $this->body = '';

        http_response_code(200);
    }

    public function code($code = 200)
    {
        http_response_code($code);

        return $this;
    }

    public function json($body)
    {
        header('Content-Type: application/json');
        
        $this->body = json_encode($body);

        return $this;
    }

    public function send()
    {
        echo $this->body;

        die();
    }
}

class App
{
    public $routes;

    function __construct()
    {
        $this->routes = new Klein();

        $this->checkHttpError();        

        $this->session = new Session();
    }

    public function checkHttpError() 
    {
        $this->routes->onHttpError(function ($code, $router) {
            $blade = new Blade('templates', 'cache');

            $router->response()->body($blade->make('errors.' . $code));
        });
    }

    public function run()
    {
        $this->routes->dispatch();
    }

    public function systemConf($model)
    {
        $this->conf = $model->findOne(array());
    }

    public function setRoute($method, $url, $callback)
    {
        $this->routes->respond($method, $url, $callback);
    }

    public function setResource($options)
    {
        $session = $this->session;

        $view = $this->getBlade();

        $klein = $this->routes;

        $routes = $options['routes'];

        $this->routes->with($options['name'], function () use ($klein, $view, $session, $routes)
        {
            foreach ($routes as $key => $route)
            {
                $callback = $route['callback'];
                
                $klein->respond($route['method'], $route['regex'], function ($request) use ($callback, $view, $session)
                {
                    return $callback($request, new Response(), $session);
                });
            }
        });
    }

    public function getBlade()
    {
        return new View(array(
            'lang' => function ($word)
            {
                $result = $word;

                if (isset($this->lang[$word]))
                {
                    $result = $this->lang[$word];
                }

                return $result;
            }
        ));
    }

    public static function loadFilesFromDir($directory = '')
    {
        $files = scandir($directory);

        foreach ($files as $key => $file)
        {
            if ($file !== '.' && $file !== '..')
            {
                $path = "$directory/$file";

                if (is_dir($path))
                {
                    self::loadFilesFromDir($path);
                }
                else
                {
                    if (file_exists($path))
                    {
                        require_once($path);
                    }
                }
            }
        }
    }
}
