<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Validators\DataType;
    use MongoDB\BSON\ObjectID;
    use MongoDB\BSON\UTCDateTime;

    class DataTypeTest extends TestCase
    {
        public function testCheckObjectIdFromNewObject()
        {
            $this->assertTrue(DataType::check('ObjectId', new ObjectID("569fd0b6e6bc4fdd583c9869")));
        }

        public function testCheckObjectIdFromString()
        {
            $this->assertFalse(DataType::check('ObjectId', "569fd0b6e6bc4fdd583c9869"));
        }

        public function testCheckObjectIdThatFails()
        {
            $this->assertFalse(DataType::check('ObjectId', "569fd0b6e6bc4fdd583c986"));

            $this->assertFalse(DataType::check('ObjectId', 569064583986321513002421));
        }

        public function testCheckObjectIDThatIsNotRequired()
        {
            $this->assertTrue(DataType::check('.ObjectId', NULL));
        }

        public function testCheckIsString()
        {
            $this->assertTrue(DataType::check('string', 'hola'));
        }

        public function testCheckIsNotString()
        {
            $this->assertFalse(DataType::check('string', 1));
        }

        public function testCheckThatStringIsNotRequired()
        {
            $this->assertTrue(DataType::check('.string', NULL));
        }

        public function testCheckInt()
        {
            $this->assertTrue(DataType::check('int', 1));

            $this->assertTrue(DataType::check('int', 0));

            $this->assertFalse(DataType::check('int', '1'));

            $this->assertFalse(DataType::check('int', ''));

            $this->assertFalse(DataType::check('int', null)); 
        }

        public function testCheckBoolean()
        {
            $this->assertFalse(DataType::check('boolean', 1));

            $this->assertFalse(DataType::check('boolean', 0));

            $this->assertFalse(DataType::check('boolean', '1')); 

            $this->assertFalse(DataType::check('boolean', '0'));

            $this->assertTrue(DataType::check('boolean', TRUE));

            $this->assertTrue(DataType::check('boolean', FALSE));
        }

        public function testCheckISODate()
        {
            $this->assertTrue(DataType::check('ISODate', new UTCDateTime(strtotime(date('d/m/Y H:i:s')) * 1000)));

            $this->assertTrue(DataType::check('.ISODate', new UTCDateTime(strtotime(date('d/m/Y H:i:s')) * 1000)));

            $this->assertTrue(DataType::check('.ISODate', NULL));

            $this->assertFalse(DataType::check('.ISODate', date('d/m/Y H:i:s')));
        }

        public function testRunValidation()
        {
            $stub = array(
                'name' => 'John',
                'id' => new ObjectID("569fd0b6e6bc4fdd583c9869"),
                'time' => new UTCDateTime(strtotime(date('d/m/Y H:i:s')) * 1000)
            );

            $map = array(
                'name' => 'string',
                'id' => 'ObjectId',
                'rol' => '.ObjectId',
                'time' => 'ISODate',
                'severals' => array()
            );

            $this->assertTrue(DataType::runValidation($map, $stub));
        }

        public function testRunValidationIsRequired()
        {
            $map = array(
                'name' => 'string',
                'id' => 'ObjectId',
                'rol' => 'ObjectId',
                'time' => 'ISODate',
                'severals' => array()
            );
            
            $stub = array(
                'name' => 'John',
                'id' => new ObjectID("569fd0b6e6bc4fdd583c9869"),
                'time' => new UTCDateTime(strtotime(date('d/m/Y H:i:s')) * 1000)
            );

            $this->assertEquals(DataType::runValidation($map, $stub), 'rol inválido.');
        }

        public function testRunValidationSecondLevel()
        {
            $map = array(
                'name' => 'string',
                'id' => 'ObjectId',
                'rol' => 'ObjectId',
                'time' => 'ISODate',
                'severals' => array(
                    array(
                        '_id' => 'ObjectID',
                        'name' => 'string'
                    )
                )
            );
            
            $stub = array(
                'name' => 'John',
                'id' => new ObjectID("569fd0b6e6bc4fdd583c9869"),
                'time' => new UTCDateTime(strtotime(date('d/m/Y H:i:s')) * 1000),
                'rol' => new ObjectID("569fd0b6e6bc4fdd583c9869"),
                'severals' => array(
                    array(
                        '_id' => '3s513sf51v35sd1v5s',
                        'name' => 'Prueba'
                    )
                )
                    );

            $this->assertEquals(DataType::runValidation($map, $stub), '_id inválido.');
        }
    }
?>
