<?php
    namespace Core\Validators;

    use Core\Mongo;
    use DateTime;
    use Models\VSystemModel;
    use Core\Validators\DataType;
    use MongoDB\BSON\ObjectID;
    
    class Shape
    {
        public static function validJson($json)
        {
            json_decode($json);

            return (json_last_error() == JSON_ERROR_NONE);
        }

        public static function validDate($date, $type = 'long')
        {
            /*
            long: d/m/Y H:i:s
            short: d/m/Y
            */
            if ($type === 'long')
            {
                $d = DateTime::createFromFormat('d/m/Y H:i:s', $date);

                if ($d && $d->format('d/m/Y H:i:s') === $date)
                {
                    return true;
                }
                else
                {
                    $d = DateTime::createFromFormat('d/m/Y G:i:s', $date);
                    
                    return $d && $d->format('d/m/Y G:i:s') === $date;
                }
                return false;
            }
            else
            {
                $d = DateTime::createFromFormat('d/m/Y', $date);
                
                return $d && $d->format('d/m/Y') === $date;
            }
        }
    }
