<?php
    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Core\Validators\Shape;

    class ShapeTest extends TestCase
    {
        public function testValidDate()
        {
            $this->assertTrue(Shape::validDate('12/12/2017 12:58:43', 'long'));
            
            $this->assertTrue(Shape::validDate('12/12/2017 12:58:43', 'long'));
            
            $this->assertTrue(Shape::validDate('12/12/2017', 'short'));
            
            $this->assertTrue(Shape::validDate('12/12/2017', 'short'));
        }
    }
?>
