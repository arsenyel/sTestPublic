<?php
    namespace Core\Validators;

    use MongoDB\BSON\UTCDateTime;
    use MongoDB\BSON\ObjectID;
    use Core\Mongo;

    class DataType
    {
        public static function runValidation($map, $value)
        {
            $keys = array_keys($map);

            if (count($keys) > 0)
            {
                if (is_numeric($keys[0]))
                {
                    foreach ($value as $key => $object)
                    {
                        return self::runValidation($map[0], $object);
                    }
                }
                else
                {
                    foreach ($map as $name => $type)
                    {
                        if (gettype($type) === 'array')
                        {
                            if (isset($value[$name]))
                            {
                                return self::runValidation($type, $value[$name]);
                            }
                        }
                        else
                        {
                            $temp = NULL;

                            if (isset($value[$name]))
                            {
                                $temp = $value[$name];
                            } 

                            if (!self::check($type, $temp))
                            {
                                return "$name inválido.";
                            }
                        }
                    }
                }
            }

            return TRUE;
        }

        public static function check($type, $value)
        {
            if ($type === 'ObjectId')
            {
                return self::_checkObjectId($value);
            }
            elseif ($type === '.ObjectId')
            {
                if ($value !== NULL && $value !== '')
                {
                    return self::_checkObjectId($value);
                }
                else
                {
                    return TRUE;
                }
            }
            elseif ($type === 'ISODate')
            {
                return self::_checkIsoDate($value);
            }
            elseif ($type === '.ISODate')
            {
                if ($value !== NULL)
                {
                    return self::_checkIsoDate($value);
                }
                else
                {
                    return TRUE;
                }
            }
            elseif ($type === 'string')
            {
                return self::_checkString($value);  
            }
            elseif ($type === '.string')
            {
                if ($value !== NULL)
                {
                    return self::_checkString($value);
                }
                else
                {
                    return TRUE;
                }
            }
            elseif ($type === 'int')
            {
                return self::_checkInt($value);
            }
            elseif ($type === '.int')
            {
                if ($value !== NULL)
                {
                    return self::_checkInt($value);
                }
                else
                {
                    return TRUE;
                }
            }
            elseif ($type === 'boolean')
            {
                return self::_checkBoolean($value);
            }
            elseif ($type === '.boolean')
            {
                if ($value !== NULL)
                {
                    return self::_checkBoolean($value);
                }
                else
                {
                    return TRUE;
                }
            }

            return FALSE;
        }
        
        public static function validOid()
        {
            $args = func_get_args();

            if (empty($args))
            {
                return FALSE;
            }

            foreach ($args as $string)
            {
                if (strlen($string) != 24 || !ctype_xdigit((string)$string))
                {
                    return FALSE;
                }
            }

            return TRUE;
        }

        public static function validateAdvancedOid($id = NULL, $collection = '', $status = FALSE, $database = '')
        {
            if ($id !== NULL)
            {
                if (is_object($id) || (strlen($id) == 24 && ctype_xdigit((string)$id)))
                {
                    $mongo = $database === '' ? new Mongo() : new Mongo($database);
                    
                    if(!is_object($id))
                    {
                        $id = new ObjectID($id);
                    }

                    if (self::check('ObjectId', $id))
                    {
                        $filtro = array(
                            '_id' => $id
                        );

                        if ($collection !== '')
                        {
                            $result = $mongo->find($collection, $filtro, array(
                                'projection' => array(
                                    '_id' => 1
                                )
                            ));

                            $result = $result->toArray();

                            return (isset($result[0]));
                        }
                        else
                        {
                            return FALSE;
                        }
                    }
                    else
                    {
                        return FALSE;
                    }
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }

        private static function _checkObjectId($value)
        {
            if (gettype($value) === 'object')
            {
                if ($value instanceof ObjectID)
                {
                    return TRUE;
                }
            }

            return FALSE;
        }

        private static function _checkIsoDate($value)
        {
            if ($value instanceof UTCDateTime)
            {
                return TRUE;
            }

            return FALSE;
        }

        private static function _checkString($value)
        {
            if (gettype($value) === 'string')
            {
                return TRUE;
            }

            return FALSE;
        }

        private static function _checkInt($value)
        {
            if (gettype($value) === 'integer')
            {
                return TRUE;
            }

            return FALSE;
        }

        private static function _checkBoolean($value)
        {
            if (gettype($value) === 'boolean')
            {
                return TRUE;
            }

            return FALSE;
        }
    }
