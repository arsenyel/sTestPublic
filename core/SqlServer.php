<?php
    namespace Core;

    class SqlServer
    {
        protected $connector = null;

        protected $connected = false;
        
        protected $parameters = '';
         
        function __construct($parameters = ['database' => '', 'host' => '', 'user' => '', 'password' => '', 'port' => ''])
        {
            $this->parameters = $parameters;
            
            $this->connect();
        }
        
        private function connect()
        {
            if ($this->parameters['host'] !== '')
            {
                try 
                {
                    $server = ($this->parameters['port'] != '') ? $this->parameters['host'].','.$this->parameters['port'] : $this->parameters['host'];
                    
                    $this->connector = sqlsrv_connect(
                        $server, 
                        array(
                            'Database' => $this->parameters['database'],
                            'Uid' => $this->parameters['user'],
                            'PWD' => $this->parameters['password']
                        )
                    );
                    
                    if($this->connector !== false)
                    {
                        $this->connected = true;
                    }
                    
                }
                catch (Exception $e)
                {
                    printf("Error: %s\n", $e->getMessage());
                    
                    $this->connected = false;

                    exit();
                }
            }
        }
        
        public function close()
        {
            return sqlsrv_close($this->connector);
        }

        public function consult($parameters)
        {
            $parameters = array_merge(array('query' => '', 'type' => 'consult', 'function' => ''), $parameters);
            
            if (!$this->connected)
            {
                $this->connect();
            }
            
            $answer = array('state' => false);
            
            try
            {
                $getResults= sqlsrv_query( $this->connector, $parameters['query'] );
                
                if ( $getResults == FALSE )
                {
                    foreach(sqlsrv_errors() as $error)
                    {
                        if(is_array($error))
                        {
                            $error = isset($error['message']) ? $error['message'] : implode(' ', $error);
                        }
                        echo("$error\n");
                    }
                    exit();
                }
                
                if($parameters['type'] == 'massiveConsult')
                {
                    while ( $row = sqlsrv_fetch_array( $getResults, SQLSRV_FETCH_ASSOC )) {
                        $parameters['function']($row);
                    }
                    
                    $answer['state'] = true;
                }
                elseif($parameters['type'] == 'consult')
                {
                    $answer['rows'] = array();
                    
                    $answer['state'] = true;
                    
                    while ( $row = sqlsrv_fetch_array( $getResults, SQLSRV_FETCH_ASSOC )) {
                        $answer[] = $row;
                    }
                    
                }
                else
                {
                    $answer['state'] = false;
                }
                
                sqlsrv_free_stmt( $getResults );
                
                
            }
            catch (Exception $e)
            {
                printf("Error: %s\n", $e->getMessage());

                exit;
            }
            
            return $answer;
        }
        
        public function Erase($where, $coleccion, $limite = '')
        {
            
        }
        
        public function Update($where, $set, $extras, $coleccion)
        {

        }
        
        public function Insert()
        {
            
        }
    }
