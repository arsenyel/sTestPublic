<?php
    namespace Core\Cli\Commands;

    use Ratchet\Server\IoServer;
    use Ratchet\Http\HttpServer;
    use Ratchet\WebSocket\WsServer;
    use Ratchet\MessageComponentInterface;
    use Ratchet\ConnectionInterface;
    use SplObjectStorage;
    use Exception;
    use Core\Cli\Command;
    use Components\WebSocket;
    use Components\WebSocketMessage;
    use Components\WebSocketConnect;
    use Core\Helper\Dates;
    use Core\Validators\Shape;

    class WebSocketServer implements MessageComponentInterface
    {
        protected $clients;
        
        protected $options;
        
        public $connectionTokens = array();
        
        function __construct($options)
        {
            $this->clients = new SplObjectStorage;
            $this->options = $options;
        }
    
        public function onOpen(ConnectionInterface $conn)
        {
            // Store the new connection to send messages to later
            
            $type = isset($this->options['-m']) ? $this->options['-m'] : 'default';
            
            $this->connectionTokens[$conn->resourceId] = new \stdClass();
            
            $this->connectionTokens[$conn->resourceId]->valid = WebSocketConnect::connect($type);
            
            $this->connectionTokens[$conn->resourceId]->date = time();
            
            $this->connectionTokens[$conn->resourceId]->id = $conn->resourceId;
            
            $this->connectionTokens[$conn->resourceId]->ip = $conn->remoteAddress;
            
            $this->connectionTokens[$conn->resourceId]->user = '';
            
            $this->clients->attach($conn);
            
            echo "New connection! ({$conn->resourceId})\n";
        }
    
        public function onMessage(ConnectionInterface $from, $msg)
        {
            $type = isset($this->options['-m']) ? $this->options['-m'] : 'default';
            
            $ip = $from->remoteAddress;
            
            $reconnect = false;
            
            if(isset($this->connectionTokens[$from->resourceId]) && $this->connectionTokens[$from->resourceId]->ip === $ip && ($this->connectionTokens[$from->resourceId]->valid === true || ($reconnect = WebSocketConnect::reconnect($msg, $type, $ip)) === true))
            {
                $json = Shape::validJson($msg) ? json_decode($msg) : new \stdClass();
                
                $this->connectionTokens[$from->resourceId]->valid = true;
                
                if(isset($json->user))
                {
                    $this->connectionTokens[$from->resourceId]->user = $json->user;
                }
                
                if($reconnect === false)
                {
                    if(isset($this->options['-m']))
                    {
                        $clean = !(isset($this->connectionTokens[$from->resourceId]->ip) && $this->connectionTokens[$from->resourceId]->ip === '127.0.0.1');
                        
                        WebSocketMessage::process($this->options['-m'], $msg, $clean);
                    }
                    
                    $senderCount = 0;
                    
                    if($msg !== '')
                    {
                        foreach ($this->clients as $client)
                        {
                            if ($from !== $client && isset($this->connectionTokens[$client->resourceId]) && $this->connectionTokens[$client->resourceId]->valid === true)
                            {
                                if(!isset($json->to) || $json->to === $this->connectionTokens[$client->resourceId]->user)
                                {
                                    $client->send($msg);
                                    
                                    $senderCount++;
                                }
                            }
                        }
                    }
                    
                    $s = $senderCount == 1 ? '' : 's';
                    
                    $msg = substr($msg, 0, 20);
                    
                    echo "Connection {$from->resourceId}\\{$this->connectionTokens[$from->resourceId]->user} sending message {$msg} to {$senderCount} other connection{$s}\n";
                    
                }
            }
            else
            {
                echo "\nConnection {$from->resourceId} can't send message {$msg}";
            }
        }
    
        public function onClose(ConnectionInterface $conn)
        {
            // The connection is closed, remove it, as we can no longer send it messages
            $this->clients->detach($conn);
            
            unset($this->connectionTokens[$conn->resourceId]);
    
            echo "\nConnection {$conn->resourceId} has disconnected\n";
        }
    
        public function onError(ConnectionInterface $conn, Exception $e)
        {
            echo "An error has occurred: {$e->getMessage()}\n";
    
            $conn->close();
        }
    }    

    class WebSocketCommand extends Command
    {
        public function run($options)
        {
            if (isset($options['-p']))
            {
                if ($options['-p'] !== TRUE || $options['-p'] !== '')
                {
                    $server = IoServer::factory(
                        new HttpServer(
                            new WsServer(
                                new WebSocketServer($options)
                            )
                        ), (int)$options['-p']);

                    $server->run();
                }
                else
                {
                    throw new Exception('The port is not defined.');
                }
            }
            else
            {
                throw new Exception('The flag -p is no present.');
            }
        }
    }
?>
