<?php
    namespace Core\Cli\Commands;
    use Core\Cli\Command;
    use Components\Files\Json;
    
    class AutoloadCommand extends Command
    {
        public function run($options)
        {
            $parameters = array();
            
            if (isset($options['-db']) && $options['-db'] !== '')
            {
                $parameters['database'] = $options['-db'];
            }
            
            Json::autoload($parameters);
        }
    }
?>
