<?php
namespace Models;

use Core\Model;
use MongoDB\BSON\ObjectID;
use Core\Validators\Shape;
use Core\Helper\Converters;
use Core\Helper\Parser;
/**
* 
*/
class TasksModel extends Model
{
    protected $collection = 'Tasks';
    
    public $completed = false;
    
    protected $map = array(
        '_id' => 'ObjectId',
        'title' => 'string',
        'description' => '.string',
        'due_date' => 'ISODate',
        'completed' => '.boolean',
        'created_at' => '.ISODate',
        'updated_at' => '.ISODate'
    );
    
    public $args = array(
        '_id' => array(
            'name' => 'id'
        ),
        'title' => array(
            'name' => 'title'
        ),	        
        'description' => array(
            'name' => 'description'
        ),
        'due_date' => array(
            'name' => 'due_date'
        ),
        'completed' => array(
            'name' => 'completed'
        ),
        'created_at' => array(
            'name' => 'created_at'
        ),
        'updated_at' => array(
            'name' => 'updated_at'
        )
    );
    
    public function validate($ignore = array(), $data = array())
    {
        if ((string)$this->due_date === '0')
        {
            return 'invalid due_date';
        }
        
        if (isset($this->created_at) && (string)$this->created_at === '0')
        {
            return 'invalid created_at';
        }
        
        if (isset($this->updated_at) && (string)$this->updated_at === '0')
        {
            return 'invalid updated_at';
        }
        
        if (isset($this->created_at) && Converters::mongoDateCompare($this->created_at, $this->due_date) === false)
        {
            return 'due_date must be greater than created_at';
        }
        
        if (isset($this->updated_at, $this->created_at) && Converters::mongoDateCompare($this->created_at, $this->updated_at) === false)
        {
            return 'updated_at must be greater than created_at';
        }
        
        return TRUE;
    }

    public function howToShowValue($name, $value)
    {
        if ($name === '_id')
        {
            $value = (string)$value;
        }
        if ($name === 'due_date' || $name === 'created_at' || $name === 'updated_at')
        {
            $value = Converters::mongoDateToString($value);
        }
        
        return $value;
    }

    public function specifyValue($name, $value)
    {
        if ($name == 'Usuario')
        {
            $value = Strings::removeInitEndSpaces($value);
        }
        
        return $value;
    } 

}
