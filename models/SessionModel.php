<?php
namespace Models;

use Core\Model;

/**
* 
*/
class SessionModel extends Model
{
    protected $collection = 'Sesiones';

    protected $map = array(
        '_id' => 'ObjectId',
        'Usuario' => 'string',	        
        'Navegador' => 'string',
        'PrimerActividad' => 'string',
        'UltimaActividad' => 'string',
        'UltimaActividadMongo' => 'ISODate',
        'Rol' => '.ObjectId',
        'IP' => 'string',
        'URI' => 'string',
        'Puesto' => '.ObjectId'
    );

    public $args = array(
        '_id' => array(
            'name' => 'id'
        ),
        'Usuario' => array(
            'name' => 'username'
        ),	        
        'Navegador' => array(
            'name' => 'browser'
        ),
        'PrimerActividad' => array(
            'name' => 'firstActivity'
        ),
        'UltimaActividad' => array(
            'name' => 'lastActivity'
        ),
        'UltimaActividadMongo' => array(
            'name' => 'lastActivityMongo'
        ),
        'Rol' => array(
            'name' => 'role',
            'relation' => array(
                'model' => 'RoleModel',
                'fields' => array(
                    'Rol'
                )
            )
        ),
        'IP' => array(
            'name' => 'ip'
        ),
        'URI' => array(
            'name' => 'uri'
        ),
        'Puesto' => array(
            'name' => 'position',
            'relation' => array(
                'model' => 'PositionModel',
                'fields' => array(
                    'Puesto',
                    'Locacion'
                )
            )
        )
    );

    public function howToShowValue($name, $value)
    {
        if ($name === '_id' || $name === 'Rol')
        {
            $value = (string)$value;
        }
        
        return $value;
    }
}
