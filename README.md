# sTest

## Install

### Apache 2.4 with PHP 7.1 
PHP 7.1 Mod
XSendFile Mod
```bash
sudo apt install -y php7.1 apache2 libapache2-mod-php7.1 libapache2-mod-xsendfile
```

### PHP 7.1 Dependencies
Composer [Link](https://getcomposer.org/download/)
Memcached [Easy install](docs/memcached/README.md)
MongoDB Driver [Pecl install](http://php.net/manual/en/mongodb.installation.pecl.php)
mosquitto (Only if working with MQTT)

mbstring
json
curl
gd
pear
pecl
```
sudo apt install curl php7.1-curl php7.1-json php7.1-dev php7.1-gd php-pear php-dev php7.1-zip unzip php7.1-mbstring
```
**NOTE:** If haven't got Apache2.4 and PHP7.1 repository
```bash
sudo add-apt-repository ppa:ondrej/php
sudo add-apt-repository ppa:ondrej/apache2
sudo apt-get update
```

**NOTE:** Composer also can be installed from ubuntu repository 
```bash
sudo apt install composer
```

### 1: clone repository

```bash
git clone git@gitlab.com:arsenyel/sTest.git

cd sTest
git checkout dev
```

### 2: Run composer

```bash
composer update
```

### 4: Edit file `.env.example` a save as `.env`

In this step configure the database server options.

### 5: Change permissions of cache directory

```bash
mkdir cache
sudo chown "username":www-data -Rf cache/
```

**Note:** the "username" of the OS and without quotes.
**NOTE:** if in dev then use chmod with param 777

### 6: Add the vhost

[Follow the next steps](docs/vhost/README.md)

## For development

### Dev enviroment

### Test enviroment
To run the unit test for PHP:
#### PHPUnit from phar installation
```bash
phpunit
```
#### PHPUnit from composer installation
```bash
./vendor/bin/phpunit
```

