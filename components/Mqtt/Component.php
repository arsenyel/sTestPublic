<?php
    namespace Components\Mqtt;
    
    use Core\Helper\Encryption;
    use Mosquitto\Client;
    use Mosquitto\Exception;
    
    class Mqtt
    {
        public static function publish($parameters = array())
        {
            $parameters = array_merge(array('message' => '', 'topic' => '', 'qos' => 2, 'persistence' => false, 'host' => 'localhost', 'port' => 1883, 'encrypt' => true), $parameters);
            
            if($parameters['message'] != '' && $parameters['encrypt'])
            {
                $parameters['message'] = Encryption::encrypt($parameters['message']);
            }
            
            $connection = new Client;
            
            $connection->onConnect(function() use ($connection, $parameters) {
                $connection->publish($parameters['topic'], $parameters['message'], (int)$parameters['qos'], $parameters['persistence']);
            });
            
            $connection->connect($parameters['host'], $parameters['port']);
            
            for ($i = 0; $i < 100; $i++) {
                $connection->loop(1);
            }
            
            return true;
        }
        
        public static function cleanTopic($topic)
        {
            self::publish(array('message' => '', 'topic' => $topic, 'qos' => 2, 'persistence' => true));
        }
        
        public function process($parameters = array())
        {
            $parameters = array_merge(array('message' => '', 'decrypted' => '', 'topic' => '', 'id' => '', 'qos' => 0, 'persistence' => false), $parameters);
            
            $aux = $parameters['persistence'] ? 'Si' : 'No';
            
            echo ("\n Mensaje: {$parameters['message']}, Desencriptado: {$parameters['decrypted']} Topico: {$parameters['topic']}, ID: {$parameters['id']}, QoS: {$parameters['qos']}, Persistencia: $aux");
        }
        
        public function listen($parameters = array())
        {
            $parameters = array_merge(array('qos' => 2, 'topic' => '#', 'host' => 'localhost', 'port' => 1883, 'uniqueMessage' => false), $parameters);
            
            $connection = new Client;
            
            $connection->onMessage(function($message) use ($connection, $parameters)
            {
                if($message->payload == '')
                {
                    $payload = 'Mensaje de limpieza de topico';
                }
                
                $newParameters = array_merge(
                    $parameters, 
                    array(
                        'message' => $message->payload, 
                        'decrypted' => Encryption::decrypt($message->payload), 
                        'topic' => $message->topic, 
                        'id' => $message->mid, 
                        'qos' => $message->qos, 
                        'persistence' => $message->retain
                    )
                );
                
                get_called_class()::process($newParameters);
                
                if($newParameters['persistence'])
                {
                    self::cleanTopic($newParameters['topic']);
                }
                if($newParameters['uniqueMessage'])
                {
                    $connection->disconnect();
                }
            });
            
            $connection->connect($parameters['host'],$parameters['port']);
            
            $connection->subscribe($parameters['topic'], $parameters['qos']);
            
            for ($i = 0; $i < 10; $i++) {
                try
                {
                    $connection->loopForever(1);
                }
                catch (Exception $e)
                {
                    
                }
            }
        }
    }
