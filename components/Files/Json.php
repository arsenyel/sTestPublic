<?php

namespace Components\Files;

use Core\Mongo;
use Core\Model;
use Core\Helper\Sorter;
use Components\Autoload\Collections;
use Core\Validators\Shape;

class Json
{
    public static function autoload()
    {
        $mongo = new Mongo();
        
        $dir = substr(__DIR__, 0, strrpos(__DIR__, 'vOne')) . 'vOne/jsons/database';
        
        $databases = scandir($dir);
        
        foreach($databases as $database)
        {
            if($database != '.' && $database != '..')
            {
                $collections = scandir("$dir/$database");
                
                foreach($collections as $collection)
                {
                    if($collection != '.' && $collection != '..')
                    {
                        $string = file_get_contents("$dir/$database/$collection");
                        
                        if(!Shape::validJson($string))
                        {
                            die("Revisar JSON $database/$collection");
                        }
                        
                        $json = json_decode($string, true);
                        
                        if($json !== null)
                        {
                            Collections::createCollection($mongo, $json);
                        }
                        else
                        {
                            echo ("\nError JSON $dir/$database/$collection\n");
                        }
                    }
                }
            }
        }
    }
}
?>
