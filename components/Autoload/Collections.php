<?php
namespace Components\Autoload;
use Core\Mongo;
use Core\Validators\DataType;
use MongoDB\BSON\ObjectID;

class Collections
{
    public static function createCollection($connection, $parameters)
    {
        $cmd = array(
            'create' => $parameters['collection']
        );
        
        $connection->executeCommand($cmd, $parameters['database'], false);
        
        $indexes = array();
        
        foreach($parameters['indexes'] as $index)
        {
            $indexes [] = $index;
        }
        
        $cmd = array(
            'createIndexes' => $parameters['collection'],
            'indexes' => $indexes
        );
        
        $connection->executeCommand($cmd, $parameters['database'], false);
        
        $mongo = new Mongo($parameters['database']);
        
        foreach($parameters['documents'] as $document)
        {
            self::prepareDocument($document);
            
            if(isset($document['_id']))
            {
                $mongo->update(
                    array('_id' => $document['_id']),
                    array('$set' => $document),
                    array('multi' => false, 'upsert' => true),
                    $parameters['collection']
                );
            }
            else
            {
                $mongo->insert($document, $parameters['collection']);
            }
        }
    }
    
    private static function prepareDocument(&$document)
    {
        foreach($document as $key=>&$value)
        {
            if(is_array($value))
            {
                if(isset($value['$oid']) && DataType::validOid($value['$oid']))
                {
                    $value = new ObjectID($value['$oid']);
                }
                elseif(!isset($value['$oid']))
                {
                    self::prepareDocument($value);
                }
            }
        }
    }
}
