<?php
    namespace Components\WebSocket;
    
    use Core\Mongo;
    use Core\Validators\Shape;
    use Core\Validators\DataType;
    use MongoDB\BSON\ObjectID;
    use Core\Helper\Converters;
    
    class Connect{
        public static function connect($type)
        {
            switch($type)
            {
                case 'default':
                    return true;
                break;
            }
            
            return false;
        }
        
        public static function reconnect($message, $type, $ip)
        {
            if(Shape::validJson($message))
            {
                $json = json_decode($message);
                
                $json->ip = $ip;
                
                if(isset($json->user, $json->password, $json->to) && $json->to === false)
                {
                    switch($type)
                    {
                        default:
                            return true;
                        break;
                    }
                }
            }
            
            return false;
        }
    }
