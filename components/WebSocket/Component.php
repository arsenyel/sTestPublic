<?php
    namespace Components\WebSocket;

    use WebSocket\Client;

    class WebSocket
    {
        public static function sendMessage($message, $client = 'ws://localhost:8181')
        {
            set_time_limit (10);

            $clienteWS = new Client($client);

            $mensajesWS = 0;

            try
            {
                if(is_array($message))
                {
                    for ($i = 0; $i < count($message); $i++)
                    {
                        if ($i > 0 && $i % 10 == 0)
                        {
                            usleep(5000);
                        }

                        $clienteWS->send($message[$i]);

                        $mensajesWS++;
                    }
                }
                else
                {
                    if ($mensajesWS !== 0 && $mensajesWS % 10 == 0)
                    {
                        usleep(5000);
                    }

                    $mensajesWS++;
                    
                    $respuesta = $clienteWS->send($message);
                }
                
            }
            catch(Exception $e)
            {
                echo ('Servidor de Web Socket no disponible');
                
                exit();
            }
        }
    }
