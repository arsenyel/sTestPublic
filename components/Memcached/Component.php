<?php
namespace Components\Memcached;
use \Memcached;

class Lists
{
    /**
    * @param string $mainList String of category
    * @param array $options This array will be used for a new search if not finded"
    * @return mixed;
    */
    public static function get($mainList, $options)
    {
        $mc = new \Memcached();
        
        $mc->addServer("localhost", 11211);
        
        if ($mc->get($mainList) !== false)
        {
            foreach ($mc->get($mainList) as $list)
            {
                if ($list['options'] === $options)
                {
                    return $list['result'];
                }
            }
            
            return false;
        }
        else
        {
            return false;
        }
    }
    
    /**
    * @param string $mainList String of category
    * @param array $options Used in search"
    * @param array $result Result of search"
    */
    public static function set($mainList, $options, $result)
    {
        $mc = new Memcached();
        
        $mc->addServer("localhost", 11211);
        
        $list = $mc->get($mainList) !== false ? $mc->get($mainList) :array();
        
        $list[] = array(
            'options' => $options,
            'ids' => self::getIds($result),
            'result' => $result
        );
        
        $mc->set($mainList, $list);
    }
    
    /**
    * @param string $mainList String of category
    * @param string $id Search elements with it will be erased"
    */
    public static function remove($mainList, $id = '')
    {
        $mc = new Memcached();
        
        $mc->addServer("localhost", 11211);
        
        $list = $mc->get($mainList) !== false ? $mc->get($mainList) :array();
        
        if ($id === '')
        {
            $list = array();
        }
        else
        {
            foreach ($list as $key=>$results)
            {
                if (isset($results['ids'][$id]))
                {
                    unset($list[$key]);
                }
            }
        }
        
        $mc->set($mainList, $list);
    }
    
    private static function getIds($result)
    {
        $ids = array();
        
        foreach ($result['data'] as $data)
        {
            $ids[$data->id] = true;
        }
        
        return $ids;
    }
}
