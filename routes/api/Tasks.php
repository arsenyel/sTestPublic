<?php
use Core\Route;
use Models\TasksModel;
use MongoDB\BSON\Regex;
use MongoDB\BSON\ObjectID;
use Core\Helper\Parser;
use Core\Validators\DataType;
use Core\Validators\Shape;
use Components\Memcached\Lists;

Route::api(array(
    'name' => '/api/tasks',
    'routes' => array(
        array(
            'method' => 'GET',
            'regex' => '/?',
            'callback' => function ($request, $response, $session)
            {
                $model = new TasksModel();
                
                $result = array(
                    'data' => array(),
                    'total' => 0
                );
                
                $page = (isset($request->page) && is_numeric($request->page) && (int)$request->page>0) ? (int)$request->page : 1;

                $options = array(
                    'skip' => ($page-1)*5,
                    'limit' => 5
                );
                
                if (isset($request->completed) && (strtolower($request->completed) === 'true' || strtolower($request->completed) === 'false'))
                {
                    $options['completed'] = strtolower($request->completed) === 'true';
                }
                
                $dates = array(
                    'due_date',
                    'updated_at',
                    'created_at'
                );
                
                foreach ($dates as $date)
                {
                    $since = $date.'_since';
                    
                    $until = $date.'_until';
                    
                    $push = array();
                    
                    if (isset($request->$since))
                    {
                        if (Shape::validDate($request->$since, 'long'))
                        {
                            $push['$gt'] = Converters::stringToDate($request->$since, 'long');
                        }
                        elseif(Shape::validDate($request->$since, 'short'))
                        {
                            $push['$gt'] = Converters::stringToDate($request->$since, 'short');
                        }
                    }
                    
                    if (isset($request->$until))
                    {
                        if (Shape::validDate($request->$until, 'long'))
                        {
                            $push['$lt'] = Converters::stringToDate($request->$until, 'long');
                        }
                        elseif(Shape::validDate($request->$until, 'short'))
                        {
                            $push['$lt'] = Converters::stringToDate($request->$until, 'short');
                        }
                    }
                    
                    if (isset($push['$gt'], $push['$lt']) && Converters::mongoDateCompare($push['$gt'], $push['$lt']) === false)
                    {
                        $push = array();
                    }
                    
                    if (!empty($push))
                    {
                        $options[$date] = $push;
                    }
                }
                
                $list = Lists::get('tasks', $options);
                
                if ($list !== false)
                {
                    $result = $list;
                }
                else
                {
                    $result['total'] = $model->count($options);
    
                    $model = $model->findWithRelations($options);
                    
                    $result['data'] = $model;
                    
                    Lists::set('tasks', $options, $result);
                }

                $response->json($result)->send();
            }
        ),
        array(
            'method' => 'GET',
            'regex' => '/[a:id]',
            'callback' => function ($request, $response, $session)
            {
                if (isset($request->id) && DataType::validOid($request->id))
                {
                    $model = new TasksModel();

                    $task = $model->findOne(array(
                        '_id' => $request->id
                    ));

                    if ($task !== FALSE)
                    {
                        $task = Parser::fastRender($task, $model->args, $model);

                        $response->json($task)->send();
                    }
                    else
                    {
                        $response->code(404)->send();
                    }
                }
                else
                {
                    $response->code(400)->send();
                }
            }
        ),
        array(
            'method' => 'POST',
            'regex' => '/?',
            'callback' => function ($request, $response, $session)
            {
                $model = new TasksModel();
                
                foreach (array('due_date', 'completed_at', 'updated_at') as $date)
                {
                    if (isset($request->$date) && Shape::validDate($request->$date, 'long') === false && Shape::validDate($request->$date, 'short') === false)
                    {
                        $response->code(400)->json(array(
                            'message' => "$date invalid"
                        ))->send();
                    }
                }
                
                $model = Parser::fastCharge($model, $request, $model->args, $model->getMap());
                    
                $result = $model->insert();
                
                if (gettype($result) === 'object')
                {
                    Lists::remove('tasks');
                    
                    $response->code(201)->json(array(
                        'id' => (string)$result
                    ))->send();
                }
                else
                {
                    $response->code(400)->json(array(
                        'message' => $result
                    ))->send();
                }
            }
        ),
        array(
            'method' => 'PUT',
            'regex' => '/[a:id]',
            'callback' => function ($request, $response, $session)
            {
                if (isset($request->id) && DataType::validOid($request->id))
                {
                    parse_str(file_get_contents('php://input'), $putArgs);
                                                
                    $model = new TasksModel();

                    $task = $model->findOne(array(
                        '_id' => (string)$request->id
                    ));

                    if ($task !== FALSE)
                    {
                        $putArgs = (object)$putArgs;
                        
                        foreach (array('due_date', 'completed_at', 'updated_at') as $date)
                        {
                            if (isset($putArgs->$date) && Shape::validDate($putArgs->$date, 'long') === false && Shape::validDate($putArgs->$date, 'short') === false)
                            {
                                $response->code(400)->json(array(
                                    'message' => "$date invalid"
                                ))->send();
                            }
                        }
                        
                        $task = Parser::fastCharge($task, $putArgs, $model->args, $model->getMap());
                        
                        $result = $task->update();

                        if ($result !== TRUE)
                        {
                            $response->code(400)->json(array(
                                'message' => $result
                            ))->send();
                        }
                        else
                        {
                            Lists::remove('tasks', (string)$request->id);
                            
                            $response->code(200)->send();
                        }
                    }
                    else
                    {
                        $response->code(404)->send();
                    }
                }
                else
                {
                    $response->code(400)->send();
                }
            }
        ),
        array(
            'method' => 'DELETE',
            'regex' => '/[a:id]',
            'callback' => function ($request, $response, $session)
            {
                if (isset($request->id) && DataType::validOid($request->id))
                {
                    $model = new TasksModel();
    
                    $task = $model->findOne(array(
                        '_id' => $request->id
                    ));
    
                    if ($task !== FALSE)
                    {
                        $result = $task->remove();
    
                        if ($result !== TRUE)
                        {
                            $response->code(400)->json(array(
                                'message' => $result
                            ))->send();
                        }
                        else
                        {
                            Lists::remove('tasks', (string)$request->id);
                            
                            $response->code(200)->send();
                        }
                    }
                    else
                    {
                        $response->code(404)->send();
                    }
                }
                else
                {
                    $response->code(400)->send();
                }
            }
        )
    )
));
