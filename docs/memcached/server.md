# Memcached Server 
## Installation
```bash
sudo apt install memcached
```
## Info
```bash
sudo service memcached status
```

```bash
● memcached.service - memcached daemon
   Loaded: loaded (/lib/systemd/system/memcached.service; enabled; vendor preset: enabled)
   Active: active (running) since lun 2017-11-06 02:19:18 -03; 20min ago
 Main PID: 31900 (memcached)
   CGroup: /system.slice/memcached.service
           └─31900 /usr/bin/memcached -m 64 -p 11211 -u memcache -l 127.0.0.1
```
**NOTE:** default port is 11211
