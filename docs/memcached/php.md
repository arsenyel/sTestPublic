# PHP 7.1 - Memcached extension
## Dependencies
```bash
sudo apt install build-essential pkg-config make git g++ gcc libmemcached-dev zlib1g-dev
```
## Extension
```bash
git clone --depth 1 https://github.com/php-memcached-dev/php-memcached.git
cd php-memcached
phpize
./configure
```
**NOTE**: if after do "./configure" shows "configure: error: memcached support requires" install depency synonym with "-dev"
```bash
make
sudo mv modules/ /usr/local/memcached/
```
## Add extension
### PHP CLI
```bash
echo 'extension=/usr/local/memcached/memcached.so' | \
sudo tee /etc/php/7.1/cli/conf.d/memcached.ini
```
### Apache2.4
```bash
echo 'extension=/usr/local/memcached/memcached.so' | \
sudo tee /etc/php/7.1/apache2/conf.d/memcached.ini
```