# Categoria

## JSON

```json
    {
        "Categoria" : "aRamdonString"
    }
```

## Name

    category

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Grupos](../Grupos/README.md)