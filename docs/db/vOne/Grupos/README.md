# Collection `Grupos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Grupo" : "aRamdonString",
    "Categoria" : "aRamdonString",
    "Estado" : true,
    "Suprema" : 1,
    "Reglas" : [{
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Grupo](Grupo.md)
- [Categoria](Categoria.md)
- [Estado](../../commons/Estado.md)
- [Suprema](../../commons/Suprema.md)
- [Reglas](Reglas/README.md)