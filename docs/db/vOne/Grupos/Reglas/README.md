# `Reglas`

```json
{
    "Reglas" : [{
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)