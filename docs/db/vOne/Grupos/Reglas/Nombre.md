# Nombre

## JSON

```json
    {
        "Nombre" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Reglas](..Reglas/README.md)