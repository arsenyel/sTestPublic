# EntradasDePulso

## JSON

```json
{
    "EntradasDePulso" : [ 
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Estado" : ObjectId("stringOf24Characteres"),
            "UltimoValor" :  "aRamdonString",
            "UltimaActualizacion" : true,
            "Historico" : [{
            	"Valor": int,
            	"Actualizacion" : date
            }]
        }
    ]
}
```

## Name

    pulseInputs

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)