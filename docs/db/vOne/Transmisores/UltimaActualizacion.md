# UltimaActualizacion

## JSON

```json
{
    "UltimaActualizacion" : date
}
```

## Name

    lastUpdate

## Type

    date

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)