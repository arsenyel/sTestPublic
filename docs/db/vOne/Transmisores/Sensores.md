# Sensores

## JSON

```json
{
   "Sensores" : [ 
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Tipo" : ObjectId("stringOf24Characteres"),
            "Unidad" :  "aRamdonString",
            "Estado" : true,
            "Tolerancia" : {
                "Minima" : int,
                "Maxima" : int
            }
        }
    ]
}
```

## Name

    sensors

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)