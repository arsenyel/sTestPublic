# EntradasDigitales

## JSON

```json
{
    "EntradasDigitales" : [
    	{
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Estado" : ObjectId("stringOf24Characteres"),
            "UltimoValor" :  int,
            "UltimaActualizacion" : date,
        }
    ]
}
```

## Name

    digitalInputs

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)