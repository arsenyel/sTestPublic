# TipoCorrespondencia

## JSON

```json
{
    "Puerto" : "aRamdonString"
}
```

## Name

    port

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)