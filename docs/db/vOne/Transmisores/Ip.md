# Ip

## JSON

```json
{
     "Ip" : "aRamdonString"
}
```

## Name

    ip

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)