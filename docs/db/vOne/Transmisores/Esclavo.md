# Esclavo

## JSON

```json
{
     "Esclavo" : int
}
```

## Name

    slave

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)