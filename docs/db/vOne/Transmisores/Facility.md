# Facility

## JSON

```json
{
    "TipoCorrespondencia" : "aRamdonString"
}
```

## Name

    facility

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](..//README.md)