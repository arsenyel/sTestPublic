# Collection `Transmisores`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Transmisor" : "aRamdonString",
    "Locacion" : ObjectId("stringOf24Characteres"),
    "Facility" : ObjectId("stringOf24Characteres"),
    "Estado" : true,
    "Ip" : "aRamdonString",
    "Puerto" : "aRamdonString",
    "Sensores" : [ 
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Tipo" : ObjectId("stringOf24Characteres"),
            "Unidad" :  "aRamdonString",
            "Estado" : true,
            "Tolerancia" : {
                "Minima" : int,
                "Maxima" : int
            }
        }
    ],
    "Actuadores" : [
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Tipo" : ObjectId("stringOf24Characteres"),
            "Normal" :  "aRamdonString",
            "Estado" : true,
            "Pulso" : int,
            "Magnitud" : "aRamdonString",
            "UltimoValor" : int
        }
    ],
    "EntradasDigitales" : [
    	{
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Estado" : ObjectId("stringOf24Characteres"),
            "UltimoValor" :  int,
            "UltimaActualizacion" : date,
        }
    ],
    "UltimaActualizacion" : date,
    "Esclavo" : int,
    "Mac" : "aRamdonString",
    "EntradasDePulso" : [ 
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Estado" : ObjectId("stringOf24Characteres"),
            "UltimoValor" :  "aRamdonString",
            "UltimaActualizacion" : true,
            "Historico" : [{
            	"Valor": int,
            	"Actualizacion" : date
            }]
        }
    ]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Transmisor](Transmisor.md)
- [Locacion](Locacion.md)
- [Facility](Facility.md)
- [Estado](Estado.md)
- [Ip](Ip.md)
- [Puerto](Puerto.md)
- [Sensores](Sensores.md)
- [Actuadores](Actuadores.md)
- [EntradasDigitales](EntradasDigitales.md)
- [UltimaActualizacion](UltimaActualizacion.md)
- [Esclavo](Esclavo.md)
- [Mac](Mac.md)
- [EntradasDePulso](EntradasDePulso.md)