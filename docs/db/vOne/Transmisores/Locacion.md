# Locacion

## JSON

```json
{
   "Locacion" : ObjectId("stringOf24Characteres")
}
```

## Name

    location

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)