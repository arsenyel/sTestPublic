# Actuadores

## JSON

```json
{
   "Actuadores" : [
        {
            "Id" : int,
            "Nombre" : "aRamdonString",
            "Tipo" : ObjectId("stringOf24Characteres"),
            "Normal" :  "aRamdonString",
            "Estado" : true,
            "Pulso" : int,
            "Magnitud" : "aRamdonString",
            "UltimoValor" : int
        }
    ]
}
```

## Name

    actuators

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Transmisores](../Transmisores/README.md)