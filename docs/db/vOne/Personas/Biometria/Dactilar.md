# Foto

## JSON

```json
    {
        "Dactilar" : [
            "aRandomStringOfNumbersSeparatedByCommas"
        ]
    }
```

## Name

    finger

## Type

    array

## Required

This field is required.

## Description

Is an array of 10 positions top, on each position has an string of numbers separated by commas representing a finger print.