# Foto

## JSON

```json
    {
        "Foto" : "aRamdonStringBase64"
    }
```

## Name

    picture

## Type

    string

## Required

This field is required.