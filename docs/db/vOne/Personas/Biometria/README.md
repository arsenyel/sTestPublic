# Biometria

## JSON

```json
    {
        "Biometria" : {
            "Foto" : "aRamdonStringBase64",
            "Dactilar" : [
                "aRandomStringOfNumbersSeparatedByCommas"
            ]
        }
    }
```

## Name

    biometry

## Required

This field is not required.

## Attributes

- [Foto](Foto.md)
- [Dactilar](Dactilar.md)