# Collection `Personas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Apellido" : "aRamdonString",
    "NumeroDocumento" : "aRamdonStringNumber",
    "TipoDocumento" : ObjectId("stringOf24Characteres"),
    "Direccion" : {
        "Pais" : ObjectId("stringOf24Characteres"),
        "Provincia" : ObjectId("stringOf24Characteres"),
        "Departamento" : ObjectId("stringOf24Characteres"),
        "Localidad" : ObjectId("stringOf24Characteres"),
        "Direccion" : "aRamdonString",
        "CPostal" : "aRamdonString"
    },
    "Nacimiento" : "aRamdonString",
    "Mail" : "aRamdonString",
    "Telefonos" : {
        "Movil" : "aRamdonString",
        "Fijo" : "sRamdonString",
        "Interno" : "aRamdonString"
    },
    "Categoria" : "Interno",
    "Grupo" : ObjectId("stringOf24Characteres"),
    "Seguro" : {
        "Aseguradora" : ObjectId("stringOf24Characteres"),
        "Numero" : "aRamdonStringNumber",
        "Vencimiento" : "aRamdonStringDate"
    },
    "Licencia" : {
        "Tipo" : ObjectId("stringOf24Characteres"),
        "Numero" : "aRamdonStringNumber",
        "Vencimiento" : "aRamdonStringDate"
    },
    "Empresa" : {
        "_id" : ObjectId("stringOf24Characteres"),
        "Departamento" : 1
    },
    "Suprema" : 1,
    "Biometria" : {
        "Foto" : "aRamdonStringBase64",
        "Dactilar" : [
            "aRandomStringOfNumbersSeparatedByCommas"
        ]
    },
    "Notas" : "aRamdonString",
    "Usuario" : "aRamdonString",
    "Legajo" : "aRamdonString",
    "Pin" : "aRamdonString",
    "Rol" : ObjectId("stringOf24Characteres"),
    "Password" : "aRamdonString",
    "PasswordVCW" : "aRamdonString",
    "Puesto" : ObjectId("stringOf24Characteres"),
    "Enrolador" : ObjectId("stringOf24Characteres"),
    "Tarjeta" : ObjectId("stringOf24Characteres"),
    "TarjetaTemporal" : "aRamdonString",
    "Chats" : [{
        "Persona" : ObjectId("stringOf24Characteres"),
        "Posicion" : 0,
        "Abierto" : false
    }],
    "Estado" : true,
    "CamarasVCW" : [{
        "Camara" : ObjectId("stringOf24Characteres"),
        "Tab" : "aRamdonString"
    }],
    "EstadoLogin" : "aRamdonStringNumber",
    "TabActualVC" : "aRamdonString",
    "GrillasVC" : "aRamdonString",
    "EmapsVC" : "aRamdonString",
    "PlaybackSegundos" : "aRamdonStringNumber",
    "PlaybackCambiarTamaños" : "aRamdonStringNumber",
    "PlaybackSaltearFotogramas" : "aRamdonStringNumber",
    "PlaybackCamaras" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](../../commons/Nombre.md)
- [Apellido](Apellido.md)
- [TipoDocumento](../../commons/TipoDocumento.md)
- [NumeroDocumento](../../commons/NumeroDocumento.md)
- [Direccion](../../commons/Direccion/README.md)
- [Nacimiento](../../commons/Nacimiento.md)
- [Mail](../../commons/Mail.md)
- [Telefonos](Telefonos/README.md)
- [Categoria](Categoria.md)
- [Grupo](../../commons/Grupo.md)
- [Seguro](../../commons/Seguro/README.md)
- [Licencia](Licencia/README.md)
- [Empresa](Empresa/README.md)
- [Suprema](Suprema.md)
- [Biometria](Biometria/README.md)
- [Notas](Notas.md)
- [Usuario](Usuario.md)
- [Legajo](Legajo.md)
- [Pin](Pin.md)
- [Rol](Rol.md)
- [Password](Password.md)
- [PasswordVCW](PasswordVCW.md)
- [Puesto](Puesto.md)
- [Enrolador](Enrolador.md)
- [Tarjeta](Tarjeta.md)
- [TarjetaTemporal](TarjetaTemporal.md)
- [Chats](Chats/README.md)
- [Estado](../../commons/Estado.md)
- [CamarasVCW](CamarasVCW/README.md)
- [EstadoLogin](EstadoLogin.md)
- [TabActualVC](TabActualVC.md)
- [GrillasVC](GrillasVC.md)
- [EmapsVC](EmapsVC.md)
- [PlaybackSegundos](PlaybackSegundos.md)
- [PlaybackCambiarTamaños](PlaybackCambiarTamaños.md)
- [PlaybackSaltearFotogramas](PlaybackSaltearFotogramas.md)
- [PlaybackCamaras](PlaybackCamaras.md)