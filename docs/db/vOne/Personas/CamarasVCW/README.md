# CamarasVCW

## JSON

```json
    {
        "CamarasVCW" : [{
            "Camara" : ObjectId("stringOf24Characteres"),
            "Tab" : "aRamdonString"
        }]
    }
```

## Name

    vCenterWebCameras

## Type

    array

## Required

This field is not required.

## Childrens

- [Camara](Camara/README.md)