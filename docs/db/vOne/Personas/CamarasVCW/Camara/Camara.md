# Camara

## JSON

```json
    {
        "Camara" : ObjectId("stringOf24Characteres")
    }
```

## Name

    camera

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../../Camaras/README.md)