# Camara

## JSON

```json
    {
        "Camara" : ObjectId("stringOf24Characteres"),
        "Tab" : "aRamdonString"
    }
```

## Attributes

- [Camara](Camara.md)
- [Tab](Tab.md)