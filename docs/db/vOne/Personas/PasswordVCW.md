# PasswordVCW

## JSON

```json
    {
        "PasswordVCW" : "aRamdonString"
    }
```

## Name

    vCenterWebPassword

## Type

    string

## Required

This field is required.

## Description

This is a `XOR` password is generated in the moment that the document is created, by default is the [Documento's](Documento) value.