# Categoria

## JSON

```json
    {
        "Categoria" : "Interno"
    }
```

## Name

    category

## Type

    string

## Values Allowed
This came from the category of the group is just a way to avoid bring this information from the `Grupos` collection.
- Interno
- Externo

## Required

This field is required.