# Suprema

## JSON

```json
    {
        "Suprema" : 1
    }
```

## Name

    suprema

## Type

    int

## Required

This field es required.

## Description

This number is required for the communication with the devices "Suprema", is generated incrementally.