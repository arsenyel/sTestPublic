# EstadoLogin

## JSON

```json
    {
        "EstadoLogin" : 1
    }
```

## Type

    int

## Description

This attribute is only used by vCenter.