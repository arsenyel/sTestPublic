# Rol

## JSON

```json
    {
        "Rol" : ObjectId("stringOf24Characteres")
    }
```

## Name

    role

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)