# Pin

## JSON

```json
    {
        "Pin" : "aRamdonString"
    }
```

## Name

    pin

## Type

    string

## Required

This field is not required.