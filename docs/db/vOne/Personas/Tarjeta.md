# Tarjeta

## JSON

```json
    {
        "Tarjeta" : ObjectId("stringOf24Characteres")
    }
```

## Name

    card

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Tarjetas](../Tarjetas/README.md)