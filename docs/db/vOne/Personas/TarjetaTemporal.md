# TarjetaTemporal

## JSON

```json
    {
        "TarjetaTemporal" : "aRamdonString"
    }
```

## Name

    tempCard

## Type

    string

## Required

This field is required.

## Description

Is a electronic id.