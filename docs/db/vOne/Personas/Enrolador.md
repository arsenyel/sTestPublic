# Enrolador

## JSON

```json
    {
        "Enrolador" : ObjectId("stringOf24Characteres")
    }
```

## Name

    enroller

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Dispositivos](../Dispositivos/README.md)