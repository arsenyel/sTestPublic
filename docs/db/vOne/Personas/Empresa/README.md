# Empresa

## JSON

```json
    {
        "Empresa" : {
            "_id" : ObjectId("stringOf24Characteres"),
            "Departamento" : 1
        },
    }
```

## Name

    company

## Required

This field is not required.

## Attributes

- [_id](_id.md)
- [Departamento](Departamento.md)