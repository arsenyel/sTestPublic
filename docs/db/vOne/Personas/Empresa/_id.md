# _id

## JSON

```json
    {
        "_id" : ObjectId("stringOf24Characteres")
    }
```

## Name

    id

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Empresas](../../Empresas/README.md)