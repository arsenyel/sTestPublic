# Departamento

## JSON

```json
    {
        "Departamento" : 1
    }
```

## Name

    department

## Type

    int

## Required

This field is required.

## Description

This attribute is a relation with the collection [Empresas](../../Empresas/README.md) and is related to [Departamentos](../../Empresas/Departamentos.md) attribute.