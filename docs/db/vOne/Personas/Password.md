# Password

## JSON

```json
    {
        "Password" : "aRamdonString"
    }
```

## Name

    password

## Type

    string

## Required

This field is required.

## Description

This is a [HASH](http://php.net/manual/es/function.hash.php) password is generated in the moment that the document is created, by default is the [Documento's](Documento) value. 