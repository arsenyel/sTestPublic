# Puesto

## JSON

```json
    {
        "Puesto" : ObjectId("stringOf24Characteres")
    }
```

## Name

    position

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Puestos](../Puestos/README.md)