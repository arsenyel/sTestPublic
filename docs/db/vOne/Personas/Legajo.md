# Legajo

## JSON

```json
    {
        "Legajo" : "aRamdonString"
    }
```

## Name

    file

## Type

    string

## Required

This field is not required.