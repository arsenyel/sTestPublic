# Telefonos

## JSON

```json
    {
        "Telefonos" : {
            "Movil" : "aRamdonString",
            "Fijo" : "sRamdonString",
            "Interno" : "aRamdonString"
        }
    }
```

## Name

    phones

## Attributes

- [Movil](Movil.md)
- [Fijo](Fijo.md)
- [Iterno](Iterno.md)