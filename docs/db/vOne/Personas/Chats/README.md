# Chats

## JSON

```json
    {
        "Chats" : [{
            "Persona" : ObjectId("stringOf24Characteres"),
            "Position" : 0,
            "Abierto" : false
        }]
    }
```

## Name

    chats

## Type

    array

## Required

This field is not required.

## Childrens

- [Chat](Chat/README.md)