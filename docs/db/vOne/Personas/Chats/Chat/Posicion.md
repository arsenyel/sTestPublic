# Posicion

## JSON

```json
    {
        "Posicion" : 0
    }
```

## Name

    position

## Type

    int

## Required

This field is required.