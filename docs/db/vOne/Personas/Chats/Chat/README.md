# Chat

## JSON

```json
    {
        "Persona" : ObjectId("stringOf24Characteres"),
        "Posicion" : 0,
        "Abierto" : false
    }
```

## Attributes

- [Persona](Persona.md)
- [Posicion](Posicion.md)
- [Abierto](Abierto.md)