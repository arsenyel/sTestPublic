# Abierto

## JSON

```json
    {
        "Abierto" : true
    }
```

## Name

    open

## Type

    boolean

## Required

This field is required.