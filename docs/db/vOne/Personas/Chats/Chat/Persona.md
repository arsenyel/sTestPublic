# Persona

## JSON

```json
    {
        "Persona" : ObjectId("stringOf24Characteres")
    }
```

## Name

    person

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Personas](../../README.md)