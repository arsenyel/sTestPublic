# PlaybackSegundos

## JSON

```json
    {
        "PlaybackSegundos" : "aRamdonStringNumber"
    }
```

## Type

    string

## Description

This attribute is only used by vCenter.