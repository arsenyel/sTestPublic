# SensorType

## JSON

```json
    {
        "SensorType" : 1
    }
```

## Name

    sensorType

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)