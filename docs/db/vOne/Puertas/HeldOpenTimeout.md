# HeldOpenTimeout

## JSON

```json
    {
        "HeldOpenTimeout" : 1
    }
```

## Name

    heldOpenTimeout

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)