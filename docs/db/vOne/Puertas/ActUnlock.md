# ActUnlock

## JSON

```json
    {
        "ActUnlock" : 1
    }
```

## Name

    actUnlock

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)