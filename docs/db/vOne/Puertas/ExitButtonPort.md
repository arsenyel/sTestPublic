# ExitButtonPort

## JSON

```json
    {
        "ExitButtonPort" : 1
    }
```

## Name

    exitButtonPort

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)