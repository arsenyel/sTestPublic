# RelayPort

## JSON

```json
    {
        "RelayPort" : 1
    }
```

## Name

    relayPort

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)