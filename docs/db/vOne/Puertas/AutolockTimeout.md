# AutolockTimeout

## JSON

```json
    {
        "AutolockTimeout" : 1,
    }
```

## Name

    autolockTimeout

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)