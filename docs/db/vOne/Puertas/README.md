# Collection `Puertas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Puerta" : "aRamdonString",
    "Suprema" : 1,
    "EnterReaderID" : ObjectId("stringOf24Characteres"),
    "ExitReaderID" : ObjectId("stringOf24Characteres"),
    "AutolockTimeout" : 1,
    "HeldOpenTimeout" : 1,
    "LockedInstantly" : "aRamdonString",
    "ActLock" : 1,
    "ActUnlock" : 1,
    "Estado" : true,
    "Relay" : "aRamdonString",
    "RelayPort" : 1,
    "Sensor" : "aRamdonString",
    "SensorPort" : 1,
    "SensorType" : 1,
    "ExitButton" : "aRamdonString",
    "ExitButtonPort" : 1,
    "ExitButtonType" : 1
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Puerta](Puerta.md)
- [Suprema](../../commons/Suprema.md)
- [EnterReaderID](EnterReaderID.md)
- [ExitReaderID](ExitReaderID.md)
- [AutolockTimeout](AutolockTimeout.md)
- [HeldOpenTimeout](HeldOpenTimeout.md)
- [LockedInstantly](LockedInstantly.md)
- [ActLock](ActLock.md)
- [ActUnlock](ActUnlock.md)
- [Estado](../../commons/Estado.md)
- [Relay](Relay.md)
- [RelayPort](RelayPort.md)
- [Sensor](Sensor.md)
- [SensorPort](SensorPort.md)
- [SensorType](SensorType.md)
- [ExitButton](ExitButton.md)
- [ExitButtonPort](ExitButtonPort.md)
- [ExitButtonType](ExitButtonType.md)