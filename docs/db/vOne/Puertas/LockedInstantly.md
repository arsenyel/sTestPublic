# LockedInstantly

## JSON

```json
    {
        "LockedInstantly" : "aRamdonString"
    }
```

## Name

    lockedInstantly

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)