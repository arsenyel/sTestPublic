# Estado

## JSON

```json
    {
        "Estado" : true
    }
```

## Name

    status

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)