# ExitButton

## JSON

```json
    {
        "ExitButton" : "aRamdonString"
    }
```

## Name

    exitButton

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)