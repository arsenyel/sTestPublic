# ActLock

## JSON

```json
    {
       "ActLock" : 1
    }
```

## Name

    actLock

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)