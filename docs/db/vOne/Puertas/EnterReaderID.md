# EnterReaderID

## JSON

```json
    {
        "EnterReaderID" : ObjectId("stringOf24Characteres")
    }
```

## Name

    enterReaderID

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)