# ExitButtonType

## JSON

```json
    {
        "ExitButtonType" : 1
    }
```

## Name

    exitButtonType

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)