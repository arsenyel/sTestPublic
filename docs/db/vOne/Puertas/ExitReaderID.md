# ExitReaderID

## JSON

```json
    {
        "ExitReaderID" : ObjectId("stringOf24Characteres")
    }
```

## Name

    exitReaderID

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Puertas](../Puertas/README.md)