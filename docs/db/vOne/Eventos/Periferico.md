# Periferico

## JSON

```json
    {
        "Periferico" : "aRamdonString"
    }
```

## Name

    peripheral

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Eventos](../Eventos/README.md)