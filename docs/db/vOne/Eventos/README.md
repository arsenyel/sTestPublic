# Collection `Eventos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Remota" : ObjectId("stringOf24Characteres"),
    "Periferico" : "aRamdonString",
    "Codigo" : "aRamdonString",
    "Descripcion" : "aRamdonString",
    "Fecha" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "Plataforma" : "aRamdonString",
    "Visto" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "Cable" : [
        "izquierdo", 
        "derecho"
    ]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Remota](../Remotas/README.md)
- [Periferico](Periferico.md)
- [Codigo](Codigo.md)
- [Descripcion](Descripcion.md)
- [Fecha](../../commons/Fecha.md)
- [Plataforma](Plataforma.md)
- [Visto](../../commons/Fecha.md)
- [Cable](Cable.md)