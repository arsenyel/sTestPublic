# Cable

## JSON

```json
    {
		"Cable" : [
			"izquierdo", 
			"derecho"
		]
    }
```

## Name

    cable

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Eventos](../Eventos/README.md)