# Collection `Correspondencia`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Correspondencia" : "aRamdonString",
    "TipoCorrespondencia" : ObjectId("stringOf24Characteres"),
    "Courier" : ObjectId("stringOf24Characteres"),
    "Interno" : ObjectId("stringOf24Characteres"),
    "Guardia" : ObjectId("stringOf24Characteres"),
    "Puesto" : ObjectId("stringOf24Characteres"),
    "Notas" : "aRamdonString",
    "FechaEnvio" : "aRamdonString",
    "FechaRecepcion" : "aRamdonString",
    "FechaRetiro" : "aRamdonString",
    "NotificacionTG" : "aRamdonString",
    "NotificacionMail" : "aRamdonString",
    "NotificacionTelefonica" : "aRamdonString",
    "InternoRetira" : ObjectId("stringOf24Characteres"),
    "NroReferencia" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Correspondecia](Correspondencia.md)
- [TipoCorrespondencia](../TipoCorrespondencia/README.md)
- [Courier](../Courier/README.md)
- [Interno](../Personas/README.md)
- [Guardia](../Personas/README.md)
- [Puesto](../Puestos/README.md)
- [Notas](../../commons/Notas.md)
- [FechaEnvio](FechaEnvio.md)
- [FechaRecepcion](FechaRecepcion.md)
- [FechaRetiro](FechaRetiro.md)
- [NotificacionTG](NotificacionTG.md)
- [NotificacionMail](NotificacionMail.md)
- [NotificacionTelefonica](NotificacionTelefonica.md)
- [IternoRetira](../Personas/README.md)
- [NroReferencia](NroReferencia.md)