# NroReferencia

## JSON

```json
    {
        "NroReferencia" : "aRamdonString"
    }
```

## Name

    referenceNumber

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Correspondencia](../Correspondencia/README.md)