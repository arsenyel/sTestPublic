# FechaEnvio

## JSON

```json
    {
        "FechaEnvio" : "aRamdonString"
    }
```

## Name

    shippingDate

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Correspondencia](../Correspondencia/README.md)