# Regla

## JSON

```json
    {
        "Regla" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Reglas](../Reglas/README.md)