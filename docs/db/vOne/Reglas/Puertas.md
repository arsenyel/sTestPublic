# Puertas

## JSON

```json
    {
        "Puertas" : 
		[
			{
			"Nombre" : "aRamdonString",
			"_id" : ObjectId("stringOf24Characteres")
			}
		]
    }
```

## Name

    doors

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Reglas](../Reglas/README.md)