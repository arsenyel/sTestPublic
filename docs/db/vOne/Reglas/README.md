# Collection `Reglas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Regla" : "aRamdonString",
    "Estado" : true,
    "Suprema" : 1,
    "Calendarios" : [{
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }],
    "Puertas" : [{
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Regla](Regla.md)
- [Estado](../../commons/Estado.md)
- [Suprema](../../commons/Suprema.md)
- [Calendarios](Calendarios.md)
- [Puertas](Puertas.md)