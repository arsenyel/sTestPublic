# Calendarios

## JSON

```json
    {
        "Calendarios" : 
        [
			{
			"Nombre" : "aRamdonString",
			"_id" : ObjectId("stringOf24Characteres")
			}
    	],
    }
```

## Name

    calendars

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Reglas](../Reglas/README.md)