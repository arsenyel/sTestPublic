# Lote

## JSON

```json
    {
        "Lote" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosPersonasHistorico](../AccesosPersonasHistorico/README.md)