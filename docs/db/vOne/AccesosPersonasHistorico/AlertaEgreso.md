# AlertaEgreso

## JSON

```json
    {
        "AlertaEgreso" : "aRamdonString"
    }
```

## Name

    outAlert

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosPersonasHistorico](../AccesosPersonasHistorico/README.md)