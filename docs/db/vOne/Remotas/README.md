# Collection `Remotas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Estado" : true,
    "Serial" : "aRamdonString",
    "Ip" : "aRamdonString",
    "Calendarios" : [{
        "Nombre" : "string",
        "_id" : ObjectId("stringOf24Characteres")
    }],
    "Perifericos" : [{
        "Id" : 1,
        "Nombre" : "aRamdonString",
        "Estado" : true,
        "Tipo" : "aRamdonString",
        "Comando" : "aRamdonString",
        "Puntos" : [{
            "Izquierda" : [{
                "Lat" : "aRamdonString",
                "Lng" : "aRamdonString"
            }],
            "Derecho" : [{
                "Lat" : "aRamdonString",
                "Lng" : "aRamdonString"
            }]
        }]
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)
- [Estado](../../commons/Estado.md)
- [Serial](Serial.md)
- [Ip](Ip.md)
- [Calendarios](Calendarios/README.md)
- [Perifericos](Perifericos/README.md)