# Calendarios

## JSON

```json
    {
       "Calendarios" : 
       					[{
							"Nombre" : "string",
							"_id" : ObjectId("stringOf24Characteres")
			    		}]
    }
```

## Name

    calendars

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Remotas](../Remotas/README.md)