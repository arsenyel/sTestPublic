# Puerto

## JSON

```json
    {
        "Nombre" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Remotas](../Remotas/README.md)