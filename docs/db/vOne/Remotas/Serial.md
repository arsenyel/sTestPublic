# Serial

## JSON

```json
    {
        "Serial" : "aRamdonString"
    }
```

## Name

    serial

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Remotas](../Remotas/README.md)