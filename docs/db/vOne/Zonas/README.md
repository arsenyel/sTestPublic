# Collection `Zonas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Zona" : "aRamdonString",
    "Puertas" : [ 
        {
            "Nombre" : "aRamdonString",
            "_id" : ObjectId("stringOf24Characteres")
        }, 
        {
            "Nombre" : "aRamdonString",
            "_id" : ObjectId("stringOf24Characteres")
        }
    ],
    "Estado" : true,
    "Emergencia" : false
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Zona](Zona.md)
- [Puertas](Puertas.md)
- [Estado](Estado.md)
- [Emergencia](Emergencia.md)