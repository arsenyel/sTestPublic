# Zona

## JSON

```json
{
	"Zona" : "aRamdonString"
}
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Zonas](../Zonas/README.md)