# Modelo

## JSON

```json
{
	"Modelo" : ObjectId("stringOf24Characteres")
}
```

## Name

    model

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Vehiculos](../Vehiculos/README.md)