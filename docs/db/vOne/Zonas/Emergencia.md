# Emergencia

## JSON

```json
{
	"Emergencia" : false
}
```

## Name

    emergencia

## Type

    boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Zonas](../Zonas/README.md)