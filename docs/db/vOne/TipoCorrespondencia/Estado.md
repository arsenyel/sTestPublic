# Estado

## JSON

```json
{
	 "Estado" : false
}
```

## Name

    status

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [TipoCorrespondencia](../TipoCorrespondencia/README.md)