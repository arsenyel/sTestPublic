# TipoCorrespondencia

## JSON

```json
{
    "TipoCorrespondencia" : "aRamdonString"
}
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [TipoCorrespondencia](../TipoCorrespondencia/README.md)