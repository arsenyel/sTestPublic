# Collection `TipoCorrespondencia`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "TipoCorrespondencia" : "aRamdonString",
    "Estado" : false
}
```

## Attributes

- [_id](../../commons/_id.md)
- [TipoCorrespondencia](TipoCorrespondencia.md)
- [Estado](Estado.md)