# Nivel

## JSON

```json
    {
        "PrimerActividad" : "aRamdonString"
    }
```

## Name

    firstActivity

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Sesiones](../Sesiones/README.md)