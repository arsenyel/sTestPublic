# UltimaActividadMongo

## JSON

```json
    {
        "UltimaActividadMongo" : date
    }
```

## Name

    lastActivityMongo

## Type

    date

## Required

This field is required.

## Relation

This attribute is related to the collection [Sesiones](../Sesiones.md)