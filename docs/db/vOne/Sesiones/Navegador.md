# Navegador

## JSON

```json
    {
       "Navegador" : "aRamdonString"
    }
```

## Name

    browser

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Sesiones](../Sesiones/README.md)