# Collection `Sesiones`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Usuario" : "aRamdonString",
    "Navegador" : "aRamdonString",
    "PrimerActividad" : "aRamdonString",
    "UltimaActividad" : "aRamdonString",
    "UltimaActividadMongo" : date,
    "Rol" : ObjectId("stringOf24Characteres"),
    "IP" : "aRamdonString",
    "URI" : "aRamdonString",
    "Puesto" : ObjectId("stringOf24Characteres")
    
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Usuario](Usuario.md)
- [Navegador](Navegador.md)
- [PrimerActividad](PrimerActividad.md)
- [UltimaActividad](UltimaActividad.md)
- [UltimaActividadMongo](UltimaActividadMongo.md)
- [Rol](Rol.md)
- [IP](IP.md)