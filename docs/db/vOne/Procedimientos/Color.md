# Color

## JSON

```json
    {
        "Color" : "aRamdonString"
    }
```

## Name

    color

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Procedimientos](../Procedimientos/README.md)