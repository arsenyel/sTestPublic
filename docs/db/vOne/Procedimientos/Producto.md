# Producto

## JSON

```json
    {
        "Producto" : 1
    }
```

## Name

    product

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Procedimientos](../Procedimientos/README.md)