# Collection `Procedimientos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Procedimiento" : "aRamdonString",
    "Producto" : 1,
    "Modulo" : 1,
    "Alerta" : "aRamdonString",
    "Estado" : true,
    "Color" : "aRamdonString",
    "Jerarquia" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)
- [Procedimiento](Procedimiento.md)
- [Producto](Producto.md)
- [Modulo](Modulo.md)
- [Alerta](Alerta.md)
- [Estado](../../commons/Estado.md)
- [Color](Color.md)
- [Jerarquia](Jerarquia.md)
