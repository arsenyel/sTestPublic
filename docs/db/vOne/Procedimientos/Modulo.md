# Modulo

## JSON

```json
    {
        "Modulo" : "aRamdonString"
    }
```

## Name

    module

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Procedimientos](../Procedimientos/README.md)