# Collection `Camaras`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Camara" : "aRamdonString",
    "Estado" : true,
    "Protocolo" : "aRamdonString",
    "Rotacion" : "aRamdonString",
    "Direccion" : "aRamdonString",
    "Grupo" : ObjectId("stringOf24Characteres"),
    "Mirror" : "aRamdonString",
    "Rotaciones" : "aRamdonString",
    "Grabacion" : "aRamdonString",
    "FPS" : "aRamdonString",
    "DetectarMovimiento" : "aRamdonString",
    "MascaraMovimiento" : "aRamdonString",
    "Sensitivity" : "aRamdonString",
    "History" : "aRamdonString",
    "Threshold" : "aRamdonString",
    "CicloGrabacion" : "aRamdonString",
    "Scheduler" : "aRamdonString",
    "EncoderActivo" : "aRamdonString",
    "Encoder" : "aRamdonString",
    "EncoderResolucion" : "aRamdonString",
    "StreamingWeb" : "aRamdonString",
    "WebUrl" : "aRamdonString",
    "StreamingWebResolocion" : "aRamdonString",
    "Alerta" : "aRamdonString",
    "IDAlerta" : "aRamdonString",
    "FiltroAlerta" : "aRamdonString",
    "Emails" : "aRamdonString",
    "UsuariosTelegram" : "aRamdonString",
    "Substream" : "aRamdonString",
    "SubstreamPadre" : "aRamdonString",
    "SubstreamRect" : "aRamdonString",
    "Onvif" : "aRamdonString",
    "IPOnvif" : "aRamdonString",
    "PuertoOnvif" : "aRamdonString",
    "UsuarioOnvif" : "aRamdonString",
    "PasswordOnvif" : "aRamdonString",
    "VelocidadOnvif" : "aRamdonString",
    "TimeoutOnvif" : "aRamdonString",
    "TourOnvif" : "aRamdonString",
    "TourTimeOnvif" : "aRamdonString",
    "ControladorPtz" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Camara](Camara.md)
- [Estado](../../commons/Estado.md)
- [Protocolo](Protocolo.md)
- [Rotacion](Rotacion.md)
- [Direccion](Direccion.md)
- [Grupo](Grupo.md)
- [Mirror](Mirror.md)
- [Rotaciones](Rotaciones.md)
- [Grabacion](Grabacion.md)
- [FPS](FPS.md)
- [DetectarMovimiento](DetectarMovimiento.md)
- [MascaraMovimiento](MascaraMovimiento.md)
- [Sensitivity](Sensitivity.md)
- [History](History.md)
- [Threshold](Threshold.md)
- [CicloGrabacion](CicloGrabacion.md)
- [Scheduler](Scheduler.md)
- [EncoderActivo](EncoderActivo.md)
- [Encoder](Encoder.md)
- [EncoderResolucion](EncoderResolucion.md)
- [StreamingWeb](StreamingWeb.md)
- [WebUrl](WebUrl.md)
- [StreamingWebResolocion](StreamingWebResolocion.md)
- [Alerta](Alerta.md)
- [IDAlerta](IDAlerta.md)
- [FiltroAlerta](FiltroAlerta.md)
- [Emails](Emails.md)
- [UsuariosTelegram](UsuariosTelegram.md)
- [Substream](Substream.md)
- [SubstreamPadre](SubstreamPadre.md)
- [SubstreamRect](SubstreamRect.md)
- [Onvif](Onvif.md)
- [IPOnvif](IPOnvif.md)
- [PuertoOnvif](PuertoOnvif.md)
- [UsuarioOnvif](UsuarioOnvif.md)
- [PasswordOnvif](PasswordOnvif.md)
- [VelocidadOnvif](VelocidadOnvif.md)
- [TimeoutOnvif](TimeoutOnvif.md)
- [TourOnvif](TourOnvif.md)
- [TourTimeOnvif](TourTimeOnvif.md)
- [ControladorPtz](ControladorPtz.md)