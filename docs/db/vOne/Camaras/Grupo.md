# Grupo

## JSON

```json
    {
        "Grupo" : ObjectId("stringOf24Characteres")
    }
```

## Name

    group

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)