# Onvif

## JSON

```json
    {
        "Onvif" : "aRamdonString"
    }
```

## Name

    onvif

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)