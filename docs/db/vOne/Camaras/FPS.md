# FPS

## JSON

```json
    {
        "FPS" : "aRamdonString"
    }
```

## Name

    fps

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)