# Protocolo

## JSON

```json
    {
        "Protocolo" : "aRamdonString"
    }
```

## Name

    protocol

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)