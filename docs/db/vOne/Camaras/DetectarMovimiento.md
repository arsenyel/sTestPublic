# DetectarMovimiento

## JSON

```json
    {
        "DetectarMovimiento" : "aRamdonString"
    }
```

## Name

    detectMovement

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)