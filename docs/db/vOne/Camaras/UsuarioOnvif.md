# UsuarioOnvif

## JSON

```json
    {
        "UsuarioOnvif" : "aRamdonString"
    }
```

## Name

    onvifUser

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Camaras](../Camaras/README.md)