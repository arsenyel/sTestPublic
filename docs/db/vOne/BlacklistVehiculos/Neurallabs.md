# Neurallabs

## JSON

```json
    {
        "Neurallabs" : true
    }
```

## Name

    neurallab

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [BlacklistVehiculos](../BlacklistVehiculos/README.md)