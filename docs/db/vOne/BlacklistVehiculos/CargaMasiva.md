# CargaMasiva

## JSON

```json
    {
        "CargaMasiva" : true
    }
```

## Name

    massiveCharge

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [BlacklistVehiculos](../BlacklistVehiculos/README.md)