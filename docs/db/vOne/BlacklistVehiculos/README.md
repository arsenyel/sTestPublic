# Collection `BlacklistVehiculos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Fecha" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "Interno" : ObjectId("stringOf24Characteres"),
    "Patente" : "aRamdonString",
    "Neurallabs" : true,
    "CargaMasiva" : true,
    "Adicional" : {
        "Motivo" : "aRamdonString",
        "Descripcion" : "aRamdonString",
        "Comentarios" : "aRamdonString"
    }
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Fecha](../../commons/Fecha.md)
- [Interno](../Personas/README.md)
- [Patente](../../commons/Patente.md)
- [Neurallabs](Neurallabs.md)
- [CargaMasiva](CargaMasiva.md)
- [Adicional](../../commons/Adicional/README.md)