# Collection `Dispositivos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Dispositivo" : "aRamdonString",
    "Tipo" : "aRamdonString",
    "TipoDispositivo" : ObjectId("stringOf24Characteres"),
    "IpPublica" : "aRamdonString",
    "IpInterna" : "aRamdonString",
    "Puerto" : 8000,
    "Enrolador" : true,
    "Puesto" : ObjectId("stringOf24Characteres"),
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Dispositivo](Dispositivo.md)
- [Tipo](Tipo.md)
- [TipoDispositivo](../../vSystem/TiposDispositivos/README.md)
- [IpPublica](IpPublica.md)
- [IpInterna](IpInterna.md)
- [Puerto](../../commons/Puerto.md)
- [Enrolador](Enrolador.md)
- [Puesto](../Puestos/README.md)
- [Estado](../../commons/Estado.md)