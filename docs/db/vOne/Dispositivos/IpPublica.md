# IpPublica

## JSON

```json
    {
        "IpPublica" : "aRamdonString"
    }
```

## Name

    publicIp

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Dispositivos](../Dispositivos/README.md)