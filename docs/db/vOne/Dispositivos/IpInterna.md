# IpInterna

## JSON

```json
    {
        "IpInterna" : "aRamdonString"
    }
```

## Name

    internalIp

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Dispositivos](../Dispositivos/README.md)