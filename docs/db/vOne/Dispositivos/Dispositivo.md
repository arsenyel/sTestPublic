# Dispositivo

## JSON

```json
    {
        "Dispositivo" : "aRamdonString",
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Dispositivos](../Dispositivos/README.md)