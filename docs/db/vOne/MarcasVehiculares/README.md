# Collection `MarcasVehiculares`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Marca" : "aRamdonString",
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Marca](Marca.md)
- [Estado](../../commons/Estado.md)