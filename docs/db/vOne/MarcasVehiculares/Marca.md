# Marca

## JSON

```json
    {
        "Marca" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [MarcasVehiculares](../MarcasVehiculares/README.md)