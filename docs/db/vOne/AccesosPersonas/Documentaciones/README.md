# `Documentaciones`

```json
{
    "Documentaciones" : [{
        "Tipo" : "aRamdonString",
        "Numero" : "aRamdonStringNumber",
        "Vencimiento" : ISODate("2017-06-09T04:52:17.000-03:00")
    }]
}
```

## Attributes

- [Tipo](Tipo.md)
- [Numero](Numero.md)
- [Vencimiento](Vencimiento.md)