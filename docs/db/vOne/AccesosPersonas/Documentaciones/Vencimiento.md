# Vencimiento

## JSON

```json
    {
		 "Vencimiento" : ISODate("2017-06-09T04:52:17.000-03:00")
    }
```

## Name

    expiration

## Type

    ISODate

## Required

This field is required.

## Relation

This attribute is related to [Documentaciones](../Documentaciones/README.md)