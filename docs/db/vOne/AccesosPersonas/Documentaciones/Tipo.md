# Tipo

## JSON

```json
    {
		"Tipo" : "aRamdonString"
    }
```

## Name

    type

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Documentaciones](../Documentaciones/README.md)