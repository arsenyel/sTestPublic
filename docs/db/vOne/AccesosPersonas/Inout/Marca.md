# Marca

## JSON

```json
    {
		"Marca" : "aRamdonString"
    }
```

## Name

    'brand

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Objetos](../Objetos/README.md)