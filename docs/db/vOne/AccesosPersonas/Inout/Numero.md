# Numero

## JSON

```json
    {
		"Numero" : "aRamdonStringNumber"
    }
```

## Name

    number

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Objetos](../Objetos/README.md)