# HoraInString

## JSON

```json
    {
        "HoraInString" : "aRamdonString"
    }
```

## Name

    timeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosPersonas](../AccesosPersonas/README.md)