# `Objetos`

```json
{
    "Objetos" : [{
        "Descripcion" : "aRamdonString",
        "Marca" : "aRamdonString",
        "Modelo" : "aRamdonString",
        "Numero" : "aRamdonStringNumber"
    }]
}
```

## Attributes

- [Descripcion](Descripcion.md)
- [Marca](Marca.md)
- [Modelo](Modelo.md)
- [Numero](Numero.md)