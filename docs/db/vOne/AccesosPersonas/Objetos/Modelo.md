# Modelo

## JSON

```json
    {
		"Modelo" : "aRamdonString"
    }
```

## Name

    model

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Objetos](../Objetos/README.md)