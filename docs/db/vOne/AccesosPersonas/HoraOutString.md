# HoraOutString

## JSON

```json
    {
        "HoraOutString" : "aRamdonString"
    }
```

## Name

    outTimeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosPersonas](../AccesosPersonas/README.md)