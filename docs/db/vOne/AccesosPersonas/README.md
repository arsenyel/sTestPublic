# Collection `AccesosPersonas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Persona" : ObjectId("stringOf24Characteres"),
    "Grupo" : ObjectId("stringOf24Characteres"),
    "Lote" : "aRamdonString",
    "DispositivoIn" : ObjectId("stringOf24Characteres"),
    "PuertaIn" : ObjectId("stringOf24Characteres"),
    "PuestoIn" : ObjectId("stringOf24Characteres"),
    "DispositivoOut" : ObjectId("stringOf24Characteres"),
    "PuertaOut" : ObjectId("stringOf24Characteres"),
    "PuestoOut" : ObjectId("stringOf24Characteres"),
    "HoraIn" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "HoraInString" : "08/06/2017 16:54:06",
    "HoraOut" : ISODate("2017-06-08T16:59:57.000-03:00"),
    "HoraOutString" : "08/06/2017 16:59:57",
    "HoraProgramada" : ISODate("2017-06-09T04:52:17.000-03:00"),
    "HoraProgramadaString" : "09/06/2017 4:52:17",
    "Objetos" : [{
        "Descripcion" : "aRamdonString",
        "Marca" : "aRamdonString",
        "Modelo" : "aRamdonString",
        "Numero" : "aRamdonStringNumber"
    }],
    "Documentaciones" : [{
        "Tipo" : "aRamdonString",
        "Numero" : "aRamdonStringNumber",
        "Vencimiento" : ISODate("2017-06-09T04:52:17.000-03:00")
    }],
    "Notas" : "aRamdonString",
    "Anfitrion" : {
        "Empresa" : ObjectId("stringOf24Characteres"),
        "Departamento" : 1,
        "Interno" : ObjectId("stringOf24Characteres")
    },
    "ConfirmarEgreso" : true,
    "VehiculoIn" : ObjectId("stringOf24Characteres"),
    "VehiculoOut" : ObjectId("stringOf24Characteres"),
    "Inout" : [{
        "Dispositivo" : ObjectId("stringOf24Characteres"),
        "Puerta" : ObjectId("stringOf24Characteres"),
        "Hora" : ISODate("2017-06-09T04:52:17.000-03:00"),
        "HoraString" : "09/06/2017 4:52:17",
        "Accion" : "aRamdonString"
    }],
    "AlertaIngreso" : "aRamdonString",
    "AlertaEgreso" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Persona](../Personas/README.md)
- [Grupo](../../commons/Grupo.md)
- [Lote](Lote.md)
- [DispositivoIn](../Dispositivos/README.md)
- [PuertaIn](../Puertas/README.md.md)
- [PuestoIn](../Puestos/README.md)
- [DispositivoOut](../Dispositivos/README.md)
- [PuertaOut](../Puertas/README.md)
- [PuestoOut](../Puestos/README.md)
- [HoraIn](../../commons/Hora.md)
- [HoraInString](HoraInString.md)
- [HoraOut](../../commons/Hora.md)
- [HoraOutString](HoraOutString.md)
- [HoraProgramada](../../commons/Hora.md)
- [HoraProgramadaString](HoraProgramadaString.md)
- [Objetos](Objetos/README.md)
- [Documentaciones](Documentaciones/README.md)
- [Notas](../../commons/Notas.md)
- [Anfitrion](Anfitrion/README.md)
- [ConfirmarEgreso](../../commons/ConfirmarEgreso.md)
- [VehiculoIn](../Vehiculos/README.md)
- [VehiculoOut](../Vehiculos/README.md)
- [Inout](Inout/README.md)
- [AlertaIngreso](AlertaIngreso.md)
- [AlertaEgreso](AlertaEgreso.md)