# HoraProgramadaString

## JSON

```json
    {
        "HoraProgramadaString" : "aRamdonString"
    }
```

## Name

    scheduledTimeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosPersonas](../AccesosPersonas/README.md)