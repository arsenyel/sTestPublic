# `Anfitrion`

```json
{
    "Anfitrion" : {
        "Empresa" : ObjectId("stringOf24Characteres"),
        "Departamento" : 1,
        "Interno" : ObjectId("stringOf24Characteres")
    }
}
```

## Attributes

- [Empresa](Empresa.md)
- [Departamento](Departamento.md)
- [Interno](Interno.md)