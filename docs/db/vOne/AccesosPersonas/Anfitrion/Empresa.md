# Empresa

## JSON

```json
    {
		"Empresa" : ObjectId("stringOf24Characteres"),
    }
```

## Name

    name

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to [Anfitrion](../Anfitrion/README.md)