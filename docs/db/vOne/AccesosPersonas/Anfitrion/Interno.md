# Interno

## JSON

```json
    {
		"Interno" : ObjectId("stringOf24Characteres")
    }
```

## Name

    intern

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to [Anfitrion](../Anfitrion/README.md)