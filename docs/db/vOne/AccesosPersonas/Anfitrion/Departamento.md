# Departamento

## JSON

```json
    {
		"Departamento" : 1
    }
```

## Name

    department

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to [Anfitrion](../Anfitrion/README.md)