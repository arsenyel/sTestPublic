# Ip

## JSON

```json
    {
        "Ip" : "aRamdonString"
    }
```

## Name

    id

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Relays](../Relays/README.md)