# Conectores

## JSON

```json
    {
	"Conectores" :
				 [{
					"Id" : 1,
					"Nombre" : "aRamdonString",
					"Normal" : true,
					"Pulso" : 1,
					"UltimoValor" : 1
				}]
    }
```

## Name

    connectors

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Relays](../Relays/README.md)