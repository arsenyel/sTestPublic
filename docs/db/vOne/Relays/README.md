# Collection `Relays`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Ip" : "aRamdonString",
    "Puerto" : 8000,
    "Usuario" : "aRamdonString",
    "Password" : "aRamdonString",
    "Estado" : true,
    "Conectores" : [{
        "Id" : 1,
        "Nombre" : "aRamdonString",
        "Normal" : true,
        "Pulso" : 1,
        "UltimoValor" : 1
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)
- [Ip](Ip.md)
- [Puerto](Puerto.md)
- [Usuario](Usuario.md)
- [Password](Password.md)
- [Estado](../../commons/Estado.md)
- [Conectores](Conectores.md)