# Puerto

## JSON

```json
    {
        "Puerto" : 8000
    }
```

## Name

    port

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Relays](../Relays/README.md)