# Usuario

## JSON

```json
    {
        "Usuario" : "aRamdonString"
    }
```

## Name

    user

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Relays](../Relays/README.md)