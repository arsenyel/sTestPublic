# Collection `Objetos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Objeto" : "aRamdonString",
    "Obligatorio" : true,
    "Estado" : true,
    "Tipo" : "aRamdonString",
    "Campos" : {
        "Descripcion" : true,
        "Marca" : true,
        "Modelo" : true,
        "Numero" : true,
        "Tipo" : true,
        "Vencimiento" : true
    },
    "Grupos" : [{
        "_id" : ObjectId("stringOf24Characteres"),
        "Nombre" : "aRamdonString"
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Objeto](Objeto.md)
- [Obligatorio](Obligatorio.md)
- [Estado](../../commons/Estado.md)
- [Tipo](Tipo.md)
- [Campos](Campos/README.md)
- [Grupos](Grupos/README.md)
