# Obligatorio

## JSON

```json
    {
        "Obligatorio" : true
    }
```

## Name

    mandatory

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Objetos](../Objetos/README.md)