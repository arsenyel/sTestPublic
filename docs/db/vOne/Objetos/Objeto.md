# Objeto

## JSON

```json
    {
        "Objeto" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Objetos](../Objetos/README.md)