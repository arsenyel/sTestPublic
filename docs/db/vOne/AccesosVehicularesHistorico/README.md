# Collection `AccesosVehicularesHistorico`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Vehiculo" : ObjectId("stringOf24Characteres"),
    "PuestoIn" : ObjectId("stringOf24Characteres"),
    "PuestoOut" : ObjectId("stringOf24Characteres"),
    "HoraIn" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "HoraInString" : "08/06/2017 16:54:06",
    "HoraOut" : ISODate("2017-06-08T16:59:57.000-03:00"),
    "HoraOutString" : "08/06/2017 16:59:57",
    "PersonaIn" : ObjectId("stringOf24Characteres"),
    "PersonaOut" : ObjectId("stringOf24Characteres"),
    "Alerta" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Vehiculo](../Vehiculos/README.md)
- [PuestoIn](../Puestos/README.md)
- [PuestoOut](../Puestos/README.md)
- [HoraIn](../../commons/Hora.md)
- [HoraInString](HoraInString.md)
- [HoraOut](../../commons/Hora.md)
- [HoraOutString](HoraOutString.md)
- [PersonaIn](../Personas/README.md)
- [PersonaOut](../Personas/README.md)
- [Alerta](Alerta.md)