# Alerta

## JSON

```json
    {
        "Alerta" : "aRamdonString"
    }
```

## Name

    alert

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosVehicularesHistorico](../AccesosVehicularesHistorico/README.md)