# Collection `Edificios`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Estado" : true,
    "Encargado" : ObjectId("stringOf24Characteres"),
    "Puesto" : ObjectId("stringOf24Characteres"),
    "Suprema" : 1,
    "Puestos" : [{
        "Puesto" : ObjectId("stringOf24Characteres"),
        "Numero" : 1,
        "Totem" : true,
        "Ip" : "aRamdonString",
        "Usuario" : "aRamdonString",
        "Password" : "aRamdonString"
    }],
    "Empresas" : [{
        "Numero" : 1,
        "Empresa" : ObjectId("stringOf24Characteres")
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)
- [Estado](../../commons/Estado.md)
- [Encargado](../Personas/README.md)
- [Puesto](../Puestos/README.md)
- [Suprema](../../commons/Suprema.md)
- [Puestos](Puestos/README.md)
- [Empresas](Empresas/README.md)