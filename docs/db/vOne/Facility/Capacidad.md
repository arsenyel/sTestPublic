# Capacidad

## JSON

```json
    {
        "Capacidad" : "aRamdonString"
    }
```

## Name

    capacity

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Facility](../Facility/README.md)