# Collection `Facility`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Facility" : "aRamdonString",
    "Capacidad" : "aRamdonString",
    "Locacion" : ObjectId("stringOf24Characteres"),
    "Estado" : true,
    "Notas" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Facility](Facility.md)
- [Capacidad](Capacidad.md)
- [Locacion](../Locaciones/README.md)
- [Estado](../../commons/Estado.md)
- [Notas](../../commons/Notas.md)