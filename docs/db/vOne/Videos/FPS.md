# FPS

## JSON

```json
{
	"FPS" : int
}
```

## Name

    fps

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Videos](../Videos/README.md)