# Collection `Videos`

```json
{    
    "_id" : ObjectId("stringOf24Characteres"),
    "Camara" : ObjectId("stringOf24Characteres"),
    "FechaDesde" : date,
    "FechaHasta" : date,
    "Duracion" : int,
    "FPS" : int,
    "Codec" : "aRamdonString",
    "Perfil" : "aRamdonString",
    "Path" : "aRamdonString",
    "Contador" : "aRamdonString"
    "Alertas" : {}
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Camara](Camara.md)
- [FechaDesde](FechaDesde.md)
- [FechaHasta](FechaHasta.md)
- [Duracion](Duracion.md)
- [FPS](FPS.md)
- [Codec](Codec.md)
- [Perfil](Perfil.md)
- [Path](Path.md)
- [Contador](Contador.md)
- [Alertas](Alertas.md)
- [NroPoliza](NroPoliza.md)
- [VtoPoliza](VtoPoliza.md)