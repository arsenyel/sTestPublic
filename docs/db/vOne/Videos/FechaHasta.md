# FechaHasta

## JSON

```json
{
	"FechaHasta" : date
}
```

## Name

    untilDate

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Videos](../Videos/README.md)