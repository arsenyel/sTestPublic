# Path

## JSON

```json
{
	 "Path" : "aRamdonString"
}
```

## Name

    path

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Videos](../Videos/README.md)