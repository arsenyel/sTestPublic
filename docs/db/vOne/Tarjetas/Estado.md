# Estado

## JSON

```json
{
	"Estado" : true
}
```

## Name

    status

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Tarjetas](../Tarjetas/README.md)