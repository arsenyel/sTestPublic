# Collection `Tarjetas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Tarjeta" : "aRamdonString",
    "Estado" : true,
    "Electronico" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Tarjeta](Tarjeta.md)
- [Estado](Estado.md)
- [Electronico](Electronico.md)