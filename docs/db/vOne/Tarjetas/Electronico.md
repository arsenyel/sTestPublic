# Electronico

## JSON

```json
    {
       "Electronico" : "aRamdonString"
    }
```

## Name

    electronic

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Tarjetas](../Tarjetas/README.md)