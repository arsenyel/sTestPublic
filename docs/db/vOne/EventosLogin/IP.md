# IP

## JSON

```json
    {
        "IP" : "aRamdonString"
    }
```

## Name

    ip

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [EventosLogin](../EventosLogin/README.md)