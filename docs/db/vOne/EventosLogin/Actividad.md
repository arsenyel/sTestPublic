# Actividad

## JSON

```json
    {
        "Actividad" : "aRamdonString"
    }
```

## Name

    activity

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [EventosLogin](../EventosLogin/README.md)