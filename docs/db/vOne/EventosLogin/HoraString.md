# HoraString

## JSON

```json
    {
        "HoraString" : "aRamdonString"
    }
```

## Name

    timeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [EventosLogin](../EventosLogin/README.md)