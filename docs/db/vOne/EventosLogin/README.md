# Collection `EventosLogin`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Usuario" : "aRamdonString",
    "Navegador" : "aRamdonString",
    "Hora" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "HoraString" : "aRamdonString",
    "Rol" : ObjectId("stringOf24Characteres"),
    "IP" : "aRamdonString",
    "Actividad" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Usuario](Usuario.md)
- [Navegador](Navegador.md)
- [Hora](../../commons/Hora.md)
- [HoraString](HoraString.md)
- [Rol](../Roles/README.md)
- [IP](IP.md)
- [Actividad](Actividad.md)