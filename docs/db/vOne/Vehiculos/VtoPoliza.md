# VtoPoliza

## JSON

```json
{
	"VtoPoliza" : "aRamdonString"
}
```

## Name

    policyDueDate

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Vehiculos](../Vehiculos/README.md)