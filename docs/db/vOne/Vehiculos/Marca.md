# Marca

## JSON

```json
{
	"Marca" : ObjectId("stringOf24Characteres")
}
```

## Name

    brand

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Vehiculos](../Vehiculos/README.md)