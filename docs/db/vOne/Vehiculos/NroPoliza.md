# NroPoliza

## JSON

```json
{
	"NroPoliza" : "aRamdonString"
}
```

## Name

    policyNumber

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Vehiculos](../Vehiculos/README.md)