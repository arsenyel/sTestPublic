# Aseguradora

## JSON

```json
    {
        "Aseguradora" : ObjectId("stringOf24Characteres")
    }
```

## Name

    insurance

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Vehiculos](..//README.md)