# Collection `Vehiculos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Patente" : "aRamdonString",
    "Marca" : ObjectId("stringOf24Characteres"),
    "Modelo" : ObjectId("stringOf24Characteres"),
    "Aseguradora" : ObjectId("stringOf24Characteres"),
    "NroPoliza" : "aRamdonString",
    "VtoPoliza" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Patente](Patente.md)
- [Marca](Marca.md)
- [Modelo](Modelo.md)
- [Aseguradora](Aseguradora.md)
- [NroPoliza](NroPoliza.md)
- [VtoPoliza](VtoPoliza.md)