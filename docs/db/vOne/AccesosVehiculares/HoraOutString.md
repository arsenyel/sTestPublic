# HoraOutString

## JSON

```json
    {
        "HoraOutString" : "08/06/2017 16:59:57"
    }
```

## Name

    outTimeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosVehiculares](../AccesosVehiculares/README.md)