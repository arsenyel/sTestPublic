# HoraInString

## JSON

```json
    {
        "HoraInString" : "08/06/2017 16:54:06"
    }
```

## Name

    inTimeString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosVehiculares](../AccesosVehiculares/README.md)