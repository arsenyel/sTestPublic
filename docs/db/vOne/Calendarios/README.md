# Collection `Calendarios`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Calendario" : "aRamdonString",
    "Estado" : true,
    "Suprema" : 1,
    "Horarios" : [{
        "Domingo" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Lunes" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Martes" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Miercoles" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Jueves" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Viernes" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }],
        "Sabado" : [{
            "Desde" : "aRamdonString",
            "Hasta" : "aRamdonString"
        }]
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Calendario](Calendario.md)
- [Estado](../../commons/Estado.md)
- [Suprema](..