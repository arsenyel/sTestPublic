# Calendario

## JSON

```json
    {
        "Calendario" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Calendarios](../Calendarios/README.md)