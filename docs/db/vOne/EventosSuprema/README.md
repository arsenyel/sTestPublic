# Collection `EventosSuprema`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Dispositivo" : 1,
    "NroEvento" : 1
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Dispositivo](Dispositivo.md)
- [NroEvento](NroEvento.md)