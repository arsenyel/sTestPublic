# Dispositivo

## JSON

```json
    {
        "Dispositivo" : 1
    }
```

## Name

    device

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [EventosSuprema](../EventosSuprema/README.md)