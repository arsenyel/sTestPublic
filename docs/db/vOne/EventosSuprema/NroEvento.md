# NroEvento

## JSON

```json
    {
        "NroEvento" : 1
    }
```

## Name

    eventNumber

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [EventosSuprema](../EventosSuprema/README.md)