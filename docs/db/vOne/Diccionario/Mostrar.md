# Mostrar

## JSON

```json
    {
       "Mostrar" : "aRamdonString"
    }
```

## Name

    show

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Diccionario](../Diccionario/README.md)