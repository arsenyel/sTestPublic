# Collection `Diccionario`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Nombre" : "aRamdonString",
    "Mostrar" : "aRamdonString",
    "Interno" : true,
    "Ejemplos" : {
        "Country" : "aRamdonString",
        "Company" : "aRamdonString",
        "Apartments" : "aRamdonString",
        "Argentina" : "aRamdonString",
        "Chile" : "aRamdonString"
    }
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Nombre](Nombre.md)
- [Mostrar](Mostrar.md)
- [Interno](Interno.md)
- [Ejemplos](Ejemplos.md)