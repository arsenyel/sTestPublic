# Ejemplos

## JSON

```json
    {
		"Ejemplos" : {
			"Country" : "aRamdonString",
			"Company" : "aRamdonString",
			"Apartments" : "aRamdonString",
			"Argentina" : "aRamdonString",
			"Chile" : "aRamdonString"
		}
    }
```

## Name

    examples

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Diccionario](../Diccionario/README.md)