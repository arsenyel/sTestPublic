# Collection `Empresas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Empresa" : "aRamdonString",
    "Notas" : "aRamdonString",
    "Estado" : true,
    "Departamentos" : [ 
        {
            "Id" : "int",
            "Departamento" : "aRamdonString",
            "Estado" : true,
            "Locacion" : ObjectId("stringOf24Characteres"),
            "LlamarATodos" : false
        }
    ]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Empresa](Empresa.md)
- [Notas](Notas.md)
- [Estado](Estado.md)
- [Departamentos](Departamentos/README.md)