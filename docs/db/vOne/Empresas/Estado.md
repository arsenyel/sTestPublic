# Estado

## JSON

```json
    {
        "Estado" : true
    }
```

## Name

    name

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Empresas](../Empresas/README.md)