# LlamarATodos

## JSON

```json
    {
		"LlamarATodos" : false
    }
```

## Name

    callAll

## Type

    boolean

## Required

This field is required.

## Relation

This attribute is related to [Departamentos](..//Departamentos/README.md)