# Extensiones

## JSON

```json
    {
		"Extensiones" : [{
			"Id" : 1,
			"Alias" : "aRamdonString",
			"Usuario" : "aRamdonString",
			"Password" : "aRamdonString",
			"Orden" : 1
		}]
    }
```

## Name

    extensions

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to [Departamentos](..//Departamentos/README.md)