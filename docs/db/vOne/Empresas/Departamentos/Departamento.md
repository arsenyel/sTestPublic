# Departamento

## JSON

```json
    {
		"Departamento" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Departamentos](..//Departamentos/README.md)