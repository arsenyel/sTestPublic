# Locacion

## JSON

```json
    {
		"Locacion" : ObjectId("stringOf24Characteres")
    }
```

## Name

    location

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to [Departamentos](..//Departamentos/README.md)