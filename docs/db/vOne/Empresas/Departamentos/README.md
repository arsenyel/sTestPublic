# `Departamentos`

```json
{
    "Departamentos" : [{
        "Id" : "aRamdonString",
        "Departamento" : "aRamdonString",
        "Estado" : true,
        "Locacion" : ObjectId("stringOf24Characteres"),
        "LlamarATodos" : false,
        "Extensiones" : [{
            "Id" : 1,
            "Alias" : "aRamdonString",
            "Usuario" : "aRamdonString",
            "Password" : "aRamdonString",
            "Orden" : 1
        }]
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Departamento](Departamento.md)
- [Estado](../../commons/Estado.md)
- [Locacion](Locacion.md)
- [LlamarATodos](LlamarATodos.md)
- [Extensiones](LlamarATodos.md)