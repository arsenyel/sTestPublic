# Notas

## JSON

```json
    {
        "Notas" : "aRamdonString"
    }
```

## Name

    notes

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Empresas](../Empresas/README.md)