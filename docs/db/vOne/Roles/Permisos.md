# Permisos

## JSON

```json
    {
       "Permisos" : {}
    }
```

## Name

    permissions

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)