# Collection `Roles`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Rol" : "aRamdonString",
    "Estado" : true,
    "Permisos" : {} ,
    "Nivel" : int,
    "Zonas" : {
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    },
    "Camaras" : {
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    },
    "Puestos" : {
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Rol](R.md)
- [Estado](Estado.md)
- [Permisos](Permisos.md)
- [Nivel](Nivel.md)
- [Zonas](Zonas.md)
- [Camaras](Camaras.md)
- [Puestos](Puestos.md)