# Estado

## JSON

```json
    {
        "Estado" : true
    }
```

## Name

    status

## Type

    boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)