# Rol

## JSON

```json
    {
         "Rol" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../RREADME.md)