# Camaras

## JSON

```json
    {
        "Camaras" : {
        	"Nombre" : "aRamdonString",
        	"_id" : ObjectId("stringOf24Characteres")
    	}
    }
```

## Name

    cameras

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)