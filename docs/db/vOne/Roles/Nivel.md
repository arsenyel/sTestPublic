# Nivel

## JSON

```json
    {
        "Nivel" : "int"
    }
```

## Name

    level

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)