# Zonas

## JSON

```json
    {
        "Zonas" : {
			"Nombre" : "aRamdonString",
			"_id" : ObjectId("stringOf24Characteres")
    	}
    }
```

## Name

    zones

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Roles](../Roles/README.md)