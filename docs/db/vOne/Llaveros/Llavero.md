# Llavero

## JSON

```json
    {
        "Llavero" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Llaveros](../Llaveros/README.md)