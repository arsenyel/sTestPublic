# Collection `Llaveros`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Llavero" : "aRamdonString",
    "Descripcion" : "aRamdonString",
    "Cantidad" : "aRamdonString",
    "Titular" : ObjectId("stringOf24Characteres"),
    "Puesto" : ObjectId("stringOf24Characteres"),
    "Estado" : true,
    "Notas" : "aRamdonString",
    "Historial" : [{
        "Titular" : ObjectId("stringOf24Characteres"),
        "FechaDesde" : "aRamdonString",
        "FechaHasta" : "aRamdonString"
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Llavero](Llavero.md)
- [Descripcion](Descripcion.md)
- [Cantidad](Cantidad.md)
- [Titular](../Personas/README.md)
- [Puesto](../Puestos/README.md)
- [Estado](../../commons/Estado.md)
- [Notas](../../commons/Notas.md)
- [Historial](Historial/README.md)