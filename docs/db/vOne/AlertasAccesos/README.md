# Collection `AlertasAccesos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Persona" : ObjectId("stringOf24Characteres"),
    "Tipo" : "aRamdonString",
    "Hora" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "HoraFin" : ISODate("2017-06-08T16:54:06.000-03:00")
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Persona](../Personas/README.md)
- [Tipo](Tipo.md)
- [Hora](../../commons/Hora.md)
- [HoraFin](../../commons/Hora.md)