# Codigo

## JSON

```json
    {
        "Codigo" : "aRamdonString"
    }
```

## Name

    code

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [MensajesWS](../MensajesWS/README.md)