# Paquete

## JSON

```json
    {
        "Paquete" : "aRamdonString"
    }
```

## Name

    package

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [MensajesWS](../MensajesWS/README.md)