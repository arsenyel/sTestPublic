# Collection `MensajesWS`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Paquete" : "aRamdonString",
    "Estado" : true,
    "Tipo" : "aRamdonString",
    "Hora" : ISODate("2017-08-10T13:59:27.000-03:00"),
    "Dispositivo" : ObjectId("stringOf24Characteres"),
    "Codigo" : "aRamdonString",
    "HoraConfirmacion" : ISODate("2017-08-10T13:59:27.000-03:00"),
    "GetLog" : 1,
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Paquete](Paquete.md)
- [Estado](../../commons/Estado.md)
- [Tipo](Tipo.md)
- [Hora](../../commons/Hora.md)
- [Dispositivo](../Dispositivos/README.md)
- [Codigo](Codigo.md)
- [HoraConfirmacion](../../commons/Hora.md)
- [GetLog](GetLog.md)
