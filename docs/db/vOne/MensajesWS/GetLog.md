# Paquete

## JSON

```json
    {
        "GetLog" : 1
    }
```

## Name

    getLog

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [MensajesWS](../MensajesWS/README.md)