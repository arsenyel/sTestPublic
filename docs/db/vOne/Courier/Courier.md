# Courier

## JSON

```json
    {
        "Courier" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Courier](../Courier/README.md)