# Collection `Courier`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Courier" : "aRamdonString",
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Courier](Courier.md)
- [Estado](../../commons/Estado.md)