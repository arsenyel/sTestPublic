# Puesto

## JSON

```json
    {
        "Puesto" : "aRamdonString"
    }
```

## Name

    name

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Puesto](../Puesto/README.md)