# Letra

## JSON

```json
    {
        "Letra" : "aRamdonString"
    }
```

## Name

    letter

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Puestos](../Puestos/README.md)