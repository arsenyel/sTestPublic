# Camaras

## JSON

```json
    {
	"Camaras" : 
		[
			{
				"Nombre" : "aRamdonString",
				"_id" : ObjectId("stringOf24Characteres")
			}
		]
    }
```

## Name

    cameras

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Puestos](../Puestos/README.md)