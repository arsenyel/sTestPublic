# Collection `Puestos`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Puesto" : "aRamdonString",
    "Locacion" : ObjectId("stringOf24Characteres"),
    "Suprema" : 1,
    "Letra" : "aRamdonString",
    "ContadorCorrespondencia" : 1,
    "Estado" : true,
    "Camaras" : [{
        "Nombre" : "aRamdonString",
        "_id" : ObjectId("stringOf24Characteres")
    }]
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Puesto](Puesto.md)
- [Locacion](../Locaciones/README.md)
- [Suprema](../../commons/Suprema.md)
- [Letra](Letra.md)
- [ContadorCorrespondencia](ContadorCorrespondencia.md)
- [Estado](../../commons/Estado.md)
- [Camaras](Camaras.md)