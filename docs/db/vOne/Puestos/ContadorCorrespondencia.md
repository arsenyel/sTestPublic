# ContadorCorrespondencia

## JSON

```json
    {
        "ContadorCorrespondencia" : 1
    }
```

## Name

    correspondencyCounter

## Type

    int

## Required

This field is required.

## Relation

This attribute is related to the collection [Puestos](../Puestos/README.md)