# Estado

## JSON

```json
    {
        "Locacion" : ObjectId("stringOf24Characteres")
    }
```

## Name

    location

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Puestos](../Puestos/README.md)