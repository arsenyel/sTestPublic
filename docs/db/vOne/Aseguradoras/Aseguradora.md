# Aseguradora

## JSON

```json
    {
        "Aseguradora" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Aseguradoras](../Aseguradoras/README.md)