# Collection `Aseguradoras`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Aseguradora" : "aRamdonString",
    "Tipo" : "aRamdonString",
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Aseguradora](Aseguradora.md)
- [Tipo](Tipo.md)
- [Estado](../../commons/Estado.md)