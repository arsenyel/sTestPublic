# Facility

## JSON

```json
    {
    	"Facility" : ObjectId("stringOf24Characteres")
    }
```

## Name

    facility

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Reservas](../Reservas/README.md)