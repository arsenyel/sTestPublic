# Collection `Reservas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "title" : "aRamdonString",
    "description" : "aRamdonString",
    "due_date" : ISODate("2017-11-07T16:54:06.000-03:00"),
    "completed" : true,
    "created_at" : ISODate("2017-11-07T16:54:06.000-03:00"),
    "updated_at" : ISODate("2017-11-07T16:54:06.000-03:00")
}
```

## Attributes

- [_id](../../commons/_id.md)
- [due_date](../../commons/ISODate.md)
- [created_at](../../commons/ISODate.md)
- [updated_at](../../commons/ISODate.md)
