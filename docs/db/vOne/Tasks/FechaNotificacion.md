# FechaNotificacion

## JSON

```json
    {
	"FechaNotificacion" : "aRamdonString"
    }
```

## Name

    notificationDate

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Remotas](../Remotas/README.md)