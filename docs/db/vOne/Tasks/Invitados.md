# Perifericos

## JSON

```json
    {
        "Perifericos" : [{
			"Id" : 1,
			"Nombre" : "aRamdonString",
			"Estado" : true,
			"Tipo" : "aRamdonString",
			"Comando" : "aRamdonString",
			"Puntos" : [{
			    "Izquierda" : [{
				"Lat" : "aRamdonString",
				"Lng" : "aRamdonString"
			    }],
			    "Derecho" : [{
				"Lat" : "aRamdonString",
				"Lng" : "aRamdonString"
			    }]
			}]
    	
    }
```

## Name

    peripheral

## Type

    array

## Required

This field is required.

## Relation

This attribute is related to the collection [Remotas](../Remotas/README.md)