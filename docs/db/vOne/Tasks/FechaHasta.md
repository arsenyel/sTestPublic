# FechaHasta

## JSON

```json
    {
	"FechaHasta" : ISODate("2017-06-08T16:54:06.000-03:00")
    }
```

## Name

    dateTo

## Type

    ISODate

## Required

This field is required.

## Relation

This attribute is related to the collection [Reservas](../Reservas/README.md)