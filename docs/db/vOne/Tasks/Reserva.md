# Perifericos

## JSON

```json
    {
        "Reserva" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Reservas](../Reservas/README.md)