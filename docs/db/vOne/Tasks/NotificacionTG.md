# NotificacionTG

## JSON

```json
    {
	"NotificacionTG" : true
    }
```

## Name

    tgNotification

## Type

    boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [Reservas](../Reservas/README.md)