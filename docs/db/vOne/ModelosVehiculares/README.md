# Collection `ModelosVehiculares`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Modelo" : "aRamdonString",
    "Marca" : ObjectId("stringOf24Characteres"),
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Modelo](Modelo.md)
- [Marca](../MarcasVehiculares/README.md)
- [Estado](../../commons/Estado.md)
