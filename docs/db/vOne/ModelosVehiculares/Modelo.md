# Modelo

## JSON

```json
    {
        "Modelo" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [ModelosVehiculares](../ModelosVehiculares/README.md)