# Collection `BlacklistPersonas`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Fecha" : ISODate("2017-06-08T16:54:06.000-03:00"),
    "Interno" : ObjectId("stringOf24Characteres"),
    "TipoDocumento" : ObjectId("stringOf24Characteres"),
    "NumeroDocumento" : "aRamdonString",
    "Nombre" : "aRamdonString",
    "Apellido" : "aRamdonString",
    "Adicional" : {
        "Motivo" : "aRamdonString",
        "Descripcion" : "aRamdonString",
        "Comentarios" : "aRamdonString"
    }
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Fecha](../../commons/Fecha.md)
- [Interno](../Personas/README.md)
- [TipoDocumento](../../commons/TipoDocumento.md)
- [NumeroDocumento](../../commons/NumeroDocumento.md)
- [Nombre](../../commons/Nombre.md)
- [Apellido](../../commons/Apellido.md)
- [Adicional](../../commons/Adicional/README.md)