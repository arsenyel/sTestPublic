# Collection `MensajesAnulados`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Mensaje" : {
        "Suprema" : 1,
        "IdentificadorMensaje" : "aRamdonStringNumber",
        "Mensaje" : "aRamdonString"
    },
    "JSON" : "aRamdonString",
    "Suprema" : 1,
    "Fecha" : ISODate("2017-08-10T13:59:27.000-03:00")
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Mensaje](Mensaje/README.md)
- [JSON](JSON.md)
- [Suprema](../../commons/Suprema.md)
- [Fecha](../../commons/Fecha.md)