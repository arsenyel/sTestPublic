# Collection `AccesosProgramados`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "InvitacionManual" : true,
    "Anfitrion" : {
        "Empresa" : ObjectId("stringOf24Characteres"),
        "Departamento" : 1,
        "Interno" : ObjectId("stringOf24Characteres")
    },
    "FechaDesde" : ObjectId("stringOf24Characteres"),
    "FechaDesdeString" : "aRamdonString",
    "FechaHasta" : ObjectId("stringOf24Characteres"),
    "FechaHastaString" : "aRamdonString",
    "Evento" : "aRamdonString",
    "Nombre" : "aRamdonString",
    "Apellido" : "aRamdonString",
    "NumeroDocumento" : "aRamdonString",
    "TipoDocumento" : ObjectId("stringOf24Characteres"),
    "Persona" : ObjectId("stringOf24Characteres"),
    "Mail" : "aRamdonString",
    "Telefono" : "aRamdonString",
    "Interno" : "aRamdonString",
    "Movil" : "aRamdonString",
    "Licencia" : {
        "Tipo" : ObjectId("stringOf24Characteres"),
        "Numero" : "aRamdonString",
        "Vencimiento" : "aRamdonString"
    },
    "Seguro" : {
        "Aseguradora" : ObjectId("stringOf24Characteres"),
        "Numero" : "aRamdonString",
        "Vencimiento" : "aRamdonString"
    },
    "Nacimiento" :  "aRamdonString",
    "Direccion" : {
        "Pais" : ObjectId("stringOf24Characteres"),
        "Provincia" : ObjectId("stringOf24Characteres"),
        "Departamento" : ObjectId("stringOf24Characteres"),
        "Localidad" : ObjectId("stringOf24Characteres"),
        "Direccion" : "aRamdonString",
        "CPostal" : "aRamdonString"
    },
    "EnviadaPor" : ObjectId("stringOf24Characteres"),
    "BorradaPor" : ObjectId("stringOf24Characteres"),
    "ConfirmarEgreso" : true,
    "UnicoAcceso" : true,
    "Estado" : true,
    "Notas" : "aRamdonString"
}
```

## Attributes

- [_id](../../commons/_id.md)
- [InvitacionManual](InvitacionManual.md)
- [Anfitrion](../../commons/Anfitrion.md)
- [FechaDesde](../../commons/Fecha.md)
- [FechaDesdeString](FechaDesdeString.md)
- [FechaHasta](../../commons/Fecha.md)
- [FechaHastaString](FechaHastaString.md)
- [Evento](Evento.md)
- [Nombre](../../commons/Nombre.md)
- [Apellido](../../commons/Apellido.md)
- [NumeroDocumento](../../commons/NumeroDocumento.md)
- [TipoDocumento](../../commons/TipoDocumento.md)
- [Persona](../Personas/README.md)
- [Mail](../../commons/Mail.md)
- [Telefono](Telefono.md)
- [Interno](Interno.md)
- [Movil](Movil.md)
- [Licencia](../../commons/Licencia/README.md.md)
- [Seguro](../../commons/Seguro/README.md)
- [Nacimiento](../../commons/Nacimiento.md)
- [Direcion](../../commons/Direccion/README.md)
- [EnviadaPor](../Personas/README.md)
- [BorradaPor](../Personas/README.md)
- [ConfirmarEgreso](../../commons/ConfirmarEgreso.md)
- [UnicoAcceso](UnicoAcceso.md)
- [Estado](../../commons/Estado.md)
- [Notas](../../commons/Notas.md)