# Telefono

## JSON

```json
    {
        "Telefono" : "aRamdonString"
    }
```

## Name

    phone

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosProgramados](../AccesosProgramados/README.md)