# Interno

## JSON

```json
    {
        "Interno" : "aRamdonString"
    }
```

## Name

    internal

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosProgramados](../AccesosProgramados/README.md)