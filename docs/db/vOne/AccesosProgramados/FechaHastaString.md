# FechaHastaString

## JSON

```json
    {
        "FechaHastaString" : "aRamdonString"
    }
```

## Name

    dateToString

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosProgramados](../AccesosProgramados/README.md)