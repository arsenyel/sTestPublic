# InvitacionManual

## JSON

```json
    {
        "InvitacionManual" : true
    }
```

## Name

    handInvitation

## Type

    Boolean

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosProgramados](../AccesosProgramados/README.md)