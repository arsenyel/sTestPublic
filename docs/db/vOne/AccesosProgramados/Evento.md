# Evento

## JSON

```json
    {
        "Evento" : "aRamdonString"
    }
```

## Name

    event

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [AccesosProgramados](../AccesosProgramados/README.md)