# Respaldo

## JSON

```json
    {
        "RespaldosMaximos" : int,
        "RespaldosAutomaticos" : "aRamdonString",
        "RespaldosTipoGeneracion" : int,
        "RespaldosHoraGeneracion" : "aRamdonString",
        "RespaldosFechaDesde" : "aRamdonString",
        "ProximoRespaldo" : "aRamdonString"
    }
```

## Name

    backup

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)