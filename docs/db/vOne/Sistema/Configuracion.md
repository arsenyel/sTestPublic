# Configuracion

## JSON

```json
{
	"MongoRam" : int,
	"TiempoChat" : int
}
```

## Name

    configuration

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema.md)