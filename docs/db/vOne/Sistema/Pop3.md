# Pop3

## JSON

```json
    {
        "Enable" : false,
        "Host" : "aRamdonString",
        "Usuario" : "aRamdonString",
        "Auth" : false,
        "Password" : "aRamdonString",
        "Secure" : "aRamdonString",
        "Puerto" : int
    }
```

## Name

    pop3

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)