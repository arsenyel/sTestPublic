# Asterisk

## JSON

```json
{
	"Asterisk" : {
		"Enable" : true,
		"Host" : "aRamdonString",
		"Puerto" : int,
		"TiempoDeTimbre" : int,
		"Stun" : "aRamdonString",
		"Token" : "aRamdonString",
		"WebSocket" : "aRamdonString"
	}
}
```

## Name

    asterisk

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)