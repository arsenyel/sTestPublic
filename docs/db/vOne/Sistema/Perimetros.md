# Perimetros

## JSON

```json
    {
        "Cortado" : "aRamdonString",
        "Intrusion" : "aRamdonString",
        "Armado" : "aRamdonString",
        "Desarmado" : "aRamdonString",
        "SinRespuesta" : "aRamdonString"
    }
```

## Name

    perimeters

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)