# Collection `Sistema`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Regionalizacion" : {
        "Pais" : ObjectId("stringOf24Characteres"),
        "Provincia" : ObjectId("stringOf24Characteres"),
        "Departamento" : ObjectId("stringOf24Characteres"),
        "Localidad" : ObjectId("stringOf24Characteres"),
        "MascaraFijo" : int,
        "MascaraMovil" : int,
        "TipoDocumento" : ObjectId("stringOf24Characteres")
    },
    "IdentidadCorporativa" : {
        "Id" : "aRamdonString",
        "Nombre" : "aRamdonString",
        "Direccion" : "aRamdonString",
        "Telefono" : "aRamdonString",
        "Mail" : "aRamdonString",
        "Latitud" : "aRamdonString",
        "Longitud" : "aRamdonString",
        "Url" : "aRamdonString",
        "Logo" : "aRamdonString"
     },
    "Configuracion" : {
        "MongoRam" : int,
        "TiempoChat" : int
    },
    "Perimetros" : {
        "Cortado" : "aRamdonString",
        "Intrusion" : "aRamdonString",
        "Armado" : "aRamdonString",
        "Desarmado" : "aRamdonString",
        "SinRespuesta" : "aRamdonString",
    },
    "Smtp" : {
        "Enable" : true,
        "Host" : "aRamdonString",
        "Usuario" : "aRamdonString",
        "Auth" : true,
        "Password" : "aRamdonString",
        "Secure" : "aRamdonString",
        "Puerto" : int
    },
    "Pop3" : {
        "Enable" : false,
        "Host" : "aRamdonString",
        "Usuario" : "aRamdonString",
        "Auth" : false,
        "Password" : "aRamdonString",
        "Secure" : "aRamdonString",
        "Puerto" : int
    },
    "Mqtt" : {
        "Host" : "aRamdonString",
        "Puerto" : int
    },
    "Neurallabs" : {
        "Enable" : false,
        "Direccion" : "aRamdonString",
        "Puerto" : "int",
        "Usuario" : "aRamdonString",
        "Password" : "aRamdonString",
        "Database" : "aRamdonString",
        "UltimoRegistro" : "int"
    },
    "Asterisk" : {
        "Enable" : true,
        "Host" : "aRamdonString",
        "Puerto" : int,
        "TiempoDeTimbre" : int,
        "Stun" : "aRamdonString",
        "Token" : "aRamdonString",
        "WebSocket" : "aRamdonString"
    },
    "Respaldo" : {
        "RespaldosMaximos" : int,
        "RespaldosAutomaticos" : "aRamdonString",
        "RespaldosTipoGeneracion" : int,
        "RespaldosHoraGeneracion" : "aRamdonString",
        "RespaldosFechaDesde" : "aRamdonString",
        "ProximoRespaldo" : "aRamdonString",
    }
    
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Regionalizacion](Regionalizacion.md)
- [IdentidadCorporativa](IdentidadCorporativa.md)
- [Configuracion](Configuracion.md)
- [Perimetros](Perimetros.md)
- [Smtp](Smtp.md)
- [Pop3](Pop3.md)
- [Mqtt](Mqtt.md)
- [Neurallabs](Neurallabs.md)
- [Asterisk](Asterisk.md)
- [Respaldo](Respaldo.md)