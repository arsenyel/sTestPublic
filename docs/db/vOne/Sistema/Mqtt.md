# Mqtt

## JSON

```json
    {
        "Host" : "aRamdonString",
        "Puerto" : int"
    }
```

## Name

    position

## Type

    ObjectId

## Required

This field is required.

## Relation

This attribute is related to the collection [Sesiones](../Sesiones/README.md)