# Smtp

## JSON

```json
    {
	"Enable" : true,
	"Host" : "aRamdonString",
	"Usuario" : "aRamdonString",
	"Auth" : true,
	"Password" : "aRamdonString",
	"Secure" : "aRamdonString",
	"Puerto" : int
    }
```

## Name

    smtp

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)