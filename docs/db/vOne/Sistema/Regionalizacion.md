# Regionalizacion

## JSON

```json
    {
		"Pais" : ObjectId("stringOf24Characteres"),
		"Provincia" : ObjectId("stringOf24Characteres"),
		"Departamento" : ObjectId("stringOf24Characteres"),
		"Localidad" : ObjectId("stringOf24Characteres"),
		"MascaraFijo" : int,
		"MascaraMovil" : int,
		"TipoDocumento" : ObjectId("stringOf24Characteres")
    }
```

## Name

    regionalization

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)