# IdentidadCorporativa

## JSON

```json
    {
        "IdentidadCorporativa" : {
			"Id" : "aRamdonString",
			"Nombre" : "aRamdonString",
			"Direccion" : "aRamdonString",
			"Telefono" : "aRamdonString",
			"Mail" : "aRamdonString",
			"Latitud" : "aRamdonString",
			"Longitud" : "aRamdonString",
			"Url" : "aRamdonString",
			"Logo" : "aRamdonString"
     	}
    }
```

## Name

    corporateIdentity

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../S/README.md)