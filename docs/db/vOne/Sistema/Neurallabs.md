# Neurallabs

## JSON

```json
    {
        "Enable" : false,
        "Direccion" : "aRamdonString",
        "Puerto" : int,
        "Usuario" : "aRamdonString",
        "Password" : "aRamdonString",
        "Database" : "aRamdonString",
        "UltimoRegistro" : int
    }
```

## Name

    neurallabs

## Type

    Object

## Required

This field is required.

## Relation

This attribute is related to the collection [Sistema](../Sistema/README.md)