# Collection `Locaciones`

```json
{
    "_id" : ObjectId("stringOf24Characteres"),
    "Locacion" : "aRamdonString",
    "Latitud" : "aRamdonString",
    "Longitud" : "aRamdonString",
    "Zoom" : 1,
    "Estado" : true
}
```

## Attributes

- [_id](../../commons/_id.md)
- [Locacion](Locacion.md)
- [Latitud](Latitud.md)
- [Longitud](Longitud.md)
- [Zoom](Zoom.md)
- [Estado](../../commons/Estado.md)