# Longitud

## JSON

```json
    {
        "Longitud" : "aRamdonString"
    }
```

## Name

    longitude

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Locaciones](../Locaciones/README.md)