# Latitud

## JSON

```json
    {
        "Latitud" : "aRamdonString"
    }
```

## Name

    latitude

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Locaciones](../Locaciones/README.md)