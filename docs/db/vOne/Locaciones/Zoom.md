# Locacion

## JSON

```json
    {
        "Locacion" : "aRamdonString"
    }
```

## Name

    name

## Type

    string

## Required

This field is required.

## Relation

This attribute is related to the collection [Locaciones](../Locaciones/README.md)