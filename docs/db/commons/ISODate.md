# _id

## JSON

```json
    {
        "date" : ISODate("2017-11-07T16:54:06.000-03:00")
    }
```

## Name

This field name depends from model

## Type

    ISODate

More information [here](http://php.net/manual/es/class.mongodb-bson-utcdatetime.php)
