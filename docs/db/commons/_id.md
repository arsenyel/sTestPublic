# _id

## JSON

```json
    {
        "_id" : ObjectId("stringOf24Characteres")
    }
```

## Name

This field is accessible through the id attribute

    id

## Type

    ObjectId

More information [here](http://php.net/manual/es/class.mongodb-bson-objectid.php)

## Required

This field es required for all the collections.