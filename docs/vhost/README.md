# Creation the vhost for apache
**Note:** the name stest.local is optional, it can be chaged for the domain selected.
## 1: Open the terminal
## 2: Create a file on vhost directory
```bash
sudo touch /etc/apache2/sites-available/stest.local.conf
```
## 3: Edit the file
```bash
sudo nano /etc/apache2/sites-available/stest.local.conf
```
## 4: Add the next
### Without ssl
```apacheconf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName stest.local
    ServerAlias stest.local
    DocumentRoot /home/user/tests/sTest
    DirectoryIndex index.php index.html

    <Directory /home/user/tests/sTest>
        Options +Indexes +FollowSymLinks +MultiViews
        AllowOverride All
        Require all granted
        XSendFile On
        XSendFilePath /home/user/tests/sTest
        XSendFilePath /home/user/tests/sTest/routes
        XSendFilePath /home/user/tests/sTest/routes/api

    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/stest-error.log
    CustomLog ${APACHE_LOG_DIR}/stest-access.log combined
</VirtualHost>
```
### With SSL and rewrite
```apacheconf
<VirtualHost *:80 *:443>
    ServerAdmin webmaster@localhost
    ServerName stest.local
    ServerAlias stest.local
    DocumentRoot /home/user/tests/sTest
    DirectoryIndex index.php index.html

    <Directory /home/user/tests/sTest>
        Options +Indexes +FollowSymLinks +MultiViews
        AllowOverride None
        Require all granted
        RewriteEngine On
        RewriteCond %{HTTPS} off
        RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URL}
        XSendFile On
        XSendFilePath /home/user/tests/sTest
        XSendFilePath /home/user/tests/sTest/routes
        XSendFilePath /home/user/tests/sTest/routes/api
    </Directory>

	SSLEngine on

    SSLCertificateFile /etc/ssl/certs/user.crt
    SSLCertificateKeyFile /etc/ssl/certs/myserver.key
	SSLCACertificateFile /etc/ssl/certs/AlphaSSLroot.crt

    ErrorLog ${APACHE_LOG_DIR}/stest-error.log
    CustomLog ${APACHE_LOG_DIR}/stest-access.log combined
</VirtualHost>
```
**Note:** The path in `DocumentRoot` and `<Directory >` it can changed from machine.

## 5: Create the redirect
Edit the file `hosts` just for make the redirect to localhost from stest.local.
### 5.1: Path to the file
```bash
sudo nano /etc/hosts
```
### 5.2: Add the next
```text
127.0.0.1   stest.local
```
### 6: Add site
```bash
sudo a2ensite stest.local.conf
```
### 7: Allow rewrite
```bash
sudo a2enmod rewrite

```
### 8: Install XSendFile Module
```bash 
sudo apt install libapache2-mod-xsendfile
```
```bash
sudo a2enmod xsendfile
```
### 9: Allow ssl (only if SSL installed)
```bash
sudo a2enmod ssl
