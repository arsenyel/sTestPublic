# Tasks
## Requests
### GET
#### List of tasks

URL: host/api/tasks/

Parameter | Values | Required | Description
--- |--- | --- | ---
completed | string (true/false) | false | Only completed tasks
due_date_since | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | due date greater than value
due_date_until | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | due date litter than value
updated_at_since | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | updated at greater than value
updated_at_until | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | updated at litter than value
created_at_since | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | created at greater than value
created_at_until | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | created at litter than value
page | int | false | to know how many register must be skipped (default: 1)

#### Response
```json
{
    "data" : [
        {
            "id" : "123456789012345678901234",
            "title" : "Title",
            "description" : "Description",
            "due_date" : "12/12/2012 12:12:12",
            "completed" : true,
            "created_at" : "10/10/2010 10:10:10",
            "updated_at" : "11/11/2011 11:11:11"
        }
    ],
    "total" : 6
}
```

#### Single task
URL: host/api/tasks/[:id]/
[:id] must be a valid oid

### POST
Parameter|Values|Required|Description
--- |--- | --- | ---
completed | Boolean | false | status for task
due_date | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | true | due date for task
updated_at | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | updated at date for task (can't be litter than create date)
created_at | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | created at date for task (can't be greater than update or due date)
title | String | true | title for task

### PUT
URL: host/api/tasks/[:id]/
[:id] must be a valid oid

Parameter|Values|Required|Description
--- |--- | --- | ---
completed | Boolean | false | new status for task
due_date | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | new due date for task
updated_at | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | new updated at date for task (can't be litter than create date)
created_at | date(dd/mm/yyyy hh:ii:ss)/date(dd/mm/yyyy) | false | new created at date for task (can't be greater than update or due date)
title | String | false | new title for task

### DELETE
URL: host/api/tasks/[:id]/
[:id] must be a valid oid
