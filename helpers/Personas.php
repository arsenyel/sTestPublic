<?php
namespace Helpers;

use Core\Helper\Parser;
use Models\VSystemModel;
use Models\InsuranceCarrierModel;
use Models\SystemModel;
use Models\GroupModel;
use Models\DeviceModel;
use MongoDB\BSON\ObjectID;
use \stdClass;

class Personas
{
    public static function getLists($result = array(), $session, $type = 'empleados')
    {
        $sistemaModel = new SystemModel();

        $result['regionalization'] = $sistemaModel->getRegionalization();

        $groupModel = new GroupModel();

        $result['groups'] = array();

        $optionsGroup = array(
            'Categoria' => 'Interno',
            'Estado' => TRUE,
            'sort' => array(
                'Grupo' => 1
            ),
            'projection' => array(
                'Grupo' => 1
            )
        );

        if ($type !== 'empleados')
        {
            $optionsGroup['Categoria'] = 'Externo';
        }

        foreach ($groupModel->find($optionsGroup) as $key => $group)
        {
            $temp = new stdClass();

            $temp->label = $group->Grupo;

            $temp->value = (string)$group->_id;

            $result['groups'][] = $temp;
        }

        $result['countries'] = VSystemModel::getCountries(array(), array(
            'projection' => array(
                'Pais' => 1
            ),
            'sort' => array(
                'Pais' => 1
            )
        ));

        $result['provinces'] = VSystemModel::getProvinces(array(
            'Pais' => new ObjectID($result['regionalization']->country)
        ), array(
            'projection' => array(
                'Provincia' => 1
            ),
            'sort' => array(
                'Provincia' => 1
            )
        ));

        $result['departments'] = VSystemModel::getDeparments(array(
            'Pais' => new ObjectID($result['regionalization']->country),
            'Provincia' => new ObjectID($result['regionalization']->province)
        ), array(
            'projection' => array(
                'Departamento' => 1
            ),
            'sort' => array(
                'Departamento' => 1
            )
        ));

        $result['localities'] = VSystemModel::getLocalities(array(
            'Pais' => new ObjectID($result['regionalization']->country),
            'Provincia' => new ObjectID($result['regionalization']->province),
            'Departamento' => new ObjectID($result['regionalization']->department)
        ), array(
            'projection' => array(
                'Localidad' => 1
            ),
            'sort' => array(
                'Localidad' => 1
            )
        ));

        $result['licenses'] = VSystemModel::getLicenses(array(
            'Pais' => new ObjectID($result['regionalization']->country)
        ), array(
            'projection' => array(
                'Licencia' => 1
            ),
            'sort' => array(
                'Licencia' => 1
            )
        ));

        $result['typesDocument'] = VSystemModel::getDocuments(array(), array(
            'projection' => array(
                'TipoDocumento' => 1
            ),
            'sort' => array(
                'TipoDocumento' => 1
            )
        ));

        $carriers = new InsuranceCarrierModel();

        $result['carriers'] = $carriers->getList();

        $result['settings'] = array();
        
        if (isset($session->position) && isset($session->enroller))
        {
            $model = new DeviceModel();
            
            $enroller = $model->findOne(array(
                '_id' => $session->enroller
            ));
            
            if ($enroller !== FALSE)
            {
                $enroller = Parser::slowRender($enroller, $model->args, $model);
                
                if (isset($enroller->deviceType))
                {
                    if (isset($enroller->deviceType->Acepta))
                    {
                        if (isset($enroller->deviceType->Acepta->FingerPrint))
                        {
                            if ($enroller->deviceType->Acepta->FingerPrint === '1')
                            {
                                $result['settings']['biometric'] = TRUE;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}

?>