<?php
namespace Helpers;

use Core\Helper\Strings;
use Core\Helper\Converters;
use Core\Helper\Parser;
use Models\SystemModel;

class Accesos
{
    public static function prepareAccess($params)
    {
        $temp = new \stdClass();

        $temp->handInvitation = FALSE;

        if (isset($params['request']))
        {
            $request = $params['request'];

            $temp->documentType = $request->documentType;
            
            $temp->documentNumber = $request->documentNumber;
            
            $temp->firstname = $request->firstname;
            
            $temp->lastname = $request->lastname;

            if (isset($request->phone))
            {
                $temp->phone = $request->phone;
            }

            $temp->dateFrom = $request->dateFrom;
            
            $temp->dateFromString = $request->dateFrom;
    
            $temp->dateTo = $request->dateTo;
    
            $temp->dateToString = $request->dateTo;
    
            $temp->event = $request->event;

            $temp->confirmEgress = FALSE;

            if (isset($request->confirmEgress))
            {
                $temp->confirmEgress = ($request->confirmEgress === 'true');
            }
    
            $temp->singleAccess = FALSE;

            if (isset($request->singleAccess))
            {
                $temp->singleAccess = ($request->singleAccess === 'true');
            }
        }

        if (isset($params['person']))
        {
            $person = $params['person'];

            $temp->firstname = $person->firstname;
            
            $temp->lastname = $person->lastname;

            $temp->documentNumber = $person->documentNumber;

            $temp->documentType = $person->documentType->id;

            $temp->person = $person->id;

            if (isset($person->phones->mobile))
            {
                $temp->phone = $person->phones->mobile;
            }

            $temp->birthdate = $person->birthdate;

            $temp->address = $person->address;
        }

        if (isset($params['hand']))
        {
            $temp->handInvitation = $params['hand'] === 'true';
        }

        if (isset($params['host']))
        {
            $temp->host = (object)$params['host'];
        }

        if (isset($params['mail']))
        {
            $temp->mail = $params['mail'];
        }

        if (isset($params['sentBy']))
        {
            $temp->sentBy = $params['sentBy'];
        }

        $temp->status = 'true';

        return $temp;
    }

    public static function prepareMessage($params)
    {
        $result = array(
            'body' => '',
            'subject' => array()
        );

        if (isset($params['request']))
        {
            $request = $params['request'];

            if ($request->controller === 'mail')
            {
                $result['body'] .= Strings::formatString($request->text, array(
                    '&lt;Destinatario&gt;' => $params['mail'],
                    '<Destinatario>' => $params['mail']
                ));
            }

            if ($request->controller === 'manual')
            {
                $host = $params['host'];

                $result['body'] .= "Usted a sido invitado al evento {$request->event} por {$host->firstname} {$host->lastname}<br>Desde el: {$request->dateFrom}<br>Hasta: {$request->dateTo}<br><br>Recuerde traer su Documento<br>";
            }

            if (isset($request->subject))
            {
                $result['subject'] = $request->subject;
            }
            else
            {
                $result['subject'] = 'Ha sido invitado a un evento';
            }
        }

        $result['body'] .= '<img alt="QR Code" src="cid:qrcode"/>';

        $systemModel = new SystemModel();

        $system = $systemModel->findOne();

        $system = Parser::slowRender($system, $systemModel->args, $systemModel);

        $foot = "<p>";

        if (isset($system->corporateIdentity))
        {
            $customer = $system->corporateIdentity;

            if (isset($customer->name))
            {
                $foot .= $customer->name;
            }
    
            if (isset($customer->address))
            {
                $foot .= " - Dirección: $customer->address";
            }

            if (isset($customer->mail))
            {
                $foot .= " - Correo de contacto: $customer->mail";
            }

            if (isset($customer->phone))
            {
                $foot .= " - Teléfono de contacto: $customer->phone";
            }

            if (isset($customer->latitude) && isset($customer->longitude))
            {
                $url = self::getUrlGoogleMaps(array(
                    'latitude' => $customer->latitude,
                    'longitude' => $customer->longitude                    
                ));

                $foot .= ' - Coordenadas de Google Maps: <a href="' . $url . '">Link a Google Maps</a>';
            }
        }

        $foot .= "</p>";

        $result['body'] .= $foot;

        return $result;
    }

    private static function getUrlGoogleMaps($params)
    {
        $g = Converters::decimalDegreesToGoogleMaps($params['latitude'], $params['longitude']);

        $url = "https://www.google.com.ar/maps/place/{$g['latitude']['grades']}°{$g['latitude']['minutes']}'{$g['latitude']['seconds']}.{$g['latitude']['hundredths']}'{$g['letterLatitude']}+{$g['longitude']['grades']}°{$g['longitude']['minutes']}'{$g['longitude']['seconds']}.{$g['longitude']['hundredths']}'{$g['letterLongitude']}";
        
        return $url;
    }
}